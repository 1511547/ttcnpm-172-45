export namespace Result {
    export class ResultInfo {
        code: number;
        message: string;
        constructor(code: number, message: string) {
            this.code = code;
            this.message = message;
        }
    }
    export class Exception extends ResultInfo implements Error {
        name: string;
        err: Error;
        constructor(err: Error) {
            super(-1, err.message);
            this.name = err.name;
            this.err = err;
        }
    }
}
export namespace Result {
    let count = 0;
    let results: Map<number, ResultInfo> = new Map<number, ResultInfo>();
    export let Success = Add("Success");
    export let UsernameInvalid = Add("Username is invalid.");
    export let EmailInvalid = Add("Email is invalid");
    export let PasswordInvalid = Add("Password is invalid");
    export let UserExist = Add("User is exist");
    export let UserNotExist = Add("User is not exist");
    export let WrongLoginData = Add("Wrong username or password.");
    function Add(message: string): ResultInfo {
        let result = new ResultInfo(results.size, message);
        result.code = count;
        results[result.code] = result;
        count++;
        return result;
    }
}
