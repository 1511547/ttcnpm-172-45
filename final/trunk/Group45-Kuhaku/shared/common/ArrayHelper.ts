export namespace ArrayHelper {
    export function expand(len: number): Array<number> {
        let arr: Array<number> = new Array<number>();
        for (let i = 0; i < len; i++) {
            arr.push(i);
        }
        return arr;
    }
}