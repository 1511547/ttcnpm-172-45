export enum ePlayerStatus {
    Free = 1, // 00000001
    InRoom = 2,
    Ready = 4,
    Playing = ePlayerStatus.InRoom | ePlayerStatus.Ready, // 00000110
}