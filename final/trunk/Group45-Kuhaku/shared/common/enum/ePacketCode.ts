export enum ePacketCode {
    Login = 1,
    Logout,
    GetProfileData,
    EnterRoom,
    LeaveRoom,
    GetRoomList,
    CreateRoom,
    RemoveRoom,
    GetRoomInfo,
    JoinRoom,
    RefreshRoom,
    GameMove,
    GameReady,
    GameStart,
    GameOver,
    GameChat,
    GameRequestLose, // Xin thua
    GameRequestDraw// Xin Hoa    
}
export default ePacketCode;