export enum STATUS {
    NotEnded,
    Win,
    Lost,
    Draw,
    FlyCheck,
    Stalemate,
    Impossbile
}

export default STATUS;