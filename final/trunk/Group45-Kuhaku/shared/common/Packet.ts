import { ePacketCode } from "./enum/ePacketCode";

export class Packet {
    code: ePacketCode;
    sussess: boolean;
    content: any;
    constructor(code: ePacketCode, success: boolean = true) {
        this.code = code;
        this.sussess = success;
    }
}