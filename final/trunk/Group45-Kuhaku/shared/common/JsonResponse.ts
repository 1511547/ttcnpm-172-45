export class JsonResponse<T> {
    success: boolean;
    data: T;
    constructor(success: boolean, data: T) {
        this.success = success;
        this.data = data;
    }
    public toString(): string {
        return JSON.stringify(this);
    }
}