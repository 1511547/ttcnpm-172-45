import { Coordinate, CoordinateState } from "./Coordinate";
import { ArrayHelper } from '../../common/ArrayHelper';
import { Cannon, Chariot, Pawn, General, Guard, Knight, Minister, Piece, PieceState } from './pieces';
import { Player } from "./Player";
import { STATUS } from "../../common/enum/STATUS";
import { setTimeout } from "timers";
import { GameMove } from "./GameMove";
import { eGameStatus } from "../../common/enum/eGameStatus";

export class ChessGame {
    private coords: Array<Coordinate[]>;
    public numPiece: number;
    private redPieces: Piece[];
    private blackPieces: Piece[];
    private selectedPiece: Piece | null;
    private activePlayer: Player;
    private isGameOver: boolean;
    public GameStatus: eGameStatus;
    private winner: Player;
    public get Winner(): Player {
        return this.winner;
    }
    public get IsGameOver(): boolean {
        return this.isGameOver;
    }
    public get CurrentTurn(): Player {
        return this.activePlayer;
    }
    public player: Player;
    /**
     * on piece move callback
     * @param isMove: Nếu là kill thì isMove=false
     */
    public onMoveCallback: (from: Coordinate, to: Coordinate, isMove: boolean) => void | null;
    protected onMove(from: Coordinate, to: Coordinate, isMove: boolean) {
        this.setDefaultState();
        this.switchPlayer();
        if (!to.piece || to.piece.coordinate !== to) {
            throw "Error";
        }
        if (!!this.onMoveCallback) {
            if (this.activePlayer !== this.player) {
                this.onMoveCallback(from, to, isMove);
            }
        }
    }
    public get CurrentPlayer(): Player {
        return this.player;
    }

    // quick test before current player doing anything
    public quickTestStatus(player: Player): STATUS {
        // two kings
        let own: (Coordinate | null) = null;
        let opponent: (Coordinate | null) = null;
        let pos: Coordinate = new Coordinate(-1, -1);
        // castle
        for (let x of [3, 4, 5]) {
            for (let y of [0, 1, 2, 7, 8, 9]) {
                pos.x = x;
                pos.y = y;
                let piece = this.coords[x][y].piece;
                if (piece == null) {
                    continue;
                }
                if (piece instanceof General) {
                    if (piece.player === player) {
                        own = new Coordinate(pos.x, pos.y);
                    } else {
                        opponent = new Coordinate(pos.x, pos.y);
                    }
                }
            }
        }
        if (own == null && opponent == null) {
            return STATUS.Impossbile;
        } else if (own == null) {
            return STATUS.Lost;
        } else if (opponent == null) {
            return STATUS.Win;
        }
        // if two kings face each other
        if (own.x === opponent.x) {
            // count intervening pieces
            let up = ((player === Player.Red) ? own.y : opponent.y);
            let blew = ((player === Player.Black) ? own.y : opponent.y);
            let intervening = 0;
            pos.x = own.x;
            pos.y = up + 1;
            while (pos.y < blew) {
                if (pos.piece === null) {
                    ++intervening;
                    break; // at least have one intervening pieces 
                }
                pos.y += 1;
            }
            // with no intervening pieces, fly check
            // win or lost depends on whose move causes fly check
            if (intervening === 0) {
                return STATUS.FlyCheck;
            }
        }

        return STATUS.NotEnded;
    }

    constructor(player: Player = Player.Red) {
        this.reset();
    }
    public reset() {
        this.coords = ArrayHelper.expand(9).map((x) => {
            return ArrayHelper.expand(10).map((y) => {
                return new Coordinate(x, y);
            });
        });
        this.isGameOver = false;
        this.GameStatus = eGameStatus.NotStart;
        this.numPiece = 0;
        this.activePlayer = Player.Red;
        this.selectedPiece = null;
        this.redPieces = this.generatePieces(Player.Red);
        this.blackPieces = this.generatePieces(Player.Black);
    }
    get getPieces(): Piece[] {
        return this.getAllPieces.filter(x => x.state !== PieceState.Dead);
    }
    public getPieceById(id: number): Piece {
        let pieces = this.getAllPieces.filter(p => p.id === id);
        if (pieces.length > 0) {
            return pieces[0];
        } else {
            return null;
        }

    }
    get getAllPieces(): Piece[] {
        return this.redPieces.concat(this.blackPieces);
    }
    public getCoordAt(x: number, y: number): Coordinate {
        return this.coords[x][y];
    }
    public move(piece: Piece, to: Coordinate): boolean {
        let from = piece.coordinate;
        if (to.piece === null && piece.getMoveOptions().indexOf(to) >= 0) {
            from.piece = null;
            to.piece = piece;
            piece.coordinate = to;
            this.onMove(from, to, true);
            return true;
        }
        return false;
    }
    public unMove(move: GameMove) {
        const from = this.getCoordAt(move.fromX, move.fromY);
        const to = this.getCoordAt(move.toX, move.toY);
        const piece = to.piece;
        if (!!from.piece || !to.piece) {
            throw "Error";
        }
        from.piece = piece;
        to.piece = null;
        return true;
    }
    public moveWithoutCallback(piece: Piece, to: Coordinate): boolean {
        let from = piece.coordinate;
        if (to.piece === null) {
            from.piece = null;
            to.piece = piece;
            piece.coordinate = to;
            this.switchPlayer();
            if (!to.piece || to.piece.coordinate !== to) {
                throw "Error";
            }
            return true;
        }
        return false;
    }
    get getRedPieces(): Piece[] {
        return this.redPieces;
    }

    get getBlackPieces(): Piece[] {
        return this.blackPieces;
    }

    public kill(attack: Piece, attackee: Piece) {
        if (attack.getKillOptions().indexOf(attackee.coordinate) >= 0) {
            const from = attack.coordinate;
            const to = attackee.coordinate;
            to.piece = attack;
            from.piece = null;
            attackee.state = PieceState.Dead;
            attackee.coordinate = null;
            if (attackee instanceof General) {
                this.isGameOver = true;
                this.GameStatus = eGameStatus.NotStart;
                this.winner = attackee.player === Player.Red ? Player.Black : Player.Red;
            }
            this.onMove(from, to, false);
            return true;
        }
        return false;
    }
    public killWithoutCallBack(attack: Piece, attackee: Piece) {
        if (attack === null || attackee === null) {
            throw "Error";
        }
        if (attack.getKillOptions().indexOf(attackee.coordinate) >= 0) {
            const from = attack.coordinate;
            const to = attackee.coordinate;
            to.piece = attack;
            from.piece = null;
            attackee.state = PieceState.Dead;
            attackee.coordinate = null;
            if (!to.piece || to.piece.coordinate !== to) {
                throw "Error";
            }
            this.switchPlayer();
            return true;
        }
        return false;
    }
    public unkill(move: GameMove) {
        const from = this.getCoordAt(move.fromX, move.fromY);
        const to = this.getCoordAt(move.toX, move.toY);
        const deadPiece = this.getPieceById(move.deadPieceId);
        const piece = to.piece;
        from.piece = piece;
        to.piece = deadPiece;
        deadPiece.state = PieceState.Active;
        this.onMove(from, to, false);
        return true;
    }
    public unkillWithoutCallBack(attack: Piece, attackee: Piece, fromX: number, fromY: number) {
        const from = this.getCoordAt(fromX, fromY);
        const to = attack.coordinate;
        from.piece = attack;
        attack.coordinate = from;
        to.piece = attackee;
        attackee.state = PieceState.Active;
        attackee.coordinate = to;
        if (!to.piece || to.piece.coordinate !== to) {
            throw "Error";
        }
        if (!from.piece || from.piece.coordinate !== from) {
            throw "Error";
        }
        this.switchPlayer();
        return true;
    }
    public selectPiece(piece: Piece) {
        if (this.activePlayer !== this.player) {
            return;
        }

        if (piece.isHaloed()) {
            if (!!this.selectedPiece) {
                this.kill(this.selectedPiece, piece);
                // this.onMove(this.selectedPiece.coordinate, piece.coordinate, false);
                this.selectedPiece = null;
                // this.switchPlayer();
            }
            //  this.setDefaultState();
        } else {
            if (this.selectedPiece === piece || piece.player !== this.activePlayer) {
                this.selectedPiece = null;
                this.setDefaultState();
            } else {
                this.selectedPiece = piece;
            }

            if (this.selectedPiece != null) {
                let player = this.selectedPiece.player;
                this.setDefaultState();
                this.selectedPiece.getMoveOptions().forEach(coord => {
                    coord.state =
                        player === Player.Black ? CoordinateState.HighlightBlack : CoordinateState.HighlightRed;
                });
                this.selectedPiece.getKillOptions().forEach(coord => {
                    let targetPiece = coord.piece;
                    if (targetPiece !== null) {
                        targetPiece.state = PieceState.Haloed;
                    }
                });
            }
        }
    }

    /**
     * name
     */
    public selectIntersection(coord: Coordinate) {
        if (!!this.selectedPiece) {
            if (this.move(this.selectedPiece, coord)) {
                // this.setDefaultState();
                // this.switchPlayer();
            } else {
                throw "Invalid Move";
            }
        }
    }

    private switchPlayer() {
        this.activePlayer = (this.activePlayer === Player.Black) ? Player.Red : Player.Black;
    }
    private setDefaultState() {
        this.coords.forEach((cols) => {
            cols.forEach((x) => {
                x.state = CoordinateState.Hidden;
            });
        });
        this.getPieces.forEach(x => {
            if (x.state !== PieceState.Dead) {
                x.state = PieceState.Active;
            }
        });
    }

    private generatePieces(player: Player): Piece[] {
        var coordinates = this.coords;
        if (player === Player.Red) {
            var redPieces: Piece[] = [
                new Chariot(coordinates[0][0], player, this),
                new Chariot(coordinates[8][0], player, this),
                new Knight(coordinates[1][0], player, this),
                new Knight(coordinates[7][0], player, this),

                new Minister(coordinates[2][0], player, this),
                new Minister(coordinates[6][0], player, this),

                new Guard(coordinates[3][0], player, this),
                new Guard(coordinates[5][0], player, this),

                new General(coordinates[4][0], player, this),

                new Cannon(coordinates[1][2], player, this),
                new Cannon(coordinates[7][2], player, this),

                new Pawn(coordinates[0][3], player, this),
                new Pawn(coordinates[2][3], player, this),
                new Pawn(coordinates[4][3], player, this),
                new Pawn(coordinates[6][3], player, this),
                new Pawn(coordinates[8][3], player, this)
            ];
            return redPieces;
        } else {
            var blackPieces = [
                new Chariot(coordinates[0][9], player, this),
                new Chariot(coordinates[8][9], player, this),

                new Knight(coordinates[1][9], player, this),
                new Knight(coordinates[7][9], player, this),

                new Minister(coordinates[2][9], player, this),
                new Minister(coordinates[6][9], player, this),

                new Guard(coordinates[3][9], player, this),
                new Guard(coordinates[5][9], player, this),

                new General(coordinates[4][9], player, this),

                new Cannon(coordinates[1][7], player, this),
                new Cannon(coordinates[7][7], player, this),

                new Pawn(coordinates[0][6], player, this),
                new Pawn(coordinates[2][6], player, this),
                new Pawn(coordinates[4][6], player, this),
                new Pawn(coordinates[6][6], player, this),
                new Pawn(coordinates[8][6], player, this)
            ];
            return blackPieces;
        }
    }

    get getCoordinates(): Array<Coordinate[]> {
        return this.coords;
    }

    public clone(): ChessGame {
        let newChessGame = new ChessGame();
        newChessGame.getCoordinates.forEach(x => x.forEach(c => c.piece = null));
        newChessGame.getAllPieces.map((x, index) => [x, this.getAllPieces[index]]).map(y => {
            if (y[1].state !== PieceState.Dead) {
                y[0].coordinate = newChessGame.getCoordAt(y[1].coordinate.x, y[1].coordinate.y);
                y[0].coordinate.piece = y[0];
            } else {
                y[0].coordinate = null;
            }
            y[0].state = y[1].state;
            return y[0];
        });
        return newChessGame;
    }
}