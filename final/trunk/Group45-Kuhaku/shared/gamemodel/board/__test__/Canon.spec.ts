import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
import { Piece } from '../pieces';
import { Cannon } from '../pieces/Cannon';
import { Coordinate } from '../Coordinate';
const game = new ChessGame();
describe('Test cannon optional move fucntion ', () => {
    it('get Cannon', () => {
        let cannonRed1 = game.getCoordAt(1, 2);
        let cannonRed2 = game.getCoordAt(7, 2);
        let cannonBlack1 = game.getCoordAt(7, 7);
        let cannonBlack2 = game.getCoordAt(1, 7);
        if (cannonRed1.piece !== null) {
            expect(cannonRed1.piece.player).toBe(Player.Red);
            expect(cannonRed1.piece.name).toBe('炮');

        }
        if (cannonRed2.piece !== null) {
            expect(cannonRed2.piece.player).toBe(Player.Red);
            expect(cannonRed2.piece.name).toBe('炮');

        }
        if (cannonBlack1.piece !== null) {
            expect(cannonBlack1.piece.player).toBe(Player.Black);
            expect(cannonBlack1.piece.name).toBe('砲');

        }
        if (cannonBlack2.piece !== null) {
            expect(cannonBlack2.piece.player).toBe(Player.Black);
            expect(cannonBlack2.piece.name).toBe('砲');

        }
    });

    it('quantity of possible move', () => {
        let cannonRed1 = game.getCoordAt(1, 2);
        let cannonRed2 = game.getCoordAt(7, 2);
        let cannonBlack1 = game.getCoordAt(7, 7);
        let cannonBlack2 = game.getCoordAt(1, 7);
        if (cannonRed1.piece !== null) {
            let moveOption = cannonRed1.piece.getMoveOptions();
            expect(moveOption.length).toBe(11);
        }
        if (cannonRed2.piece !== null) {
            let moveOption = cannonRed2.piece.getMoveOptions();
            expect(moveOption.length).toBe(11);
        }
        if (cannonBlack1.piece !== null) {
            let moveOption = cannonBlack1.piece.getMoveOptions();
            expect(moveOption.length).toBe(11);
        }
        if (cannonBlack2.piece !== null) {
            let moveOption = cannonBlack2.piece.getMoveOptions();
            expect(moveOption.length).toBe(11);
        }
    });

    it('Test move Option', () => {
        let cannons = new Array<Coordinate>();
        cannons.push(game.getCoordAt(1, 2));
        cannons.push(game.getCoordAt(7, 2));
        cannons.push(game.getCoordAt(1, 7));
        cannons.push(game.getCoordAt(7, 7));
        let possibleMove = [
            [[1, 1], [1, 3], [1, 4], [1, 5], [1, 6], [0, 2], [2, 2], [3, 2], [4, 2], [5, 2], [6, 2]],
            [[7, 1], [7, 3], [7, 4], [7, 5], [7, 6], [8, 2], [6, 2], [5, 2], [4, 2], [3, 2], [2, 2]],
            [[1, 8], [1, 3], [1, 4], [1, 5], [1, 6], [0, 7], [2, 7], [3, 7], [4, 7], [5, 7], [6, 7]],
            [[7, 8], [7, 3], [7, 4], [7, 5], [7, 6], [8, 7], [2, 7], [3, 7], [4, 7], [5, 7], [6, 7]]];
        cannons.forEach((cannon, index) => {
            if (cannon.piece !== null) {
                let moveOption = cannon.piece.getMoveOptions();

                expect(possibleMove[index].reduce(
                    (check, arr) => {
                        if (!check) {
                            return check;
                        } else {
                            let finder = moveOption.find((coord) => {
                                if (coord.x === arr[0] && coord.y === arr[1]) {
                                    return true;
                                } else {
                                    return false;
                                }
                            });
                            if (finder === undefined) {
                                return false;
                            } else { return check; }
                        }
                    },
                    true)).toBe(true);
            }
        });
    });
 
});

describe('Test cannon move fucntion ', () => {
    it('Test Move ', () => {
        let cannons = game.getPieces.filter((piece) => {
            return piece instanceof Cannon ;
        });
        expect( game.move(cannons[0], game.getCoordAt(1, 5))).toBe(true);
        expect( game.move(cannons[0], game.getCoordAt(3, 5))).toBe(true);
        expect( game.move(cannons[0], game.getCoordAt(3, 8))).toBe(true);
        expect( game.move(cannons[2], game.getCoordAt(1, 8))).toBe(true);
        expect( game.move(cannons[0], game.getCoordAt(1, 8))).toBe(false);
        expect( game.move(cannons[2], game.getCoordAt(1, 2))).toBe(true);
        expect( game.move(cannons[2], game.getCoordAt(7, 2))).toBe(false);
        expect( game.move(cannons[2], game.getCoordAt(6, 2))).toBe(true);
    });
});
