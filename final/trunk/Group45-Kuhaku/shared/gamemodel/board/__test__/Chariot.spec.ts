import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
import { Piece, Chariot } from '../pieces';
import { Cannon } from '../pieces/Cannon';
import { Coordinate } from '../Coordinate';
const game = new ChessGame();
describe('Test chariot optional move fucntion ', () => {
    it('get Chariot', () => {
        let chariots = game.getPieces.filter((piece) => {
            return piece instanceof Chariot;
        });
    });

    it('quantity of possible move', () => {
        let chariots = game.getPieces.filter((piece) => {
            return piece instanceof Chariot;
        });

        chariots.forEach((piece) => {
            let moveOption = piece.getMoveOptions();
            expect(moveOption.length).toBe(2);
        });

    });

    it('Test move Option', () => {
        let chariots = game.getPieces.filter((piece) => {
            return piece instanceof Chariot;
        });
        let possibleMove = [
            [[0, 1], [0, 2]],
            [[8, 1], [8, 2]],
            [[0, 7], [0, 8]],
            [[8, 7], [8, 8]]];
        chariots.forEach((chariot, index) => {
            let moveOption = chariot.getMoveOptions();
            expect(possibleMove[index].reduce(
                (check, arr) => {
                    if (!check) {
                        return check;
                    } else {
                        let finder = moveOption.find((coord) => {
                            if (coord.x === arr[0] && coord.y === arr[1]) {
                                return true;
                            } else {
                                return false;
                            }
                        });
                        if (finder === undefined) {
                            return false;
                        } else { return check; }
                    }
                },
                true)).toBe(true);
        });
    });

});

describe('Test chariot move  ', () => {
    it('Move verticaly ', () => {
        // game.reset();
        let chariots = game.getPieces.filter((piece) => {
            return piece instanceof Chariot;
        });
        expect(game.getCoordAt(0, 1).piece).toBe(null);
        expect(game.move(chariots[0], game.getCoordAt(0, 1))).toBe(true);
        expect(game.move(chariots[0], game.getCoordAt(0, 4))).toBe(false);
        expect(game.move(chariots[0], game.getCoordAt(0, 2))).toBe(true);
        expect(game.move(chariots[0], game.getCoordAt(0, 0))).toBe(true);
    });

    it('Move horizontaly ', () => {
        game.reset();
        let chariots = game.getPieces.filter((piece) => {
            return piece instanceof Chariot;
        });
        expect(chariots[0].coordinate.x).toBe(0);
        expect(chariots[0].coordinate.y).toBe(0);
    });

    it('Move on current possition ', () => {
        game.reset();
        let chariots = game.getPieces.filter((piece) => {
            return piece instanceof Chariot;
        });
       
        expect(game.move(chariots[0], game.getCoordAt(0, 0))).toBe(false);
        expect(game.move(chariots[0], game.getCoordAt(8, 0))).toBe(false);
        expect(game.move(chariots[0], game.getCoordAt(0, 9))).toBe(false);
        expect(game.move(chariots[0], game.getCoordAt(8, 9))).toBe(false);
    });
});
