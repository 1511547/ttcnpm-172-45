import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
const game = new ChessGame();
describe('Test chess game', () => {

    // red cannon from begining position
    it("Red cannon(1,2) from begining position guards no one", () => {
        const cannon = game.getCoordAt(1, 2);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            expect(cannon.piece.player).toBe(Player.Red);
            expect(cannon.piece.getGuardOptions().length).toBe(0);
        }
    });
    it("Red cannon(7,2) from begining position guards no one", () => {
        const cannon = game.getCoordAt(1, 2);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            expect(cannon.piece.player).toBe(Player.Red);
            expect(cannon.piece.getGuardOptions().length).toBe(0);
        }
    });
    it("Red cannon(1,3) guards a pawn", () => {
        // Get cannon (1,2)
        let cannon = game.getCoordAt(1, 2);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            // Move canon to (1,3)
            cannon.piece.coordinate.x = 1;
            cannon.piece.coordinate.y = 3;
            expect(cannon.piece.player).toBe(Player.Red);
            expect(cannon.piece.getGuardOptions().length).toBe(1);
        }
    });
    it("Red cannon(3,3) guards two pawn", () => {
        // Get cannon (1,2)
        let cannon = game.getCoordAt(1, 2);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            // Move canon to (3,3)
            cannon.piece.coordinate.x = 3;
            cannon.piece.coordinate.y = 3;
            expect(cannon.piece.player).toBe(Player.Red);
            expect(cannon.piece.getGuardOptions().length).toBe(2);
        }
    });
    it("Red cannon(3,3) guards two pawn", () => {
        // Get cannon (1,2)
        let cannon = game.getCoordAt(1, 2);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            // Move canon to (3,3)
            cannon.piece.coordinate.x = 3;
            cannon.piece.coordinate.y = 3;
            expect(cannon.piece.player).toBe(Player.Red);
            expect(cannon.piece.getGuardOptions().length).toBe(2);
        }
    });
});
