import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
const game = new ChessGame();
describe('Test chess game', () => {

    // red pawn from begining position 
    it("Red Pawn(0,3) from begining position", () => {
        const pawn = game.getCoordAt(0, 3);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });

    it("Red Pawn(2,3) from begining position", () => {
        const pawn = game.getCoordAt(2, 3);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });

    it("Red Pawn(4,3) from begining position", () => {
        const pawn = game.getCoordAt(4, 3);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });

    it("Red Pawn(6,3) from begining position", () => {
        const pawn = game.getCoordAt(6, 3);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });

    it("Red Pawn(8,3) from begining position", () => {
        const pawn = game.getCoordAt(8, 3);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });
    ///////////////////////////////////////////////////////////////

    // black pawn from begining position
    it("Red Pawn(0,3) from begining position", () => {
        const pawn = game.getCoordAt(0, 6);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });

    it("Red Pawn(2,3) from begining position", () => {
        const pawn = game.getCoordAt(2, 6);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });

    it("Red Pawn(4,3) from begining position", () => {
        const pawn = game.getCoordAt(4, 6);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });

    it("Red Pawn(6,3) from begining position", () => {
        const pawn = game.getCoordAt(6, 6);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });

    it("Red Pawn(8,3) from begining position", () => {
        const pawn = game.getCoordAt(8, 6);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });
    /////////////////////////////////////////////////////////////

    it("Red Pawn(0,3) go up to (0,4)", () => {
        let pawn = game.getCoordAt(0, 3);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            // before move
            expect(pawn.piece.getKillOptions().length).toBe(0);
            expect(pawn.piece.getMoveOptions().length).toBe(1);
            expect(game.move(pawn.piece, game.getCoordAt(0, 4))).toBe(true); 
                  
        }
        
    });

    it("Red Pawn(0,4) go up to (0, 5) ", () => {
        let pawn = game.getCoordAt(0, 4);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            // before move
            expect(pawn.piece.getKillOptions().length).toBe(0);
            expect(pawn.piece.getMoveOptions().length).toBe(1);

            // move
            expect(game.move(pawn.piece, game.getCoordAt(0, 5))).toBe(true);  
                  
        }
    });

    it("Red Pawn(0,5) can kill black pawn(0,6)", () => {
        let pawn = game.getCoordAt(0, 5);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            // before move
            expect(pawn.piece.getKillOptions().length).toBe(1);
            expect(pawn.piece.getMoveOptions().length).toBe(1);

        }
    });

    it("Red Pawn(0,5) kill black pawn(0,6)", () => {
        let attack = game.getCoordAt(0, 5);
        let guard = game.getCoordAt(0, 6);
        expect(attack.piece).not.toBe(null);
        expect(guard.piece).not.toBe(null);
        if (attack.piece !== null && guard.piece != null) {
            expect(game.kill(attack.piece, guard.piece)).toBe(true);
        }
    });

    it("Red Pawn(0,6) check after kill", () => {
        let pawn = game.getCoordAt(0, 6);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            // check pawn(0, 6) is red and name === pawn
            expect(pawn.piece.player).toBe(Player.Red);
            expect(pawn.piece.isDead()).toBe(false);
        }
    });

    it("Red Pawn(0,6) move to (1,6)", () => {
        let pawn = game.getCoordAt(0, 6);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getMoveOptions().length).toBe(2);
            expect(pawn.piece.getKillOptions().length).toBe(0);

            // move
            expect(game.move(pawn.piece, game.getCoordAt(1, 6))).toBe(true);
        }
    });

    it("Red Pawn(1,6) can kill 2 piece", () => {
        let pawn = game.getCoordAt(1, 6);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getMoveOptions().length).toBe(1);
            expect(pawn.piece.getKillOptions().length).toBe(2);

        }
    });
    
    it("Red Pawn(2,3) go up to (2,4)", () => {
        let pawn = game.getCoordAt(2, 3);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            // before move
            expect(pawn.piece.getKillOptions().length).toBe(0);
            expect(pawn.piece.getMoveOptions().length).toBe(1);

            // move
            expect(game.move(pawn.piece, game.getCoordAt(2, 4))).toBe(true); 
                  
        }
    });

    it("Black Pawn(2,6) go down to (2,5)", () => {
        let pawn = game.getCoordAt(2, 6);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            // before move
            expect(pawn.piece.getKillOptions().length).toBe(0);
            expect(pawn.piece.getMoveOptions().length).toBe(1);

            // move
            expect(game.move(pawn.piece, game.getCoordAt(2, 5))).toBe(true); 
                  
        }
    });

    it("Black at Pawn(2,5) and Red Pawn at (2, 4). check move and kill option", () => {
        let blackPawn = game.getCoordAt(2, 5);
        let redPawn = game.getCoordAt(2, 4);
        expect(blackPawn.piece).not.toBe(null);
        expect(redPawn.piece).not.toBe(null);
        
        if (blackPawn.piece !== null && redPawn.piece !== null) {
            // black
            expect(blackPawn.piece.getKillOptions().length).toBe(1);
            expect(blackPawn.piece.getMoveOptions().length).toBe(0);
            
            // red
            expect(redPawn.piece.getKillOptions().length).toBe(1);
            expect(redPawn.piece.getMoveOptions().length).toBe(0);
        }
    });

    it("black pawn(2,5) kill red pawn(2,4)", () => {
        let attack = game.getCoordAt(2, 5);
        let guard = game.getCoordAt(2, 4);
        expect(attack.piece).not.toBe(null);
        expect(guard.piece).not.toBe(null);
        if (attack.piece !== null && guard.piece !== null) {
            expect(game.kill(attack.piece, guard.piece)).toBe(true);

        }
    });

    it("black Pawn(2,4) check after kill", () => {
        let pawn = game.getCoordAt(2, 4);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            // check pawn(0, 6) is red and name === pawn
            expect(pawn.piece.player).toBe(Player.Black);
            expect(pawn.piece.isDead()).toBe(false);
        }
    });

    it("Red Pawn(6,3) go up to (6,4)", () => {
        let pawn = game.getCoordAt(6, 3);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            // before move
            expect(pawn.piece.getKillOptions().length).toBe(0);
            expect(pawn.piece.getMoveOptions().length).toBe(1);
            expect(game.move(pawn.piece, game.getCoordAt(6, 4))).toBe(true); 
                  
        }
        
    });

    it("Red Pawn(6,4) go up to (6, 5) ", () => {
        let pawn = game.getCoordAt(6, 4);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            // before move
            expect(pawn.piece.getKillOptions().length).toBe(0);
            expect(pawn.piece.getMoveOptions().length).toBe(1);

            // move
            expect(game.move(pawn.piece, game.getCoordAt(6, 5))).toBe(true);  
                  
        }
    });

    it("Red Pawn(6,5) check move and kill set", () => {
        let pawn = game.getCoordAt(6, 5);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            // before move
            expect(pawn.piece.getKillOptions().length).toBe(1);
            expect(pawn.piece.getMoveOptions().length).toBe(2);

        }
    });

    it("Red Pawn(6,5) turn right (7,5) ", () => {
        let pawn = game.getCoordAt(6, 5);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(game.move(pawn.piece, game.getCoordAt(7, 5))).toBe(true);
        }
    });

    it("Red Pawn (7,5) check move set and kill set ", () => {
        let pawn = game.getCoordAt(7, 5);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
            expect(pawn.piece.getMoveOptions().length).toBe(3);
        }
    });

    it("Red Pawn(7,5) go up => (7,6) ", () => {
        let pawn = game.getCoordAt(7, 5);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(game.move(pawn.piece, game.getCoordAt(7, 6))).toBe(true);
        }
    });

    it("Red Pawn(7,6) check move set and kill set", () => {
        let pawn = game.getCoordAt(7, 6);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(3);
            expect(pawn.piece.getMoveOptions().length).toBe(0);
        }
    });

    it("Black Pawn(4,6) go up to (4,4) move two times", () => {
        let pawn = game.getCoordAt(4, 6);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(game.move(pawn.piece, game.getCoordAt(4, 5))).toBe(true);
        }

        pawn = game.getCoordAt(4, 5);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(game.move(pawn.piece, game.getCoordAt(4, 4))).toBe(true);
        }
    });
    it("black pawn(4,4) check move set and kill set ", () => {
        let pawn = game.getCoordAt(4, 4);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(1);
            expect(pawn.piece.getMoveOptions().length).toBe(2);
        }
    });

    it("black pawn(4,4) kill red pawn(4,3) ", () => {
        let attack = game.getCoordAt(4, 4);
        let guard = game.getCoordAt(4, 3);
        expect(attack.piece).not.toBe(null);
        expect(guard.piece).not.toBe(null);
        if (attack.piece !== null && guard.piece !== null) {
            expect(game.kill(attack.piece, guard.piece)).toBe(true);

        }
    });

    it("black Pawn(4,3) check after kill", () => {
        let pawn = game.getCoordAt(4, 3);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.player).toBe(Player.Black);
            expect(pawn.piece.isDead()).toBe(false);
        }
    });
});
