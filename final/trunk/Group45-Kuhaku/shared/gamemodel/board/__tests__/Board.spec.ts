import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
const game = new ChessGame();
describe('Test chess game', () => {
    it('32 piece in board', () => {
        const count = game.getPieces.length;
        expect(count).toBe(32);
    });
    it('get red Chariot', () => {
        let chariotRed1 = game.getCoordAt(0, 0);
        let chariotRed2 = game.getCoordAt(8, 0);
        expect(chariotRed1.piece).toBeDefined();
        expect(chariotRed2.piece).toBeDefined();
        if (chariotRed1.piece !== null) {
            expect(chariotRed1.piece.player).toBe(Player.Red);
        }
        if (chariotRed2.piece !== null) {

            expect(chariotRed2.piece.player).toBe(Player.Red);
        }
    });
    it("get red general", () => {
        const general = game.getCoordAt(4, 0);
        expect(general.piece).not.toBe(null);
        if (general.piece !== null) {
            expect(general.piece.player).toBe(Player.Red);
        }
    });
    it("Clone game pieces test", () => {
        let clone = game.clone();
        let newPieces = clone.getAllPieces;
        game.getAllPieces.forEach((p, i) => {
            let np = newPieces[i];
            expect(p.name).toBe(np.name);
            expect(p.coordinate.x).toBe(np.coordinate.x);
            expect(p.coordinate.y).toBe(np.coordinate.y);
        });
    });
});
