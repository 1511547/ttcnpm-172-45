import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
const game = new ChessGame();
describe('Test chess game clone', () => {
    it('Clone and check', () => {
        let chessGame1 = new ChessGame();
        let chessGame2 = chessGame1.clone();
        chessGame2.getPieces[0].coordinate.x = 6;
        chessGame2.getPieces[0].coordinate.y = 4;
        expect(chessGame1 === chessGame2).toBeFalsy();
        expect(chessGame1.getPieces[0].coordinate.x === chessGame2.getPieces[0].coordinate.x).toBeFalsy();
        expect(chessGame1.getPieces[0].coordinate.y === chessGame2.getPieces[0].coordinate.y).toBeFalsy();
        expect(chessGame1.getPieces === chessGame2.getPieces).toBeFalsy();
        expect(chessGame1.getPieces[0] === chessGame2.getPieces[0]).toBeFalsy();
    });
});
