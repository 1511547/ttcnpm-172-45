import 'jest';
import { ChessGame } from '../ChessGame';
const game = new ChessGame();
describe('Test chess game', () => {
    // red knight from begining position
    it("red knight(1,0) from begining position", () => {
        const knight = game.getCoordAt(1, 0);
        expect(knight.piece).not.toBe(null);
        if (knight.piece !== null) {
            expect(knight.piece.getKillOptions().length).toBe(0);
        }
    });
    it("red knight(7,0) from begining position", () => {
        const knight = game.getCoordAt(7, 0);
        expect(knight.piece).not.toBe(null);
        if (knight.piece !== null) {
            expect(knight.piece.getKillOptions().length).toBe(0);
        }
    });

    // black knight from begining position
    it("black knight(1,9) from begining position", () => {
        const knight = game.getCoordAt(1, 9);
        expect(knight.piece).not.toBe(null);
        if (knight.piece !== null) {
            expect(knight.piece.getKillOptions().length).toBe(0);
        }
    });
    it("black knight(7,9) from begining position", () => {
        const knight = game.getCoordAt(7, 9);
        expect(knight.piece).not.toBe(null);
        if (knight.piece !== null) {
            expect(knight.piece.getKillOptions().length).toBe(0);
        }
    });

});
