import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
const game = new ChessGame();
describe('Test chess game', () => {
    
    // red chariot from begining position
    it("Red Chariot(0,0) from begining position", () => {
        const chariot = game.getCoordAt(0, 0);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            expect(chariot.piece.getKillOptions().length).toBe(0);
        }
    });
    it("Red Chariot(8,0) from begining position", () => {
        const chariot = game.getCoordAt(8, 0);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            expect(chariot.piece.getKillOptions().length).toBe(0);
        }
    });

    // black chariot from begining position
    it("Black Chariot(0,9) from begining position", () => {
        const chariot = game.getCoordAt(0, 9);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            expect(chariot.piece.getKillOptions().length).toBe(0);
        }
    });
    it("Black Chariot(8,9) from begining position", () => {
        const chariot = game.getCoordAt(8, 9);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            expect(chariot.piece.getKillOptions().length).toBe(0);
        }
    });

    it("Black Chariot(8,9) move to (8,8)", () => {
        const chariot = game.getCoordAt(8, 9);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            expect(game.move(chariot.piece, game.getCoordAt(8, 8))).toBe(true);
        }
    });

    it("Black Chariot(8,8) move to (5,8)", () => {
        const chariot = game.getCoordAt(8, 8);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            // kill set at (8, 8)
            expect(chariot.piece.getKillOptions().length).toBe(0);
            // move to (5, 8)
            expect(game.move(chariot.piece, game.getCoordAt(5, 8))).toBe(true);
        }
    });
    
    it("Black Chariot(5,8) kill set", () => {
        const chariot = game.getCoordAt(5, 8);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            // kill set at (5, 8)
            expect(chariot.piece.getKillOptions().length).toBe(1);
        }
    });

    it("Black Chariot(5,8) move to (5,2)", () => {
        const chariot = game.getCoordAt(5, 8);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            // move to (5, 2)
            expect(game.move(chariot.piece, game.getCoordAt(5, 2))).toBe(true);
        }
    });

    it("Black Chariot(5,2) kill set", () => {
        const chariot = game.getCoordAt(5, 2);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            // kill set at (5, 2)
            expect(chariot.piece.getKillOptions().length).toBe(3);
        }
    });

    it("Black Chariot(5,2) move to (4,2)", () => {
        const chariot = game.getCoordAt(5, 2);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            // move to (4, 2)
            expect(game.move(chariot.piece, game.getCoordAt(4, 2))).toBe(true);
        }
    });

    it("Black Chariot(4,2) kill set", () => {
        const chariot = game.getCoordAt(4, 2);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            // kill set at (4, 2)
            expect(chariot.piece.getKillOptions().length).toBe(4);
        }
    });

    it("red cannon(1,2) move to (1,5)", () => {
        const chariot = game.getCoordAt(1, 2);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            // move to (1, 5)
            expect(game.move(chariot.piece, game.getCoordAt(1, 5))).toBe(true);
        }
    });

    it("Red Chariot(0,0) move to (0,2)", () => {
        const chariot = game.getCoordAt(0, 0);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            // move to (0, 2)
            expect(game.move(chariot.piece, game.getCoordAt(0, 2))).toBe(true);
        }
    });

    it("red Chariot(0,2) kill set", () => {
        const chariot = game.getCoordAt(0, 2);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            // kill set at (0, 2)
            expect(chariot.piece.getKillOptions().length).toBe(1);
        }
    });

    it("Black Chariot(4,2) kill red Chariot(0,2)", () => {
        const guard = game.getCoordAt(0, 2);
        const attack = game.getCoordAt(4, 2);
        expect(guard.piece).not.toBe(null);
        expect(attack.piece).not.toBe(null);
        
        if (guard.piece !== null && attack.piece !== null) {
            // kill
            expect(game.kill(attack.piece, guard.piece)).toBe(true);
        }
    });

    it("Black Chariot(0,2) kill set", () => {
        const chariot = game.getCoordAt(0, 2);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            // player
            expect(chariot.piece.player).toBe(Player.Black);
            // kill set at (0, 2)
            expect(chariot.piece.getKillOptions().length).toBe(2);
        }
    });

    it("Red Cannon(7,2) move to (7, 3)", () => {
        const cannon = game.getCoordAt(7, 2);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            // move to (0, 2)
            expect(game.move(cannon.piece, game.getCoordAt(7, 3))).toBe(true);
        }
    });

    it("Black Chariot(0,2) kill set", () => {
        const chariot = game.getCoordAt(0, 2);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            // player
            expect(chariot.piece.player).toBe(Player.Black);
            // kill set at (0, 2)
            expect(chariot.piece.getKillOptions().length).toBe(1);
        }
    });

    it("Red Knight(1,0) move to (2, 2)", () => {
        const knight = game.getCoordAt(1, 0);
        expect(knight.piece).not.toBe(null);
        if (knight.piece !== null) {
            // move to (2, 2)
            expect(game.move(knight.piece, game.getCoordAt(2, 2))).toBe(true);
        }
    });

    it("Black Chariot(0,2) kill set", () => {
        const chariot = game.getCoordAt(0, 2);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            // player
            expect(chariot.piece.player).toBe(Player.Black);
            // kill set at (0, 2)
            expect(chariot.piece.getKillOptions().length).toBe(2);
        }
    });
});
