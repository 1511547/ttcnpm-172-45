import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
const game = new ChessGame();
describe('Test chess game', () => {
    // red general from begining position
    it("red general(4,0) from begining position", () => {
        const general = game.getCoordAt(4, 0);
        expect(general.piece).not.toBe(null);
        if (general.piece !== null) {
            expect(general.piece.getKillOptions().length).toBe(0);
        }
    });
    ///////////////////////////////////////////////////////////////

    // black general from begining position
    it("black general(4,9) from begining position", () => {
        const general = game.getCoordAt(4, 9);
        expect(general.piece).not.toBe(null);
        if (general.piece !== null) {
            expect(general.piece.getKillOptions().length).toBe(0);
        }
    });

    it("black general(4,9) move to 4,8 and get moves", () => {
        const general = game.getCoordAt(4, 9);
        let coord = game.getCoordAt(4, 8);
        game.move(general.piece, coord);
        if (coord.piece !== null) {
            expect(coord.piece.getMoveOptions().length).toBe(4);
        }
    });
    it("red general(4,0) move to 4,1 and get moves", () => {
        const general = game.getCoordAt(4, 0);
        let coord = game.getCoordAt(4, 1);
        game.move(general.piece, coord);
        if (coord.piece !== null) {
            expect(coord.piece.getMoveOptions().length).toBe(4);
        }
    });

    it("black general(4,8) move to 5,8 and get moves", () => {
        const general = game.getCoordAt(4, 8);
        let coord = game.getCoordAt(5, 8);
        game.move(general.piece, coord);
        if (coord.piece !== null) {
            expect(coord.piece.getMoveOptions().length).toBe(2);
        }
    });
    it("red general(4,1) move to 5,1 and get moves", () => {
        const general = game.getCoordAt(4, 1);
        let coord = game.getCoordAt(5, 1);
        game.move(general.piece, coord);
        if (coord.piece !== null) {
            expect(coord.piece.getMoveOptions().length).toBe(2);
        }
    });

    it("red general(5,1) and 5,8 can kill each orther", () => {
        const general = game.getCoordAt(5, 1);
        let coord = game.getCoordAt(5, 8);
        if (general.piece !== null) {
            expect(general.piece.getKillOptions().length).toBe(1);
        }
    });
    it("red general(5,8) and 5,1 can kill each orther", () => {
        const general = game.getCoordAt(5, 1);
        let coord = game.getCoordAt(5, 8);
        game.move(general.piece, coord);
        if (coord.piece !== null) {
            expect(coord.piece.getKillOptions().length).toBe(1);
        }
    });

    it("black general(5,8) move to 4,8 and get moves", () => {
        const general = game.getCoordAt(5, 8);
        let coord = game.getCoordAt(4, 8);
        game.move(general.piece, coord);
        if (coord.piece !== null) {
            expect(coord.piece.getMoveOptions().length).toBe(4);
        }
    });

    it("red general(5,1) move to 4,1 and get moves", () => {
        const general = game.getCoordAt(5, 1);
        let coord = game.getCoordAt(4, 1);
        game.move(general.piece, coord);
        if (coord.piece !== null) {
            expect(coord.piece.getMoveOptions().length).toBe(4);
        }
    });

    it("black general(4,8) move to 3,8 and get moves", () => {
        const general = game.getCoordAt(4, 8);
        let coord = game.getCoordAt(3, 8);
        game.move(general.piece, coord);
        if (coord.piece !== null) {
            expect(coord.piece.getMoveOptions().length).toBe(2);
        }
    });
    it("red general(4,1) move to 3,1 and get moves", () => {
        const general = game.getCoordAt(4, 1);
        let coord = game.getCoordAt(3, 1);
        game.move(general.piece, coord);
        if (coord.piece !== null) {
            expect(coord.piece.getMoveOptions().length).toBe(2);
        }
    });
});
