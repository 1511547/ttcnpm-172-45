import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
const game = new ChessGame();
describe('Test chess game', () => {

    it("Red Knight(1,0) from begining position guards no one", () => {
        const knight = game.getCoordAt(1, 0);
        expect(knight.piece).not.toBe(null);
        if (knight.piece !== null) {
            expect(knight.piece.player).toBe(Player.Red);
            expect(knight.piece.getGuardOptions().length).toBe(0);
        }
    });

    it("Red Knight(7,0) from begining position guards no one", () => {
        const knight = game.getCoordAt(7, 0);
        expect(knight.piece).not.toBe(null);
        if (knight.piece !== null) {
            expect(knight.piece.player).toBe(Player.Red);
            expect(knight.piece.getGuardOptions().length).toBe(0);
        }
    });

    it("Red Knight(2,2) guards a Pawn(4,3) and a Guard(3, 0)", () => {
        // First get the knight at (1, 0)
        let knight = game.getCoordAt(1, 0);
        expect(knight.piece).not.toBe(null);
        if (knight.piece !== null) {
            // Then move it to (2, 2)
            knight.x = 2;
            knight.y = 2;
            expect(knight.piece.player).toBe(Player.Red);
            expect(knight.piece.getGuardOptions().length).toBe(2);
        }
    });
    
});