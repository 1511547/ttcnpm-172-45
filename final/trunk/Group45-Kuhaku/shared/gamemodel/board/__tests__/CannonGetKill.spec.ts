import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
const game = new ChessGame();
describe('Test chess game', () => {

    // red cannon from begining position
    it("Red cannon(1,2) from begining position", () => {
        const cannon = game.getCoordAt(1, 2);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            expect(cannon.piece.player).toBe(Player.Red);
            expect(cannon.piece.getKillOptions().length).toBe(1);
        }
    });
    it("Red cannon(7,2) from begining position", () => {
        const cannon = game.getCoordAt(7, 2);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            expect(cannon.piece.player).toBe(Player.Red);
            expect(cannon.piece.getKillOptions().length).toBe(1);
        }
    });
    ///////////////////////////////////////////////////////////////

    // black cannon from begining position
    it("Black cannon(1,2) from begining position", () => {
        const cannon = game.getCoordAt(1, 7);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            expect(cannon.piece.player).toBe(Player.Black);
            expect(cannon.piece.getKillOptions().length).toBe(1);
        }
    });
    it("Black cannon(7,2) from begining position", () => {
        const cannon = game.getCoordAt(7, 7);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            expect(cannon.piece.player).toBe(Player.Black);
            expect(cannon.piece.getKillOptions().length).toBe(1);
        }
    });
    ///////////////////////////////////////////////////////////////

    it("red cannon(1,2) move to (4,2)", () => {
        let cannon = game.getCoordAt(1, 2);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            expect(game.move(cannon.piece, game.getCoordAt(4, 2))).toBe(true);
        }
    });

    it("red cannon(4,2) check kill set", () => {
        let cannon = game.getCoordAt(4, 2);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            expect(cannon.piece.getKillOptions().length).toBe(1);
        }
    });

    it("red cannon(4,2)  kill black pawn(4, 6)", () => {
        let attack = game.getCoordAt(4, 2);
        let guard = game.getCoordAt(4, 6);
        expect(attack.piece).not.toBe(null);
        expect(guard.piece).not.toBe(null);
        if (attack.piece !== null && guard.piece !== null) {
            expect(game.kill(attack.piece, guard.piece)).toBe(true);
        }
    });

    it("red cannon(4,6) check kill set and piece information at (4, 6)", () => {
        let cannon = game.getCoordAt(4, 6);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            expect(cannon.piece.player).toBe(Player.Red); // check player
            expect(cannon.piece.name).toBe("炮");   // check name

            expect(cannon.piece.getKillOptions().length).toBe(2); // kill options
        }
    });

    it("red cannon(7,2) move to (7,6)", () => {
        let cannon = game.getCoordAt(7, 2);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            expect(cannon.piece.getKillOptions().length).toBe(1);

            expect(game.move(cannon.piece, game.getCoordAt(7, 6))).toBe(true);
        }
        expect(cannon.piece).toBe(null);
    });

    it("red cannon(7,6) check kill set", () => {
        let cannon = game.getCoordAt(7, 6);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {

            expect(cannon.piece.getKillOptions().length).toBe(1);
        }
    });

    it("red cannon(4,6) kill black pawn(0, 6)", () => {
        const attack = game.getCoordAt(4, 6);
        const guard = game.getCoordAt(0, 6);
        
        expect(attack.piece).not.toBe(null);
        expect(guard.piece).not.toBe(null);

        if (attack.piece !== null && guard.piece !== null) {
            expect(attack.piece.getKillOptions().length).toBe(1);
            expect(game.kill(attack.piece, guard.piece)).toBe(true);
        }
    });

    it("red cannon(7,6) check kill set second time", () => {
        let cannon = game.getCoordAt(7, 6);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {

            expect(cannon.piece.getKillOptions().length).toBe(2);
        }
    });

    it("red cannon(7,6) kill black knight(7,9)", () => {
        let attack = game.getCoordAt(7, 6);
        let guard = game.getCoordAt(7, 9);
        expect(attack.piece).not.toBe(null);
        expect(guard.piece).not.toBe(null);
        if (attack.piece !== null && guard.piece !== null) {

            expect(game.kill(attack.piece, guard.piece)).toBe(true);
        }
    });

    it("red cannon(7,9) check kill set", () => {
        let cannon = game.getCoordAt(7, 9);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {

            expect(cannon.piece.getKillOptions().length).toBe(1);
        }
    });
});
