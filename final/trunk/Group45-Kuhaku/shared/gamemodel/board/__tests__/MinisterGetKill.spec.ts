import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
const game = new ChessGame();
describe('Test chess game', () => {
    
    // red minister from begining position
    it("red minister(2,0) from begining position", () => {
        const minister = game.getCoordAt(2, 0);
        expect(minister.piece).not.toBe(null);
        if (minister.piece !== null) {
            expect(minister.piece.getKillOptions().length).toBe(0);
            expect(minister.piece.getKillOptions().length).toBe(0);
        }
    });
    it("red minister(6,0) from begining position", () => {
        const minister = game.getCoordAt(6, 0);
        expect(minister.piece).not.toBe(null);
        if (minister.piece !== null) {
            expect(minister.piece.getKillOptions().length).toBe(0);
        }
    });
    ///////////////////////////////////////////////////////////////

    // red minister from begining position
    it("black minister(2,9) from begining position", () => {
        const minister = game.getCoordAt(2, 9);
        expect(minister.piece).not.toBe(null);
        if (minister.piece !== null) {
            expect(minister.piece.getKillOptions().length).toBe(0);
        }
    });
    it("black minister(6,9) from begining position", () => {
        const minister = game.getCoordAt(6, 9);
        expect(minister.piece).not.toBe(null);
        if (minister.piece !== null) {
            expect(minister.piece.getKillOptions().length).toBe(0);
        }
    });
    
});
