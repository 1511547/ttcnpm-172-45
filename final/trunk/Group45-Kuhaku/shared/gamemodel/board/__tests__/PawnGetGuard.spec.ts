import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
const game = new ChessGame();
describe('Test chess game', () => {

    it("Red Pawn(0,3) from begining position guards no one", () => {
        const pawn = game.getCoordAt(0, 3);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.player).toBe(Player.Red);
            expect(pawn.piece.getGuardOptions().length).toBe(0);
        }
    });

    it("Red Pawn(2,3) from begining position guards no one", () => {
        const pawn = game.getCoordAt(2, 3);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.player).toBe(Player.Red);
            expect(pawn.piece.getGuardOptions().length).toBe(0);
        }
    });

    it("Red Pawn(4,3) guards a Cannon placed at (4,4)", () => {
        const pawn = game.getCoordAt(4, 3);
        // First get the Cannon at (1, 2)
        const cannon = game.getCoordAt(1, 2);
        expect(pawn.piece).not.toBe(null);
        expect(cannon.piece).not.toBe(null);
        if (pawn.piece !== null && cannon.piece !== null) {
            // Then place that cannon at (4, 4)
            cannon.x = 4;
            cannon.y = 4;
            expect(pawn.piece.player).toBe(Player.Red);
            expect(cannon.piece.player).toBe(Player.Red);
            expect(pawn.piece.getGuardOptions().length).toBe(1);
        }
    });

    it("Red Pawn(3,5) guards a Cannon placed at (3,6) and and a Chariot placed at (2, 5)", () => {
        const pawn = game.getCoordAt(4, 3);
        // First get the Cannon at (1, 2) and the Chariot at (0, 0)
        const cannon = game.getCoordAt(1, 2);
        const chariot = game.getCoordAt(0, 0);
        expect(pawn.piece).not.toBe(null);
        expect(cannon.piece).not.toBe(null);
        expect(chariot.piece).not.toBe(null);
        if (pawn.piece !== null && cannon.piece !== null && chariot.piece !== null) {
            // First place that pawn at (3, 5)
            pawn.x = 3;
            pawn.y = 5;
            // Then place that cannon at (3, 6)
            cannon.x = 3;
            cannon.y = 6;
            // Finally place that chariot at (2, 5)
            chariot.x = 2;
            chariot.y = 5;
            expect(pawn.piece.player).toBe(Player.Red);
            expect(cannon.piece.player).toBe(Player.Red);
            expect(chariot.piece.player).toBe(Player.Red);
            expect(pawn.piece.getGuardOptions().length).toBe(2);
        }
    });
    
});