import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
const game = new ChessGame();
describe('Test chess game', () => {
    it('32 piece in board', () => {
        const count = game.getPieces.length;
        expect(count).toBe(32);
    });
    it('get red Chariot', () => {
        let chariotRed1 = game.getCoordAt(0, 0);
        let chariotRed2 = game.getCoordAt(8, 0);
        expect(chariotRed1.piece).toBeDefined();
        expect(chariotRed2.piece).toBeDefined();
        if (chariotRed1.piece !== null) {
            expect(chariotRed1.piece.player).toBe(Player.Red);
        }
        if (chariotRed2.piece !== null) {

            expect(chariotRed2.piece.player).toBe(Player.Red);
        }
    });
    it("get red general", () => {
        const general = game.getCoordAt(4, 0);
        expect(general.piece).not.toBe(null);
        if (general.piece !== null) {
            expect(general.piece.player).toBe(Player.Red);
        }
    });

    // red pawn from begining position 
    it("Red Pawn(0,3) from begining position", () => {
        const pawn = game.getCoordAt(0, 3);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });

    it("Red Pawn(2,3) from begining position", () => {
        const pawn = game.getCoordAt(2, 3);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });

    it("Red Pawn(4,3) from begining position", () => {
        const pawn = game.getCoordAt(4, 3);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });

    it("Red Pawn(6,3) from begining position", () => {
        const pawn = game.getCoordAt(6, 3);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });

    it("Red Pawn(8,3) from begining position", () => {
        const pawn = game.getCoordAt(8, 3);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });
    ///////////////////////////////////////////////////////////////

    // black pawn from begining position
    it("black Pawn(0,6) from begining position", () => {
        const pawn = game.getCoordAt(0, 6);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });

    it("black Pawn(2,6) from begining position", () => {
        const pawn = game.getCoordAt(2, 6);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });

    it("Red Pawn(4,3) from begining position", () => {
        const pawn = game.getCoordAt(4, 6);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });

    it("Red Pawn(6,3) from begining position", () => {
        const pawn = game.getCoordAt(6, 6);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });

    it("Red Pawn(8,3) from begining position", () => {
        const pawn = game.getCoordAt(8, 6);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
        }
    });
    /////////////////////////////////////////////////////////////

    // red cannon from begining position
    it("Red cannon(1,2) from begining position", () => {
        const cannon = game.getCoordAt(1, 2);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            expect(cannon.piece.getKillOptions().length).toBe(1);
        }
    });
    it("Red cannon(7,2) from begining position", () => {
        const cannon = game.getCoordAt(7, 2);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            expect(cannon.piece.getKillOptions().length).toBe(1);
        }
    });
    ///////////////////////////////////////////////////////////////

    // black cannon from begining position
    it("Black cannon(1,2) from begining position", () => {
        const cannon = game.getCoordAt(1, 7);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            expect(cannon.piece.getKillOptions().length).toBe(1);
        }
    });
    it("Black cannon(7,2) from begining position", () => {
        const cannon = game.getCoordAt(7, 7);
        expect(cannon.piece).not.toBe(null);
        if (cannon.piece !== null) {
            expect(cannon.piece.getKillOptions().length).toBe(1);
        }
    });
    ///////////////////////////////////////////////////////////////

    // red chariot from begining position
    it("Red Chariot(0,0) from begining position", () => {
        const chariot = game.getCoordAt(0, 0);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            expect(chariot.piece.getKillOptions().length).toBe(0);
        }
    });
    it("Red Chariot(8,0) from begining position", () => {
        const chariot = game.getCoordAt(8, 0);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            expect(chariot.piece.getKillOptions().length).toBe(0);
        }
    });
    ///////////////////////////////////////////////////////////////

    // black chariot from begining position
    it("Black Chariot(0,9) from begining position", () => {
        const chariot = game.getCoordAt(0, 9);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            expect(chariot.piece.getKillOptions().length).toBe(0);
        }
    });
    it("Black Chariot(8,9) from begining position", () => {
        const chariot = game.getCoordAt(8, 9);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            expect(chariot.piece.getKillOptions().length).toBe(0);
        }
    });
    ///////////////////////////////////////////////////////////////

    // red knight from begining position
    it("red knight(1,0) from begining position", () => {
        const knight = game.getCoordAt(1, 0);
        expect(knight.piece).not.toBe(null);
        if (knight.piece !== null) {
            expect(knight.piece.getKillOptions().length).toBe(0);
        }
    });
    it("red knight(7,0) from begining position", () => {
        const knight = game.getCoordAt(7, 0);
        expect(knight.piece).not.toBe(null);
        if (knight.piece !== null) {
            expect(knight.piece.getKillOptions().length).toBe(0);
        }
    });
    ///////////////////////////////////////////////////////////////

    // black knight from begining position
    it("black knight(1,9) from begining position", () => {
        const knight = game.getCoordAt(1, 9);
        expect(knight.piece).not.toBe(null);
        if (knight.piece !== null) {
            expect(knight.piece.getKillOptions().length).toBe(0);
        }
    });
    it("black knight(7,9) from begining position", () => {
        const knight = game.getCoordAt(7, 9);
        expect(knight.piece).not.toBe(null);
        if (knight.piece !== null) {
            expect(knight.piece.getKillOptions().length).toBe(0);
        }
    });
    ///////////////////////////////////////////////////////////////

    // red minister from begining position
    it("red minister(2,0) from begining position", () => {
        const minister = game.getCoordAt(2, 0);
        expect(minister.piece).not.toBe(null);
        if (minister.piece !== null) {
            expect(minister.piece.getKillOptions().length).toBe(0);
            expect(minister.piece.getKillOptions().length).toBe(0);
        }
    });
    it("red minister(6,0) from begining position", () => {
        const minister = game.getCoordAt(6, 0);
        expect(minister.piece).not.toBe(null);
        if (minister.piece !== null) {
            expect(minister.piece.getKillOptions().length).toBe(0);
        }
    });
    ///////////////////////////////////////////////////////////////

    // red minister from begining position
    it("black minister(2,9) from begining position", () => {
        const minister = game.getCoordAt(2, 9);
        expect(minister.piece).not.toBe(null);
        if (minister.piece !== null) {
            expect(minister.piece.getKillOptions().length).toBe(0);
        }
    });
    it("black minister(6,9) from begining position", () => {
        const minister = game.getCoordAt(6, 9);
        expect(minister.piece).not.toBe(null);
        if (minister.piece !== null) {
            expect(minister.piece.getKillOptions().length).toBe(0);
        }
    });
    ///////////////////////////////////////////////////////////////
    
    // red guard from begining position
    it("red guard(3,0) from begining position", () => {
        const guard = game.getCoordAt(3, 0);
        expect(guard.piece).not.toBe(null);
        if (guard.piece !== null) {
            expect(guard.piece.getKillOptions().length).toBe(0);
        }
    });
    it("red guard(5,0) from begining position", () => {
        const guard = game.getCoordAt(5, 0);
        expect(guard.piece).not.toBe(null);
        if (guard.piece !== null) {
            expect(guard.piece.getKillOptions().length).toBe(0);
        }
    });
    ///////////////////////////////////////////////////////////////

    // black guard from begining position
    it("black guard(3,9) from begining position", () => {
        const guard = game.getCoordAt(3, 9);
        expect(guard.piece).not.toBe(null);
        if (guard.piece !== null) {
            expect(guard.piece.getKillOptions().length).toBe(0);
        }
    });
    it("black guard(5,9) from begining position", () => {
        const guard = game.getCoordAt(5, 9);
        expect(guard.piece).not.toBe(null);
        if (guard.piece !== null) {
            expect(guard.piece.getKillOptions().length).toBe(0);
        }
    });
    ///////////////////////////////////////////////////////////////

    // red general from begining position
    it("red general(4,0) from begining position", () => {
        const general = game.getCoordAt(4, 0);
        expect(general.piece).not.toBe(null);
        if (general.piece !== null) {
            expect(general.piece.getKillOptions().length).toBe(0);
        }
    });
    ///////////////////////////////////////////////////////////////

    // black general from begining position
    it("black general(4,9) from begining position", () => {
        const general = game.getCoordAt(4, 9);
        expect(general.piece).not.toBe(null);
        if (general.piece !== null) {
            expect(general.piece.getKillOptions().length).toBe(0);
        }
    });
    ///////////////////////////////////////////////////////////////

    it("Red Pawn(0,3) go up to (0,4)", () => {
        let pawn = game.getCoordAt(0, 3);
        expect(pawn.piece).not.toBe(null);
        if (pawn.piece !== null) {
            expect(pawn.piece.getKillOptions().length).toBe(0);
            expect(pawn.piece.getMoveOptions().length).toBe(1);
            expect(game.move(pawn.piece, game.getCoordAt(0, 4))).toBe(true);        
        }
        
    });
});
