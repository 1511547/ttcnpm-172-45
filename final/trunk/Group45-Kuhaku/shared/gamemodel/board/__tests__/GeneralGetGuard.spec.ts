import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
const game = new ChessGame();
describe('Test chess game', () => {

    // red cannon from begining position
    it("Red General(4,0) from begining position guards two guard", () => {
        const general = game.getCoordAt(4, 0);
        expect(general.piece).not.toBe(null);
        if (general.piece !== null) {
            expect(general.piece.player).toBe(Player.Red);
            expect(general.piece.getGuardOptions().length).toBe(2);
        }
    });
    
});