import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
const game = new ChessGame();
describe('Test chess game', () => {

    it("Red Guard(5,0) from begining position guards no one", () => {
        const guard = game.getCoordAt(5, 0);
        expect(guard.piece).not.toBe(null);
        if (guard.piece !== null) {
            expect(guard.piece.player).toBe(Player.Red);
            expect(guard.piece.getGuardOptions().length).toBe(0);
        }
    });

    it("Red Guard(5,0) guards Generl(4,1)", () => {
        const guard = game.getCoordAt(5, 0);
        let general = game.getCoordAt(4, 0);
        expect(guard.piece).not.toBe(null);
        expect(general.piece).not.toBe(null);
        general.y = 1;
        if (guard.piece !== null && general.piece !== null) {
            expect(guard.piece.player).toBe(Player.Red);
            expect(guard.piece.getGuardOptions().length).toBe(1);
        }
    });
    
});