import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
const game = new ChessGame();
describe('Test chess game', () => {
    // red guard from begining position
    it("red guard(3,0) from begining position", () => {
        const guard = game.getCoordAt(3, 0);
        expect(guard.piece).not.toBe(null);
        if (guard.piece !== null) {
            expect(guard.piece.getKillOptions().length).toBe(0);
        }
    });
    it("red guard(5,0) from begining position", () => {
        const guard = game.getCoordAt(5, 0);
        expect(guard.piece).not.toBe(null);
        if (guard.piece !== null) {
            expect(guard.piece.getKillOptions().length).toBe(0);
        }
    });
    ///////////////////////////////////////////////////////////////

    // black guard from begining position
    it("black guard(3,9) from begining position", () => {
        const guard = game.getCoordAt(3, 9);
        expect(guard.piece).not.toBe(null);
        expect(guard.piece.getKillOptions().length).toBe(0);

    });
    it("black guard(5,9) from begining position", () => {
        const guard = game.getCoordAt(5, 9);
        expect(guard.piece).not.toBe(null);
        if (guard.piece !== null) {
            expect(guard.piece.getKillOptions().length).toBe(0);
        }
    });

});
