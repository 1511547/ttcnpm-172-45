import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
const game = new ChessGame();
describe('Test chess game', () => {

    // red cannon from begining position
    it("Red Chariot(0,0) from begining position guards a Pawn and a Knight", () => {
        const chariot = game.getCoordAt(0, 0);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            expect(chariot.piece.player).toBe(Player.Red);
            expect(chariot.piece.getGuardOptions().length).toBe(2);
        }
    });

    it("Red Chariot(8,0) from begining position guards a Pawn and a Knight", () => {
        const chariot = game.getCoordAt(8, 0);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            expect(chariot.piece.player).toBe(Player.Red);
            expect(chariot.piece.getGuardOptions().length).toBe(2);
        }
    });
    it("Red Chariot(2,2) guards a pawn, two cannon and a minister", () => {
        // Get chariot (0,0)
        let chariot = game.getCoordAt(0, 0);
        expect(chariot.piece).not.toBe(null);
        if (chariot.piece !== null) {
            // Move chariot to (2,2)
            chariot.piece.coordinate.x = 2;
            chariot.piece.coordinate.y = 2;
            expect(chariot.piece.player).toBe(Player.Red);
            expect(chariot.piece.getGuardOptions().length).toBe(4);
        }
    });
    
});
