import 'jest';
import { ChessGame } from '../ChessGame';
import { Player } from '../Player';
const game = new ChessGame();
describe('Test chess game', () => {

    it("Red Minister(2,0) from begining position guards no one", () => {
        const minister = game.getCoordAt(2, 0);
        expect(minister.piece).not.toBe(null);
        if (minister.piece !== null) {
            expect(minister.piece.player).toBe(Player.Red);
            expect(minister.piece.getGuardOptions().length).toBe(0);
        }
    });

    it("Red Minister(6,0) from begining position guards no one", () => {
        const minister = game.getCoordAt(6, 0);
        expect(minister.piece).not.toBe(null);
        if (minister.piece !== null) {
            expect(minister.piece.player).toBe(Player.Red);
            expect(minister.piece.getGuardOptions().length).toBe(0);
        }
    });

    it("Red Minister(4,2) guards another Minister(6,0)", () => {
        // First get the minister at (2, 0)
        let minister = game.getCoordAt(2, 0);
        expect(minister.piece).not.toBe(null);
        if (minister.piece !== null) {
            // Then move it to (4, 2)
            minister.x = 4;
            minister.y = 2;
            expect(minister.piece.player).toBe(Player.Red);
            expect(minister.piece.getGuardOptions().length).toBe(1);
        }
    });
    
});