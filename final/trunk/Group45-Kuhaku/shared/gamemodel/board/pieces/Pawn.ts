import { Piece } from "./Piece";
import { Coordinate } from "../Coordinate";
import { Player } from "../Player";
import { ChessGame } from "../ChessGame";
import { PieceState } from ".";

export class Pawn extends Piece {
    move: Array<{ horizontal: number, vertical: number }>;
    attack: Array<{ horizontal: number, vertical: number }>;
    defend: Array<{ horizontal: number, vertical: number }>;
    isCrossRiver: boolean;
    riverBoundary: number;
    getMoveOptions(): Coordinate[] {
        let currentCoordinate = this.coordinate;
        this.move = this.checkCrossRiver(currentCoordinate) ? this.attack : this.defend;
        let results = this.move.reduce(
            (result, offset) => {
                let newCoordinate = this.calculateMove(currentCoordinate, offset);
                if (newCoordinate !== null && newCoordinate.piece === null) {
                    result.push(newCoordinate);
                }
                return result;
            },
            new Array<Coordinate>());

        return results;
    }

    checkCrossRiver(coord: Coordinate) {
        if (this.player === Player.Black) {
            return coord.y < 5;
        } else { return coord.y > 4; }

    }

    getMoveToPiecesOption(canKill: boolean): Coordinate[] {

        let result: Coordinate[] = new Array(0);
        let pieceList: Piece[] = this.game.getPieces;

        if (this.player === Player.Red) {
            if (this.coordinate.y < 5) {
                // check ahead
                
                pieceList.forEach(piece => {
                    if (piece.state !== PieceState.Dead) {
                        if (
                            (piece.coordinate.x === this.coordinate.x)
                            && (piece.coordinate.y === (this.coordinate.y + 1))
                            && (canKill ? (piece.player !== this.player) 
                            : ((piece.player === this.player) && piece !== this))
                        ) {
                            
                            result.push(piece.coordinate);
                        } 
                    }
                });
            } else {
                 // check ahead
                pieceList.forEach(piece => {
                    if (piece.state !== PieceState.Dead) {

                        if (
                            (piece.coordinate.x === this.coordinate.x)
                            && (piece.coordinate.y === (this.coordinate.y + 1))
                            && (canKill ? (piece.player !== this.player) 
                            : ((piece.player === this.player) && piece !== this))
                        ) {
                            
                            result.push(piece.coordinate);
                        } 

                    // check left
                        if (
                            (piece.coordinate.x === (this.coordinate.x - 1))
                            && (piece.coordinate.y === this.coordinate.y)
                            && (canKill ? (piece.player !== this.player) 
                            : ((piece.player === this.player) && piece !== this))
                        ) {
                            // canKillPiece = new Coordinate(piece.coordinate.x, piece.coordinate.y);
                            // canKillPiece.piece = piece;
                            result.push(piece.coordinate);
                        } 

                    // check right
                        if (
                            (piece.coordinate.x === (this.coordinate.x + 1))
                            && (piece.coordinate.y === this.coordinate.y)
                            && (canKill ? (piece.player !== this.player) 
                            : ((piece.player === this.player) && piece !== this))
                        ) {
                            
                            result.push(piece.coordinate);
                        } 
                    }
                });
            }
        } else {
            if (this.coordinate.y > 4) {
                // check ahead
                pieceList.forEach(piece => {
                    if (piece.state !== PieceState.Dead) {
                        if (
                            (piece.coordinate.x === this.coordinate.x)
                            && (piece.coordinate.y === (this.coordinate.y - 1))
                            && (canKill ? (piece.player !== this.player) 
                            : ((piece.player === this.player) && piece !== this))
                        ) {
                            result.push(piece.coordinate);
                        }
                    }
                });
            } else {
                
                pieceList.forEach(piece => {
                    if (piece.state !== PieceState.Dead) {
                    // check ahead
                        if (
                            (piece.coordinate.x === this.coordinate.x)
                            && (piece.coordinate.y === (this.coordinate.y - 1))
                            && (canKill ? (piece.player !== this.player) 
                            : ((piece.player === this.player) && piece !== this))
                        ) {
                            result.push(piece.coordinate);
                        } 

                    // check left
                        if (
                            (piece.coordinate.x === (this.coordinate.x - 1))
                            && (piece.coordinate.y === this.coordinate.y)
                            && (canKill ? (piece.player !== this.player) 
                            : ((piece.player === this.player) && piece !== this))
                        ) {
                            result.push(piece.coordinate);
                        } 

                    // check right
                        if (
                            (piece.coordinate.x === (this.coordinate.x + 1))
                            && (piece.coordinate.y === this.coordinate.y)
                            && (canKill ? (piece.player !== this.player) 
                            : ((piece.player === this.player) && piece !== this))
                        ) {
                            
                            result.push(piece.coordinate);
                        } 
                    }
                });
            }   
        }

        return result;
    }

    constructor(coordinate: Coordinate, player: Player, game: ChessGame) {
        super("Pawn", coordinate, player, game);
        this.isCrossRiver = false;
        if (player === Player.Black) {
            this.riverBoundary = 4;
            this.defend = [{ horizontal: 0, vertical: -1 }
            ];
            this.attack = [
                { horizontal: 0, vertical: -1 },
                { horizontal: 1, vertical: 0 },
                { horizontal: -1, vertical: 0 }
            ];
        } else {
            this.defend = [{ horizontal: 0, vertical: 1 }
            ];
            this.attack = [
                { horizontal: 0, vertical: 1 },
                { horizontal: -1, vertical: 0 },
                { horizontal: 1, vertical: 0 },
            ];

        }
    }
}