import { Coordinate } from '../Coordinate';
import { Player } from '../Player';
import { PieceState } from './PieceState';
import { PieceInfo } from './PieceInfo';
import { ChessGame } from '../ChessGame';
export abstract class Piece {
    player: Player;
    id: number;
    private _coordinate: Coordinate;
    state: PieceState;
    game: ChessGame;
    killmoveSet = null;
    moveSet = null;

    private _name: string;
    constructor(name: string, coordinate: Coordinate, player: Player, game: ChessGame) {
        this._coordinate = coordinate;
        this._coordinate.piece = this;
        this.player = player;
        this.state = PieceState.Active;
        this.game = game;
        this.id = this.game.numPiece++;
        this._name = name;
    }
    public get coordinate(): Coordinate {
        return this._coordinate;
    }
    public set coordinate(coord: Coordinate) {
        if (coord === null) {
            this._coordinate = null;
        } else if (coord !== this._coordinate) {
            this._coordinate = coord;
            this._coordinate.piece = this;
        }
    }
    get name(): string {
        let str = this.player === Player.Red ? "Red" : "Black";
        return PieceInfo[str][this._name];
    }
    get pieceState(): PieceState {
        return this.state;
    }
    /**
     * Get all coordinate which this piece can move
     * @returns Coordinate[]
     */
    abstract getMoveOptions(): Coordinate[];

    /**
     * Get all coordinate contain piece which this piece can move to (kill or guard)
     * @returns Coordinate[]
     */
    protected abstract getMoveToPiecesOption(canKill: boolean): Coordinate[];

    getKillOptions(): Coordinate[] {
        return this.getMoveToPiecesOption(true);
    }

    getGuardOptions(): Coordinate[] {
        return this.getMoveToPiecesOption(false);
    }

    calculateMove(
        coods: Coordinate,
        offset: {
            horizontal: number,
            vertical: number
        },
        boundary?: { top: number, bottom: number, left: number, right: number }): Coordinate | null {
        let top = 0;
        let bottom = 10;
        let left = 0;
        let right = 9;
        if (typeof boundary !== 'undefined') {
            top = boundary.top;
            bottom = boundary.bottom;
            left = boundary.left;
            right = boundary.right;
        }
        let newX = coods.x + offset.horizontal;
        let newY = coods.y + offset.vertical;
        if ((newX < right) && (newX >= left) && (newY >= top) && (newY < bottom)) {
            return this.game.getCoordinates[newX][newY];
        } else {
            return null;
        }
    }

    isDead() {
        return this.state === PieceState.Dead;
    }
    isActive() {
        return this.state === PieceState.Active;
    }
    isHaloed() {
        return this.state === PieceState.Haloed;
    }
}