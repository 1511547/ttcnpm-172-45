import { Piece } from "./Piece";
import { Coordinate } from "../Coordinate";
import { Player } from "../Player";
import { ChessGame } from "../ChessGame";
import { PieceState } from ".";

const getX = (coord: Coordinate) => {
    return coord.x;
};
const getY = (coord: Coordinate) => {
    return coord.y;
};

export class Cannon extends Piece {
    constructor(coordinate: Coordinate, player: Player, game: ChessGame) {
        super("Cannon", coordinate, player, game);
    }

    getMoveOptions(): Coordinate[] {
        let coords = this.game.getCoordinates;
        let currentCoordinate = this.coordinate;
        let col = coords[currentCoordinate.x];
        let row = coords.map((coord) => { return (coord[currentCoordinate.y]); });
        let possibleCol = this.possibleMove(col, currentCoordinate, false);
        let possibleRow = this.possibleMove(row, currentCoordinate, true);
        return possibleCol.concat(possibleRow);
    }

    possibleMove(results: Coordinate[], selectedPiece: Coordinate, isRow: boolean): Coordinate[] {
        let isStop = false;
        let get = isRow ? getX : getY;
        return results.reduce(
            (arr, newValue) => {
                if (newValue.piece === null) {
                    if (!isStop) { arr.push(newValue); }
                } else {
                    if (get(newValue) > get(selectedPiece)) {
                        isStop = true;
                    } else if (get(newValue) < get(selectedPiece)) {
                        arr = [];
                    }
                }
                return arr;
            },
            new Array<Coordinate>());

    }

    getMoveToPiecesOption(canKill: boolean): Coordinate[] {

        let columnList = this.getMoveToPiecesOptionColumn(canKill);
        let rowList = this.getMoveToPiecesOptionRow(canKill);
        return columnList.concat(rowList);
    }

    private getMoveToPiecesOptionColumn(canKill: boolean): Coordinate[] {
        let result: Coordinate[] = new Array(0);
        let pieceList: Piece[] = this.game.getPieces;

        // forward checking
        let forwardList = pieceList.filter(anotherPiece => {
            return anotherPiece.state !== PieceState.Dead
                && this.coordinate.x === anotherPiece.coordinate.x
                && anotherPiece.coordinate.y > this.coordinate.y;
        });

        let forwardListLength = forwardList.length;
        // do if the length of forwardList is greater than 0
        if (forwardListLength >= 2) {
            // if there are more than or equal two element, sort -> check second element -> push
            // ascending order
            forwardList.sort((e1, e2) => {
                return e1.coordinate.y - e2.coordinate.y;
            });
            if ((forwardList[1].player !== this.player && canKill) 
            || (forwardList[1].player === this.player && !canKill)) {
                result.push(forwardList[1].coordinate);
            }
        }

        // backward checking
        let backwardList = pieceList.filter(anotherPiece => {
            return anotherPiece.state !== PieceState.Dead
                && this.coordinate.x === anotherPiece.coordinate.x
                && anotherPiece.coordinate.y < this.coordinate.y;
        });

        let backwardListLength = backwardList.length;
        // do if the length of backwardList is greater than 0
        if (backwardListLength >= 2) {
            // if there are more than or equal two element, sort -> check second element -> push
            // descending order
            backwardList.sort((e1, e2) => {
                return e2.coordinate.y - e1.coordinate.y;
            });
            if ((backwardList[1].player !== this.player && canKill)  
            || (backwardList[1].player === this.player && !canKill)) {
                result.push(backwardList[1].coordinate);
            }

        }

        return result;
    }

    private getMoveToPiecesOptionRow(canKill: boolean): Coordinate[] {

        let result: Coordinate[] = new Array(0);
        let pieceList: Piece[] = this.game.getPieces;

        // right checking
        let rightList = pieceList.filter(anotherPiece => {
            return anotherPiece.state !== PieceState.Dead
                && this.coordinate.y === anotherPiece.coordinate.y
                && anotherPiece.coordinate.x > this.coordinate.x;
        });

        let rightListLength = rightList.length;
        // do if the length of rightList is greater than 0
        if (rightListLength >= 2) {
            // if there are more than or equal two element, sort -> check second element -> push
            // ascending order
            rightList.sort((e1, e2) => {
                return e1.coordinate.x - e2.coordinate.x;
            });
            if ((rightList[1].player !== this.player && canKill) || (rightList[1].player === this.player && !canKill)) {
                result.push(rightList[1].coordinate);
            }

        }

        // left checking
        let leftList = pieceList.filter(anotherPiece => {
            return anotherPiece.state !== PieceState.Dead
                && this.coordinate.y === anotherPiece.coordinate.y
                && anotherPiece.coordinate.x < this.coordinate.x;
        });

        let leftListLength = leftList.length;
        // do if the length of leftList is greater than 0
        if (leftListLength >= 2) {
            // if there are more than or equal two element, sort -> check first element -> push
            // descending order
            leftList.sort((e1, e2) => {
                return e2.coordinate.x - e1.coordinate.x;
            });
            if ((leftList[1].player !== this.player && canKill) || (leftList[1].player === this.player && !canKill)) {
                result.push(leftList[1].coordinate);
            }

        }
        return result;
    }

}