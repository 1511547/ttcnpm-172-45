import { Piece } from "./Piece";
import { Coordinate } from "../Coordinate";
import { Player } from "../Player";
import { ChessGame } from "../ChessGame";
import { PieceState } from ".";

const block = [
    { horizontal: 1, vertical: 0 },
    { horizontal: 0, vertical: 1 },
    { horizontal: -1, vertical: 0 },
    { horizontal: 0, vertical: -1 }
];

const move = [
    { horizontal: 2, vertical: 1 },
    { horizontal: 2, vertical: -1 },
    { horizontal: 1, vertical: 2 },
    { horizontal: -1, vertical: 2 },
    { horizontal: -2, vertical: 1 },
    { horizontal: -2, vertical: -1 },
    { horizontal: 1, vertical: -2 },
    { horizontal: -1, vertical: -2 }
];

export class Knight extends Piece {
    constructor(coordinate: Coordinate, player: Player, game: ChessGame) {
        super("Knight", coordinate, player, game);
    }

    getMoveOptions(): Coordinate[] {
        let currentCoordinate = this.coordinate;

        let results = block.reduce(
            (result, offset, index) => {
                let blockCoord = this.calculateMove(currentCoordinate, offset);
                if (blockCoord !== null) {
                    if (blockCoord.piece === null) {
                        let newCoordinate1 = this.calculateMove(currentCoordinate, move[index * 2]);
                        let newCoordinate2 = this.calculateMove(currentCoordinate, move[index * 2 + 1]);
                        if (newCoordinate1 !== null && newCoordinate1.piece === null) { result.push(newCoordinate1); }
                        if (newCoordinate2 !== null && newCoordinate2.piece === null) { result.push(newCoordinate2); }
                    }
                }

                return result;
            },
            new Array<Coordinate>());

        return results;
    }
    getMoveToPiecesOption(canKill: boolean): Coordinate[] {
        
        let pieceList: Piece[] = this.game.getPieces;
        
        // method : check each piece
        let preventList = pieceList.filter( preventPiece => {
            return preventPiece.state !== PieceState.Dead
                    && (
                            // forward coordinate
                            (preventPiece.coordinate.x === this.coordinate.x
                            && preventPiece.coordinate.y === this.coordinate.y + 1)
                        || 
                            // backward coordinate
                            (preventPiece.coordinate.x === this.coordinate.x
                            && preventPiece.coordinate.y === this.coordinate.y - 1)
                        || 
                            // right coordinate
                            (preventPiece.coordinate.x === this.coordinate.x + 1
                            && preventPiece.coordinate.y === this.coordinate.y)
                        ||
                            // left coordinate
                            (preventPiece.coordinate.x === this.coordinate.x - 1
                            && preventPiece.coordinate.y === this.coordinate.y)
                    );
        });

        let pieceToMoveToList: Piece[] = [];

        if (canKill) {
            pieceToMoveToList = pieceList.filter(anotherPiece => {
                return (anotherPiece.state !== PieceState.Dead  // anotherPiece not dead
                    && anotherPiece.player !== this.player)  // is different color
                    && (
                        // two top piece
                        ((anotherPiece.coordinate.y === this.coordinate.y + 2)
                        && 
                        (
                            (anotherPiece.coordinate.x === this.coordinate.x + 1)
                            ||
                            (anotherPiece.coordinate.x === this.coordinate.x - 1)
                        )      
                        && !this.isPrevent(preventList, 0, 1) )
                        ||
                        // two bottom piece
                        ((anotherPiece.coordinate.y === this.coordinate.y - 2)
                        && 
                        (
                            (anotherPiece.coordinate.x === this.coordinate.x + 1)
                            ||
                            (anotherPiece.coordinate.x === this.coordinate.x - 1)
                        )      
                        && !this.isPrevent(preventList, 0, -1) )
                        ||
                        // two right piece
                        ((anotherPiece.coordinate.x === this.coordinate.x + 2)
                        && 
                        (
                            (anotherPiece.coordinate.y === this.coordinate.y + 1)
                            ||
                            (anotherPiece.coordinate.y === this.coordinate.y - 1)
                        )      
                        && !this.isPrevent(preventList, 1, 0) )
                        // two left piece
                        ||
                        ((anotherPiece.coordinate.x === this.coordinate.x - 2)
                        && 
                        (
                            (anotherPiece.coordinate.y === this.coordinate.y + 1)
                            ||
                            (anotherPiece.coordinate.y === this.coordinate.y - 1)
                        )      
                        && !this.isPrevent(preventList, -1, 0) )
                    );
            });
        } else {
            pieceToMoveToList = pieceList.filter(anotherPiece => {
                return (anotherPiece.state !== PieceState.Dead  // anotherPiece not dead
                    && anotherPiece.player === this.player)  // is same color
                    && anotherPiece !== this                    // Not itself
                    && (
                        // two top piece
                        ((anotherPiece.coordinate.y === this.coordinate.y + 2)
                        && 
                        (
                            (anotherPiece.coordinate.x === this.coordinate.x + 1)
                            ||
                            (anotherPiece.coordinate.x === this.coordinate.x - 1)
                        )      
                        && !this.isPrevent(preventList, 0, 1) )
                        ||
                        // two bottom piece
                        ((anotherPiece.coordinate.y === this.coordinate.y - 2)
                        && 
                        (
                            (anotherPiece.coordinate.x === this.coordinate.x + 1)
                            ||
                            (anotherPiece.coordinate.x === this.coordinate.x - 1)
                        )      
                        && !this.isPrevent(preventList, 0, -1) )
                        ||
                        // two right piece
                        ((anotherPiece.coordinate.x === this.coordinate.x + 2)
                        && 
                        (
                            (anotherPiece.coordinate.y === this.coordinate.y + 1)
                            ||
                            (anotherPiece.coordinate.y === this.coordinate.y - 1)
                        )      
                        && !this.isPrevent(preventList, 1, 0) )
                        // two left piece
                        ||
                        ((anotherPiece.coordinate.x === this.coordinate.x - 2)
                        && 
                        (
                            (anotherPiece.coordinate.y === this.coordinate.y + 1)
                            ||
                            (anotherPiece.coordinate.y === this.coordinate.y - 1)
                        )      
                        && !this.isPrevent(preventList, -1, 0) )
                    );
            });
        }
    
        let result = pieceToMoveToList.map(piece => {
            return piece.coordinate;
        });
        return result;
    }

    // check if have prevent piece at (xCheck, yCheck)
    private isPrevent(preventList: Piece[], xCheck: number, yCheck: number): Boolean {
        let result = false;
        preventList.forEach(preventPiece => {
            if (preventPiece.coordinate.x === this.coordinate.x + xCheck 
                && preventPiece.coordinate.y === this.coordinate.y + yCheck) {
                result = true;
            }
        });
        return result;
    }
}