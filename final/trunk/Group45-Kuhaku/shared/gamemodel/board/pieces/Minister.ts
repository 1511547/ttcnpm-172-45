import { Piece } from "./Piece";
import { Coordinate } from "../Coordinate";
import { Player } from "../Player";
import { ChessGame } from "../ChessGame";
import { PieceState } from ".";

const move = [
    { horizontal: 2, vertical: 2 },
    { horizontal: -2, vertical: 2 },
    { horizontal: -2, vertical: -2 },
    { horizontal: 2, vertical: -2 }
];

export class Minister extends Piece {
    
    boundary: { top: number, bottom: number, left: number, right: number };

    constructor(coordinate: Coordinate, player: Player, game: ChessGame) {
        super("Minister", coordinate, player, game);
        if (this.player === Player.Black) {
            this.boundary = { top: 5, bottom: 10, left: 0, right: 10 };
        } else {
            this.boundary = { top: 0, bottom: 5, left: 0, right: 10 };
        }
    }
    getMoveOptions(): Coordinate[] {
        let currentCoordinate = this.coordinate;
        let results = move.reduce(
            (result, offset) => {
                let newCoordinate = this.calculateMove(currentCoordinate, offset, this.boundary);
                if (newCoordinate !== null && newCoordinate.piece === null) {
                    let block = this.calculateMove(
                        currentCoordinate,
                        { horizontal: offset.horizontal / 2, vertical: offset.vertical / 2 });
                    if (block !== null && block.piece === null) {
                        result.push(newCoordinate);
                    }
                }
                return result;
            },
            new Array<Coordinate>());
        return results;
    }
    getMoveToPiecesOption(canKill: boolean): Coordinate[] {
        let pieceList: Piece[] = this.game.getPieces;
        if (this.player === Player.Red) {
            let redSideList = pieceList.filter(redSidePiece => {
                return (redSidePiece.coordinate.y >= 0) && 
                        (redSidePiece.coordinate.y <= 4);
            });
            return canKill ? this.canKill(redSideList) : this.canGuard(redSideList);
        } else {
            let blackSideList = pieceList.filter(redSidePiece => {
                return (redSidePiece.coordinate.y >= 5) && 
                        (redSidePiece.coordinate.y <= 9);
            });
            return canKill ? this.canKill(blackSideList) : this.canGuard(blackSideList);
        }
    }
    private canKill(pieceList: Piece[]): Array<Coordinate> {
        let preventList = pieceList.filter(preventPiece => {
            return (preventPiece.state !== PieceState.Dead)
                    &&
                    (
                        // top right
                        (preventPiece.coordinate.x === this.coordinate.x + 1
                        && preventPiece.coordinate.y === this.coordinate.y + 1)
                        ||
                        // bottom right
                        (preventPiece.coordinate.x === this.coordinate.x + 1
                        && preventPiece.coordinate.y === this.coordinate.y - 1)
                        ||
                        // top left
                        (preventPiece.coordinate.x === this.coordinate.x - 1
                        && preventPiece.coordinate.y === this.coordinate.y + 1)
                        ||
                        // bottom left
                        (preventPiece.coordinate.x === this.coordinate.x - 1
                        && preventPiece.coordinate.y === this.coordinate.y - 1)
                    );
        });
        let canKillList = pieceList.filter(anotherPiece => {
            return (anotherPiece.state !== PieceState.Dead  // anotherPiece not dead
                    && anotherPiece.player !== this.player)  // is different color
                    &&  
                    (
                        // top right
                        (anotherPiece.coordinate.x === this.coordinate.x + 2
                        && anotherPiece.coordinate.y === this.coordinate.y + 2
                        && !this.isPrevent(preventList, 1, 1))
                        ||
                        // bottom right
                        (anotherPiece.coordinate.x === this.coordinate.x + 2
                        && anotherPiece.coordinate.y === this.coordinate.y - 2
                        && !this.isPrevent(preventList, 1, -1))
                        ||
                        // top left
                        (anotherPiece.coordinate.x === this.coordinate.x - 2
                        && anotherPiece.coordinate.y === this.coordinate.y + 2
                        && !this.isPrevent(preventList, -1, 1))
                        ||
                        // bottom left
                        (anotherPiece.coordinate.x === this.coordinate.x - 2
                        && anotherPiece.coordinate.y === this.coordinate.y - 2
                        && !this.isPrevent(preventList, -1, -1))
                    );
            });
        let result = canKillList.map(piece => {
            return piece.coordinate;
        });
        return result;
    }
    private canGuard(pieceList: Piece[]): Array<Coordinate> {
        let preventList = pieceList.filter(preventPiece => {
            return (preventPiece.state !== PieceState.Dead)
                    &&
                    (
                        // top right
                        (preventPiece.coordinate.x === this.coordinate.x + 1
                        && preventPiece.coordinate.y === this.coordinate.y + 1)
                        ||
                        // bottom right
                        (preventPiece.coordinate.x === this.coordinate.x + 1
                        && preventPiece.coordinate.y === this.coordinate.y - 1)
                        ||
                        // top left
                        (preventPiece.coordinate.x === this.coordinate.x - 1
                        && preventPiece.coordinate.y === this.coordinate.y + 1)
                        ||
                        // bottom left
                        (preventPiece.coordinate.x === this.coordinate.x - 1
                        && preventPiece.coordinate.y === this.coordinate.y - 1)
                    );
        });
        let canGuardList = pieceList.filter(anotherPiece => {
            return (anotherPiece.state !== PieceState.Dead  // anotherPiece not dead
                    && anotherPiece.player === this.player)  // is same color
                    && anotherPiece !== this                // Not itself
                    &&  
                    (
                        // top right
                        (anotherPiece.coordinate.x === this.coordinate.x + 2
                        && anotherPiece.coordinate.y === this.coordinate.y + 2
                        && !this.isPrevent(preventList, 1, 1))
                        ||
                        // bottom right
                        (anotherPiece.coordinate.x === this.coordinate.x + 2
                        && anotherPiece.coordinate.y === this.coordinate.y - 2
                        && !this.isPrevent(preventList, 1, -1))
                        ||
                        // top left
                        (anotherPiece.coordinate.x === this.coordinate.x - 2
                        && anotherPiece.coordinate.y === this.coordinate.y + 2
                        && !this.isPrevent(preventList, -1, 1))
                        ||
                        // bottom left
                        (anotherPiece.coordinate.x === this.coordinate.x - 2
                        && anotherPiece.coordinate.y === this.coordinate.y - 2
                        && !this.isPrevent(preventList, -1, -1))
                    );
            });
        let result = canGuardList.map(piece => {
            return piece.coordinate;
        });
        return result;
    }
    
    private isPrevent(preventList: Piece[], xCheck: number, yCheck: number): Boolean {
        let result = false;
        preventList.forEach(preventPiece => {
            if (preventPiece.coordinate.x === this.coordinate.x + xCheck 
                && preventPiece.coordinate.y === this.coordinate.y + yCheck) {
                result = true;
            }
        });
        return result;
    }
}