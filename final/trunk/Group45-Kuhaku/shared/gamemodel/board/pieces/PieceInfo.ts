export var PieceInfo = {
    Red: {

        Chariot: '俥',
        Knight: '傌',
        Minister: '相',
        Guard: '士',
        General: '帥',
        Cannon: '炮',
        Pawn: '卒'
    },
    Black: {
        Chariot: '車',
        Knight: '馬',
        Minister: '象',
        Guard: '仕',
        General: '將',
        Cannon: '砲',
        Pawn: '兵'
    }
};