import { Piece } from "./Piece";
import { Coordinate } from "../Coordinate";
import { Player } from "../Player";
import { ChessGame } from "../ChessGame";
import { PieceState } from ".";

const move = [
    { horizontal: 1, vertical: 1 },
    { horizontal: -1, vertical: 1 },
    { horizontal: -1, vertical: -1 },
    { horizontal: 1, vertical: -1 }
];

export class Guard extends Piece {
    boundary: { top: number, bottom: number, left: number, right: number };
    getMoveOptions(): Coordinate[] {

        let currentCoordinate = this.coordinate;

        let results = move.reduce(
            (result, offset) => {
                let newCoordinate = this.calculateMove(currentCoordinate, offset, this.boundary);
                if (newCoordinate !== null && newCoordinate.piece === null) {
                    result.push(newCoordinate);
                }
                return result;
            },
            new Array<Coordinate>());

        return results;
    }
    getMoveToPiecesOption(canKill: boolean): Coordinate[] {
        var result: Coordinate[] = new Array(0);
        var pieceList = this.game.getPieces;

        if (this.player === Player.Red) {
            var redPieceList: Piece[] = [];
            if (canKill) {
                redPieceList = pieceList.filter(piece => {
                    return (
                        (piece.state !== PieceState.Dead) // piece isn't dead
                        && (piece.player !== this.player) //
                        && (piece.coordinate.x >= 3)      // in alowable area
                        && (piece.coordinate.x <= 5)
                        && (piece.coordinate.y >= 0)
                        && (piece.coordinate.y <= 2)
                    );
                });
            } else {
                redPieceList = pieceList.filter(piece => {
                    return (
                        (piece.state !== PieceState.Dead) // piece isn't dead
                        && (piece.player === this.player) //
                        && (piece !== this)               // Not itself
                        && (piece.coordinate.x >= 3)      // in alowable area
                        && (piece.coordinate.x <= 5)
                        && (piece.coordinate.y >= 0)
                        && (piece.coordinate.y <= 2)
                    );
                });
            }

            redPieceList.forEach(piece => {
                if (
                        // top right
                        (piece.coordinate.x === this.coordinate.x + 1
                        && piece.coordinate.y === this.coordinate.y + 1)
                        ||
                        // bottom right
                        (piece.coordinate.x === this.coordinate.x + 1
                        && piece.coordinate.y === this.coordinate.y - 1)
                        ||
                        // top left
                        (piece.coordinate.x === this.coordinate.x - 1
                        && piece.coordinate.y === this.coordinate.y + 1)
                        ||
                        // bottom left
                        (piece.coordinate.x === this.coordinate.x - 1
                        && piece.coordinate.y === this.coordinate.y - 1)
                ) {
                    result.push(piece.coordinate);
                }
            });
        } else {
            var blackPieceList: Piece[] = [];
            if (canKill) {
                blackPieceList = pieceList.filter(piece => {
                    return (
                        (piece.state !== PieceState.Dead) // piece isn't dead
                        && (piece.player !== this.player) //
                        && (piece.coordinate.x >= 3)      // in alowable area
                        && (piece.coordinate.x <= 5)
                        && (piece.coordinate.y >= 7)
                        && (piece.coordinate.y <= 9)
                    );
                });
            } else {
                blackPieceList = pieceList.filter(piece => {
                    return (
                        (piece.state !== PieceState.Dead) // piece isn't dead
                        && (piece.player === this.player) //
                        && (piece !== this)               // Not itself
                        && (piece.coordinate.x >= 3)      // in alowable area
                        && (piece.coordinate.x <= 5)
                        && (piece.coordinate.y >= 7)
                        && (piece.coordinate.y <= 9)
                    );
                });
            }

            blackPieceList.forEach(piece => {
                if (
                        // top right
                        (piece.coordinate.x === this.coordinate.x + 1
                        && piece.coordinate.y === this.coordinate.y + 1)
                        ||
                        // bottom right
                        (piece.coordinate.x === this.coordinate.x + 1
                        && piece.coordinate.y === this.coordinate.y - 1)
                        ||
                        // top left
                        (piece.coordinate.x === this.coordinate.x - 1
                        && piece.coordinate.y === this.coordinate.y + 1)
                        ||
                        // bottom left
                        (piece.coordinate.x === this.coordinate.x - 1
                        && piece.coordinate.y === this.coordinate.y - 1)
                ) {
                    result.push(piece.coordinate);
                }
            });
        }
        return result;
    }
    constructor(coordinate: Coordinate, player: Player, game: ChessGame) {
        super("Guard", coordinate, player, game);
        if (player === Player.Black) {
            this.boundary = { top: 7, bottom: 10, left: 3, right: 6 };
        } else {
            this.boundary = { top: 0, bottom: 3, left: 3, right: 6 };
        }
    }
}