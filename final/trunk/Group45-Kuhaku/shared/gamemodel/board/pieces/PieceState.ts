export enum PieceState {
    Active,
    Dead,
    Haloed,
    Selected
}