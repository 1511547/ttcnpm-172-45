import { Piece } from './pieces/Piece';
export enum CoordinateState {
    Hidden,
    HighlightRed,
    HighlightBlack
}
export class Coordinate {
    x: number;
    y: number;
    private _piece: Piece;
    state: CoordinateState;
    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
        this._piece = null;
        this.state = CoordinateState.Hidden;
    }
    get piece(): Piece {
        return this._piece;
    }
    set piece(piece: Piece) {
        if (piece === null) {
            this._piece = null;
        } else {
            if (piece.coordinate !== this) {
                piece.coordinate = this;
            }
            this._piece = piece;
        }

    }

    public clone(): Coordinate {
        let newCoordinate = new Coordinate(this.x, this.y);
        return newCoordinate;
    }

}