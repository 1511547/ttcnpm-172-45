import { Coordinate } from "./Coordinate";
import { Piece } from "./pieces";

export class GameMove {
    fromX: number;
    fromY: number;
    toX: number;
    toY: number;
    isMove: boolean;
    deadPieceId: number;
    userId: number;
    constructor(from: Coordinate, to: Coordinate, isMove: boolean, guard: Piece = null) {
        this.fromX = from.x;
        this.fromY = from.y;
        this.toX = to.x;
        this.toY = to.y;
        this.isMove = isMove;
        if (!!guard) {
            this.deadPieceId = guard.id;
        } else {
            this.deadPieceId = -1;
        }
    }
}