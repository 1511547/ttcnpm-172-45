import { ePlayerStatus } from "../common/enum/ePlayerStatus";
import { IUserDetail } from "../model/IUserDetail";

export class GamePlayer {
    userId: number;
    username: string;
    avatar: string;
    fullname: string;
    exp: number;
    expPercent: number;
    level: number;
    coin: number;
    isHost: boolean;
    status: ePlayerStatus;
    constructor(id: number, username: string) {
        this.userId = id;
        this.username = username;
        this.avatar = "/assets/avatar/unknown.png";
        this.exp = 0;
        this.coin = 0;
        this.expPercent = 0;
        this.level = 0;
        this.isHost = false;
        this.status = ePlayerStatus.Free;
    }
    public setData(userDetail: IUserDetail) {
        this.avatar = userDetail.Avatar;
        this.coin = userDetail.Money;
        this.level = userDetail.Level;
        this.exp = userDetail.ExpCurrent;
        this.fullname = userDetail.FullName;
        this.expPercent = userDetail.ExpCurrent * 100 / userDetail.ExpNextLevel;
    }
}