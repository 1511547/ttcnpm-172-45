import { SearchEngine } from '../SearchEngine';
import { ChessGame } from '../../board/ChessGame';
import { Player } from '../../board/Player';
import { Coordinate } from '../../board/Coordinate';
import { Piece, PieceState, Guard } from '../../board/pieces';
import { SimpleEvaluator } from '../Evaluators/SimpleEvaluator';
import { GameMove } from '../../board/GameMove';
// alpha-beta pruning
// Alexander Brudno, 1963
export class AlphaBetaMiniMax extends SearchEngine {
    constructor() {
        super();
        this.evaluator = new SimpleEvaluator();
    }
    searchAGoodMove(player: Player, chessGame: ChessGame, timeLimit: number): GameMove {
        this.bestMove = null;

        this.currentGame = chessGame.clone();
        this.cloneCheck(this.currentGame, chessGame);
        let score = this.alphabeta(player, this.maxDepth, this.evaluator.LOST, this.evaluator.WIN);
        this.setStatus(score);
        return this.bestMove;
    }
    private cloneCheck(game1: ChessGame, game2: ChessGame) {
        for (let x = 0; x < 9; x++) {
            for (let y = 0; y < 10; y++) {
                let c1 = game1.getCoordinates[x][y];
                let c2 = game2.getCoordinates[x][y];
                if (!!c1.piece && !!c2.piece && c1.piece.name === c2.piece.name) {
                    continue;
                } else if (!c1.piece && !c2.piece) {
                    continue;
                } else {
                    throw "Clone error.";
                }
            }
        }
        for (let x = 0; x < game1.getAllPieces.length; x++) {
            let p1 = game1.getAllPieces[x];
            let p2 = game2.getAllPieces[x];
            if (!!p1.coordinate) {
                if (p1.coordinate.piece !== p1) {
                    throw "Clone Error";
                }
            }
            if (!!p2.coordinate) {
                if (p2.coordinate.piece !== p2) {
                    throw "Clone Error";
                }
            }
            if (p1.name === p2.name && p1.state === p2.state) {
                if (p1.state !== PieceState.Dead) {
                    if (p1.coordinate.x === p2.coordinate.x && p1.coordinate.y === p2.coordinate.y) {
                        continue;
                    } else {
                        throw "Clone Error";
                    }
                }
            } else {
                throw "Clone Error";
            }
        }
    }
    private alphabeta(player: Player, depth: number, alpha: number, beta: number): number {
        let opponent = (player === Player.Black) ? Player.Red : Player.Black;
        let layer = this.maxDepth - depth;
        let isMaxLayer = (layer % 2 === 0); // own
        let score = this.quickEvaluate(player, layer); // quick evaluation
        if (score !== 0) {
            return isMaxLayer ? score : -score; // WIN or LOSS
        }
        if (depth === 0) {// Leaf
            score = this.evaluator.evaluate(this.currentGame, player);
            return isMaxLayer ? score : -score;
        }
        let possible: GameMove[] = [];
        let listPiece: Piece[] = (player === Player.Black) ? this.currentGame.getBlackPieces
            : this.currentGame.getRedPieces;
        listPiece = listPiece.filter(x => x.state !== PieceState.Dead);
        for (let piece of listPiece) {
            let moves = piece.getMoveOptions().map(m => new GameMove(piece.coordinate, m, true));
            let kills = piece.getKillOptions().map(k => new GameMove(piece.coordinate, k, false, k.piece));
            possible = possible.concat(moves).concat(kills);

        }
        if (possible.length === 0) { // terminal node
            score = this.stalemate(this.maxDepth - depth);
            return isMaxLayer ? score : -score;
        }
        if (isMaxLayer) { // max
            for (let move of possible) {

                this.doMove(move);
                score = this.alphabeta(opponent, depth - 1, alpha, beta);
                this.doUnMove(move); // Unmove
                this.cloneCheck(this.currentGame, this.currentGame);

                if (score > alpha) { // alpha = max(alpha, score)
                    alpha = score;
                    if (depth === this.maxDepth) { // root
                        this.bestMove = move;
                    }
                }
                if (alpha >= beta) {
                    return beta; // beta pruning
                }
            }
            return alpha; // maximum
        } else { // min
            for (let move of possible) {

                this.doMove(move);
                score = this.alphabeta(opponent, depth - 1, alpha, beta);
                this.doUnMove(move); // Unmove
                if (score < beta) { // beta = min(beta, score)
                    beta = score;
                }
                if (alpha >= beta) {
                    return alpha; // alpha pruning
                }
            }
            return beta; // minimum
        }
    }
}