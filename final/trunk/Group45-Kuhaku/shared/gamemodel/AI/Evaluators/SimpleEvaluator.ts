import Evaluator from "../Evaluator";
import { Piece, Cannon, Pawn, Guard, Minister, Knight, General, Chariot } from "../../board/pieces";
import { Player } from "../../board/Player";
import { Coordinate } from "../../board/Coordinate";
import { ChessGame } from "../../board/ChessGame";
import { isNullOrUndefined } from "util";

export class SimpleEvaluator extends Evaluator {
    private static baseValue = function (piece: Piece): number {
        if (piece instanceof Pawn) {
            return 100;
        } else if (piece instanceof Guard || piece instanceof Minister) {
            return 200;
        } else if (piece instanceof Knight) {
            return 400;
        } else if (piece instanceof Cannon) {
            return 450;
        } else if (piece instanceof Chariot) {
            return 1000;
        } else if (piece instanceof General) {
            return 10000;
        } else {
            throw new Error("Unknow type of Piece");
        }
    };
    // value of each reachable position
    private static flexibility = function (piece: Piece): number {
        if (piece instanceof Pawn) {
            return 15;
        } else if (piece instanceof Guard || piece instanceof Minister) {
            return 1;
        } else if (piece instanceof Knight) {
            return 12;
        } else if (piece instanceof Cannon) {
            return 6;
        } else if (piece instanceof Chariot) {
            return 6;
        } else if (piece instanceof General) {
            return 0;
        } else {
            throw new Error("Unknow type of Piece");
        }
    };
    // extra value for pawn
    private static extraPawnValue = function (owner: Player, posX: number, posY: number): number {
        let value: Array<Array<number>> = [
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [70, 70, 70, 70, 70, 70, 70, 70, 70],
            [70, 90, 110, 110, 110, 110, 110, 90, 70],
            [90, 90, 110, 120, 120, 120, 110, 90, 90],
            [90, 90, 110, 120, 120, 120, 110, 90, 90],
            [0, 0, 0, 0, 0, 0, 0, 0, 0]
        ];
        return value[posX][posY];

    };
    constructor() {
        super();
        this.threshold = 19900;
        this.maxSearchDepth = 100;
    }
    evaluate(chessGame: ChessGame, player: Player): number {
        // fisrt round scanning : statistics (per piece)
        // 1. how many pieces guard this piece
        // 2. how many pieces threat this piece
        // 3. how many poosible moves of this piece
        let attack = Array.apply(null, Array(9 * 10)).map(() => 0);
        let guard = Array.apply(null, Array(9 * 10)).map(() => 0);
        let flexibility = Array.apply(null, Array(9 * 10)).map(() => 0);
        let possible: Coordinate[] = [];
        let pieceArray: Piece[] = [];
        if (player === Player.Black) {
            pieceArray = chessGame.getBlackPieces;
        } else {
            pieceArray = chessGame.getRedPieces;
        }
        for (let vPiece of pieceArray) {
            if (!vPiece.isDead()) {
                let guardMoves: Coordinate[] = vPiece.getGuardOptions();
                let killMoves: Coordinate[] = vPiece.getKillOptions();
                possible = possible.concat(killMoves).concat(guardMoves);
                for (let move of possible) {
                    // let from = 10 * move.x + 9 * move.y;
                    let to = 10 * move.x + move.y;
                    if (guardMoves.indexOf(move) !== -1) {
                        guard[to] += 1;
                    } else if ((killMoves.indexOf(move) !== -1)) {
                        let attacker = vPiece;
                        let attackee = chessGame.getCoordAt(move.x, move.y).piece;
                        if (attacker == null || attackee == null) {
                            throw Error("Piece Not Found!");
                        }
                        attack[to] += 3 + Math.floor(0.01 * (SimpleEvaluator.baseValue(attackee) -
                            SimpleEvaluator.baseValue(attacker)));
                        // checkmate
                        if (attackee instanceof General) {
                            if (player !== attackee.player) {
                                return this.WIN;
                            }
                        }
                    }
                }
                possible = [];
            }
        }

        // second round scanning : score
        // 1. base, flexibility, extra value related to position
        // 2. threat, guard
        let self: number = 0;
        let opponent: number = 0;
        for (let x = 0; x < 9; ++x) {
            for (let y = 0; y < 10; ++y) {
                let piece = chessGame.getCoordAt(x, y).piece;
                if (piece == null) {
                    continue;
                }
                let pos = x * 10 + y;
                let pieceValue = 0;
                // Part 1: value of piece itself
                pieceValue += SimpleEvaluator.baseValue(piece); // 1. base value
                pieceValue += SimpleEvaluator.flexibility(piece) * flexibility[pos]; // 2. flexibility
                if (piece instanceof Pawn) {
                    // 3. extra value depends on position
                    pieceValue += SimpleEvaluator.extraPawnValue(piece.player, x, y);
                }
                // Part 2: adjust value according to relationships with other pieces
                let unit = Math.floor(SimpleEvaluator.baseValue(piece) / 16);
                if (attack[pos] > 0) { //  menace from others
                    if (piece.player === player) { // for own pieces
                        if (piece instanceof General) { // king is in check
                            pieceValue -= 20; // state of emergency, must remove the threat immediately
                        } else {
                            pieceValue -= 2 * unit; // the threat vaules 2 unit
                            if (guard[pos] > 0) {
                                pieceValue += unit; // guarded by others, decrease the threat
                            }
                        }
                    } else { // for opponent's pieces
                        if (piece instanceof General) { // checkmate
                            return this.WIN;
                        }
                        pieceValue -= 10 * unit; // attack, the threat vaules 10 unit
                        if (guard[pos] > 0) {
                            pieceValue += 9 * unit; // guarded by others, decrease the threat
                        }
                    }
                    // More threat with capture, less chance for survival,
                    // which is for exchange evaluation.
                    pieceValue -= attack[pos];
                } else {
                    // If a piece is only guarded by others without threat from others,
                    // the active defense should increase safety a little.
                    if (guard[pos] > 0) {
                        pieceValue += 5;
                    }
                }
                if (piece.player === player) {
                    self += pieceValue;
                } else {
                    opponent += pieceValue;
                }
            }
        }
        return self - opponent;
    }
}