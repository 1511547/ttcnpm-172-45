import { ChessGame } from "../board/ChessGame";
import { Player } from "../board/Player";

export default abstract class Evaluator {
    protected threshold: number = 0;
    protected maxSearchDepth: number = 0;
    public abstract evaluate(chessGame: ChessGame, player: Player): number;
    get WIN(): number {
        return this.threshold + this.maxSearchDepth;
    }
    get LOST(): number {
        return -(this.threshold + this.maxSearchDepth);
    }
    win(step: number): number {
        return this.threshold + this.maxSearchDepth - step;
    }
    lost(step: number): number {
        return -(this.threshold + this.maxSearchDepth - step);
    }
    isWin(score: number, step: number): boolean {
        return score >= this.threshold + this.maxSearchDepth - step;
    }
    isLost(score: number, step: number): boolean {
        return score <= -(this.threshold + this.maxSearchDepth - step);
    }
}