import { ChessGame } from "../board/ChessGame";
import Evaluator from "./Evaluator";
import STATUS from "../../../shared/common/enum/STATUS";
import { Coordinate } from "../board/Coordinate";
import { Piece } from "../board/pieces";
import { Player } from "../board/Player";
import { GameMove } from "../board/GameMove";

export abstract class SearchEngine {
    protected currentGame!: ChessGame;
    protected evaluator!: Evaluator;
    protected status: STATUS = STATUS.NotEnded;
    protected maxDepth: number = 3;
    protected bestMove: GameMove = null;
    protected resignStep: number = 1;
    protected winStep: number = 0;

    protected searchNodes: number = 0; // only for log

    // From Coordinate A to Coordinate B
    public abstract searchAGoodMove(player: Player, game: ChessGame, timeLimit?: number): GameMove;
    public get STATUS(): STATUS {
        return this.status;
    }
    setSearchDepth(depth: number): void {
        this.maxDepth = depth;
    }
    setEvaluator(evaluator: Evaluator): void {
        this.evaluator = evaluator;
    }
    protected doMove(move: GameMove): void {
        let from = this.currentGame.getCoordAt(move.fromX, move.fromY);
        let to = this.currentGame.getCoordAt(move.toX, move.toY);
        let result = true;
        if (move.isMove) {
            result = this.currentGame.move(from.piece, to);
        } else {
            result = this.currentGame.kill(from.piece, to.piece);
        }
        if (!result) {
            throw "Error on move";
        }
    }
    protected doUnMove(move: GameMove): void {
        let result = true;
        if (move.isMove) {
            result = this.currentGame.unMove(move);
        } else {
            let piece = this.currentGame.getPieceById(move.deadPieceId);
            result = this.currentGame.unkill(move);
        }

    }
    protected quickEvaluate(player: Player, depth: number): number {
        // depth = distance from root
        // depth=0, 2, 4, 6,...: self;
        // depth=1, 3, 5, 7,...: oppenent
        let status = this.currentGame.quickTestStatus(player);
        switch (status) {
            case STATUS.Win:
                return this.evaluator.win(depth); // depth small first, win
            case STATUS.Lost:
                return this.evaluator.lost(depth); // lost
            case STATUS.FlyCheck:
                // Notice: If search engine can find out fly check as early as possible,
                // the fly check found must be caused by oppenent, because the fly check
                // by self should be tested in previous search.
                return this.evaluator.win(depth);
            case STATUS.Impossbile:
                throw new Error("Impossible!");
            default:
        }
        return 0; // STATUS.NotEnded
    }
    protected stalemate(step: number) {
        return this.evaluator.lost(step);
    }
    protected setStatus(score: number) {
        if (this.evaluator.isWin(score, this.winStep)) {
            this.status = STATUS.Win;
        } else if (this.evaluator.isLost(score, this.resignStep)) {
            this.status = STATUS.Lost;
        } else {
            this.status = STATUS.NotEnded;
        }
    }
}
