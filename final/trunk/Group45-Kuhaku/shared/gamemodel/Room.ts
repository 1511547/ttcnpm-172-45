import { GamePlayer } from "./GamePlayer";
import { eRoomType } from "../common/enum/eRoomType";

export class Room {
    id: number;
    name: string;
    owner: number;
    type: eRoomType;
    bet: number;
    redPlayer: GamePlayer;
    blackPlayer: GamePlayer;
    viewers: GamePlayer[];
    constructor(id: number, name: string, typ: eRoomType) {
        this.id = id;
        this.type = typ;
        this.name = name;
    }
}