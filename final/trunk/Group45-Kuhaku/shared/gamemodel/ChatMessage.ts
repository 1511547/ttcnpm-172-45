export class ChatMessage {
    username: string;
    chatMessage: string;

    constructor(user: string, msg: string) {
        this.username = user;
        this.chatMessage = msg;
    }
}