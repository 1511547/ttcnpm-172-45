import { IUser } from "./IUser";

export interface IUserDetail {
    Id?: number;
    FullName?: string;
    Avatar?: string;
    Country?: string;
    Gender?: boolean;
    Rank?: number;
    WinRate?: number;
    Level?: number;
    Money?: number;
    ExpCurrent?: number;
    ExpNextLevel?: number;
    JoinDate?: Date;
    LastLogin?: Date;
    Seen?: number;
    Introduce?: string;
    User?: IUser;
    // history:

}