export interface IUser {
    Id?: number;
    Username?: string;
    Password?: string;
    Email?: string;
    IsAdmin?: boolean;
    Status?: boolean;
    StatusMessage?: string;
}