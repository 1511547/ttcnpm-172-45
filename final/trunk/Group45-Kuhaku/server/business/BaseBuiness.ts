import { Connection, createConnection, getConnection } from 'typeorm';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';
import * as Debug from 'debug';
export class BaseBuiness {
    protected connection: Connection;
    protected debug: Debug.IDebugger;
    constructor(name: string) {
        this.init();
        this.debug = Debug(name);
        this.debug.enabled = true;
    }
    protected init(): void {
        this.connection = getConnection("kuhaku-connection");
    }
}