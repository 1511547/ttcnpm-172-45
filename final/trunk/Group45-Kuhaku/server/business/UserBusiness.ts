import { User } from '../entities/User';
import { getRepository, Repository, getConnection } from 'typeorm';
import { IUser } from '../../shared/model/IUser';
import { IUserDetail } from '../../shared/model/IUserDetail';
import { BaseBuiness } from './BaseBuiness';
import * as validator from 'validator';
import { Result } from '../../shared/common/Result';
import * as Debug from 'debug';

import { UserDetail } from '../entities/UserDetail';
export class UserBusiness extends BaseBuiness {
    protected userRepository: Repository<User>;
    protected userDetailRepository: Repository<UserDetail>;
    constructor() {
        super("UserBusiness");
        this.userRepository = this.connection.getRepository(User);
        this.userDetailRepository = this.connection.getRepository(UserDetail);
    }
    async exist(user: IUser): Promise<boolean> {
        let val = await this.userRepository.findOne(user);
        if (val !== undefined) {
            return true;
        } else {
            return false;
        }
    }
    async getAllUser(): Promise<User[]> {
        return await this.userRepository.find();
    }

    /**
     * Get User
     * @param username Username
     * @returns return User if username is exist and return undefined if username is not exist
     */
    async getUserByUsername(username: string): Promise<User | undefined> {
        try {
            let info: IUser = {
                Username: username
            };
            let user = await this.userRepository.findOne(info);
            return user;
        } catch (err) {
            this.debug(err);
            return undefined;
        }

    }

    validate(user: IUser): Result.ResultInfo {
        if (!user.Username || !validator.isLength(user.Username, { min: 3, max: 255 })) {
            return Result.UsernameInvalid;
        }
        if (!user.Email || !validator.isEmail(user.Email) || !validator.isLength(user.Email, { min: 6, max: 255 })) {
            return Result.EmailInvalid;
        }
        if (!user.Password || !validator.isMD5(user.Password)) {
            return Result.PasswordInvalid;
        }
        return Result.Success;
    }
    async addNewUser(user: IUser): Promise<Result.ResultInfo> {
        if (await this.exist(user)) {
            return Result.UserExist;
        }
        let validateResult = this.validate(user);
        if (validateResult !== Result.Success) {
            return validateResult;
        }
        try {

            const info = new User();
            info.Username = user.Username;
            info.Password = user.Password;
            info.Email = user.Email;
            const detail = new UserDetail();
            detail.User = info;
            await this.userRepository.save(info);
            await this.userDetailRepository.save(detail);
            return Result.Success;
        } catch (err) {
            return new Result.Exception(err);
        }
    }
    async login(username: string, password: string): Promise<Result.ResultInfo> {
        let userInfo: IUser = {
            Username: username,
        };
        try {
            let user = await this.userRepository.findOne(userInfo);
            if (user === undefined) {
                return Result.UserNotExist;
            } else {
                if (user.Password === password) {
                    return Result.Success;
                } else {
                    return Result.WrongLoginData;
                }
            }
        } catch (err) {
            return new Result.Exception(err);
        }

    }
    async getUserDetailById(id: number): Promise<UserDetail> {

        let userDetail = await this.userDetailRepository
            .createQueryBuilder("UserDetail")
            .innerJoinAndSelect("UserDetail.User", "User")
            .where("User.Id = :id", { id: id })
            .getOne();
        return userDetail;
    }
    async getUserDetailByUsername(username: string): Promise<UserDetail> {
        return this.userDetailRepository
            .createQueryBuilder("UserDetail")
            .innerJoinAndSelect("UserDetail.User", "User")
            .where("User.Username = :username", { username: username })
            .getOne();
    }

    async addDefaultUserDetail(user: IUser) {
        let userDetail: IUserDetail = {
            FullName: "",
            User: user,
            ExpCurrent: 10,
            ExpNextLevel: 20
        };
        await this.userDetailRepository.save(userDetail);
    }

    async updateUsetDetail(username: string, detail: IUserDetail) {
        try {
            let userDetail = await this.getUserDetailByUsername(username);
            userDetail.Avatar = detail.Avatar ? detail.Avatar : userDetail.Avatar;
            userDetail.Country = detail.Country ? detail.Country : userDetail.Country;
            userDetail.FullName = detail.FullName ? detail.FullName : userDetail.FullName;
            userDetail.Introduce = detail.Introduce ? detail.Introduce : userDetail.Introduce;
            userDetail.ExpCurrent = detail.ExpCurrent ? detail.ExpCurrent : userDetail.ExpCurrent;
            userDetail.ExpNextLevel = detail.ExpNextLevel ? detail.ExpNextLevel : userDetail.ExpNextLevel;
            userDetail.LastLogin = detail.LastLogin ? detail.LastLogin : userDetail.LastLogin;
            userDetail.Money = detail.Money ? detail.Money : userDetail.Money;
            userDetail.Level = detail.Level ? detail.Level : userDetail.Level;
            userDetail.Rank = detail.Rank ? detail.Rank : userDetail.Rank;
            userDetail.Seen = detail.FullName ? detail.Seen : userDetail.Seen;
            userDetail.Gender = detail.Gender ? detail.Gender : userDetail.Gender;
            userDetail.WinRate = detail.WinRate ? detail.WinRate : userDetail.WinRate;
            await this.userDetailRepository.save(userDetail);
            return Result.Success;
        } catch (e) {
            return new Result.Exception(e);
        }
    }
}
