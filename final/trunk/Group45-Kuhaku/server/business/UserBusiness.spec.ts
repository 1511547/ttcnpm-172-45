import 'jest';
import { createConnection, Connection } from "typeorm";
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';
import { UserBusiness } from './UserBusiness';
import { IUser } from '../../shared/model/IUser';
import { User } from '../entities/User';
import * as CryptoJS from 'crypto-js';
import { Result } from '../../shared/common/Result';
import { IUserDetail } from '../../shared/model/IUserDetail';
let connection: Connection;
let userBusiness: UserBusiness;
beforeAll(async () => {
    let config: MysqlConnectionOptions = {
        "name": "kuhaku-connection",
        "type": "mysql",
        "host": process.env.DB_HOST || "localhost",
        "port": 3306,
        "username": "kuhaku",
        "password": "123456",
        "database": "kuhaku",
        "charset": "utf8mb4",
        "entities": [
            "server/entities/**/*.ts"
        ]
    };
    connection = await createConnection(config);
    await connection.synchronize(true);
    userBusiness = new UserBusiness();

});
test('Connected Database', async () => {
    expect(connection.isConnected).toBe(true);
});

describe('Insert User', () => {
    test('insert new user', async () => {
        let pass = CryptoJS.MD5("123456").toString();
        let user: IUser = {
            Username: "duckhan",
            Password: pass,
            Email: "duckhan123@gmail.com"
        };
        let result = await userBusiness.addNewUser(user);
        expect(result).toBe(Result.Success);
    });
    test('insert exist user', async () => {
        let pass = CryptoJS.MD5("123456").toString();
        let user: IUser = {
            Username: "duckhan",
            Password: pass,
            Email: "duckhan123@gmail.com"
        };
        let result = await userBusiness.addNewUser(user);
        expect(result).toBe(Result.UserExist);
    });
    test('insert user with invalid email', async () => {
        let pass = CryptoJS.MD5("123456").toString();
        let user: IUser = {
            Username: "duckhan2",
            Password: pass,
            Email: "duckhan123gmail.com"
        };
        let result = await userBusiness.addNewUser(user);
        expect(result).toBe(Result.EmailInvalid);
    });
    test('insert user with invalid md5 password', async () => {
        let pass = '4515471645454545';
        let user: IUser = {
            Username: "duckhan33",
            Password: pass,
            Email: "duckhan123@gmail.com"
        };
        let result = await userBusiness.addNewUser(user);
        expect(result).toBe(Result.PasswordInvalid);
    });
});
describe('User login', () => {
    test('login with correct infomation', async () => {
        let pass = CryptoJS.MD5("123456").toString();
        let username = "duckhan";
        let result = await userBusiness.login(username, pass);
        expect(result).toBe(Result.Success);
    });
    test('login with wrong username not exist', async () => {
        let pass = CryptoJS.MD5("123456").toString();
        let username = "duck32n";
        let result = await userBusiness.login(username, pass);
        expect(result).toBe(Result.UserNotExist);
    });
    test('login with wrong password', async () => {
        let pass = CryptoJS.MD5("12322456").toString();
        let username = "duckhan";
        let result = await userBusiness.login(username, pass);
        expect(result).toBe(Result.WrongLoginData);
    });

});

describe('User detail', () => {
    test('Get exist user detail', async () => {
        let result = await userBusiness.getUserDetailById(1);
        expect(result).toBeDefined();
    });
    test("Update user detail", async () => {
        let result = await userBusiness.updateUsetDetail("duckhan", {
            FullName: "Nguyen Duc Khan"
        });
        expect(result).toBe(Result.Success);
    });
    test("Get new user detail", async () => {
        let detait = await userBusiness.getUserDetailByUsername("duckhan");
        expect(detait).toBeDefined();
        expect(detait.FullName).toBe("Nguyen Duc Khan");
    });
});

afterAll(async () => {
    await connection.close();
});