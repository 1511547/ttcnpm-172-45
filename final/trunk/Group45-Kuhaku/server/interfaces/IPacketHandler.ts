import { GameClient } from "../client/GameClient";
import { Packet } from "../../shared/common/Packet";
import { Code } from "typeorm";

export interface IPacketHandler {
    HandlePacket(client: GameClient, packet: Packet): void;
}