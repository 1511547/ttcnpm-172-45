import { Packet } from "../../shared/common/Packet";

export interface IGameSocketOut {
    SendPacket(packet: Packet);
    SendUserLogout(msg: string);
}