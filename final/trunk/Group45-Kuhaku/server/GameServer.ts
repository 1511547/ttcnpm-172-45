import * as jwt from "jsonwebtoken";
import { Server, Socket } from 'socket.io';
import { GameClient } from "./client/GameClient";
import { LoginManager } from "./manager/LoginManager";
import { Config } from './config';
import { json } from "body-parser";
import { IUser } from "../shared/model/IUser";
import { PacketManager } from "./manager/PacketManager";
import { GamePlayer } from "./client/GamePlayer";
import { UserBusiness } from "./business/UserBusiness";
import * as Debug from 'debug';
const debug = Debug('GameServer');
debug.enabled = true;
export class GameServer {

    private server: Server;

    constructor(serverIO: Server) {
        this.server = serverIO;
    }

    public Start(): void {
        debug('Start game server');
        this.server.on('connection', this.onConnect);
    }

    public async Init() {
        debug('Init game server');
        await PacketManager.Instance.Init();
    }
    
    private async onConnect(socket: Socket) {
        let decoded = jwt.decode(socket.handshake.query.token);
        if (typeof decoded !== "string") {
            let userBusiness = new UserBusiness();
            let userDetail = await userBusiness.getUserDetailById(decoded.Id);
            debug(userDetail);
            let player = new GamePlayer(decoded.Id, decoded.Username, socket, userDetail);
            LoginManager.Instance.AddPlayer(decoded.Id, player);
           
        }
    }
}