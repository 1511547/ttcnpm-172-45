import { eRoomType } from "../../shared/common/enum/eRoomType";
import { GamePlayer } from "../client/GamePlayer";
import { ePlayerStatus } from "../../shared/common/enum/ePlayerStatus";
import { RoomManager } from "../manager/RoomManager";
import { Room as RoomModel } from '../../shared/gamemodel/Room';
import { GamePlayer as Player } from '../../shared/gamemodel/GamePlayer';
import { ChessGame } from '../../shared/gamemodel/board/ChessGame';
import { GameMove } from "../../shared/gamemodel/board/GameMove";
import { Player as PlayerType } from '../../shared/gamemodel/board/Player';
import { ePacketCode } from "../../shared/common/enum/ePacketCode";
import * as Debug from 'debug';
const debug = Debug('Room');
debug.enabled = true;

export class Room {

    public Name: string;

    public Host: GamePlayer;

    public BlackPlayer: GamePlayer;

    public RedPlayer: GamePlayer;
    private gameOver: boolean;
    private isPlaying: boolean;
    public Bet: number;
    private game: ChessGame;
    public get IsGameOver() {
        return this.gameOver;
    }
    public get IsPlaying() {
        return this.isPlaying;
    }
    public get Game(): ChessGame {
        return this.game;
    }
    public get IsEmpty(): boolean {
        return !this.Host && !this.RedPlayer && !this.BlackPlayer;
    }

    private id: number;
    public get Id(): number {
        return this.id;
    }

    private roomType: eRoomType;
    public get RoomType(): eRoomType {
        return this.roomType;
    }

    private viewers: GamePlayer[];
    public get Viewers(): GamePlayer[] {
        return this.viewers;
    }

    constructor(id: number, name: string, roomType: eRoomType) {
        this.id = id;
        this.Name = name;
        this.roomType = roomType;
        this.viewers = [];
        this.game = new ChessGame();
    }

    public NewGame(): ChessGame {
        this.game = new ChessGame();
        return this.game;
    }

    public get AllPlayers(): GamePlayer[] {
        return this.viewers.concat(this.RedPlayer).concat(this.BlackPlayer ? this.BlackPlayer : []);
    }

    public RemovePlayer(player: GamePlayer) {
        debug("remove " + player.Username);
        player.CurrentRoom = null;
        if (player === this.Host) {
            if (!!this.BlackPlayer) {
                debug("host " + this.BlackPlayer.Username);
                this.Host = this.BlackPlayer;
                this.RedPlayer = this.BlackPlayer;
                this.BlackPlayer = null;
            } else {
                if (this.viewers.length > 0) {
                    this.Host = this.viewers[0];
                    delete this.viewers[0];
                } else {
                    RoomManager.Instance.RemoveRoom(this.Id);
                }
            }
        } else if (player === this.BlackPlayer) {
            this.BlackPlayer = null;
        } else {
            let index = this.viewers.indexOf(player);
            if (index > -1) {
                delete this.viewers[index];
            }
        }
        player.Status = ePlayerStatus.Free;
    }

    public AddPlayer(player: GamePlayer) {
        player.Status = ePlayerStatus.InRoom;
        player.CurrentRoom = this;
        if (!this.Host) {
            this.Host = player;
            this.RedPlayer = player;
        } else if (this.Host === this.BlackPlayer && !this.RedPlayer) {
            this.RedPlayer = player;
        } else if (this.Host === this.RedPlayer && !this.BlackPlayer) {
            this.BlackPlayer = player;
        }
    }

    public get CurrentTurn(): GamePlayer {
        if (this.game.CurrentTurn === PlayerType.Red) {
            return this.RedPlayer;
        } else if (this.game.CurrentTurn === PlayerType.Black) {
            return this.BlackPlayer;
        } else {
            return null;
        }
    }

    public Move(move: GameMove): boolean {
        let from = this.game.getCoordAt(move.fromX, move.fromY);
        let to = this.game.getCoordAt(move.toX, move.toY);
        if (move.isMove) {
            return this.game.move(from.piece, to);
        } else {
            return this.game.kill(from.piece, to.piece);
        }

    }

    public GameOver(win: PlayerType, isDraw: boolean = false) {
        let winner: GamePlayer;
        let loser: GamePlayer;
        if (win === PlayerType.Red) {
            winner = this.RedPlayer;
            loser = this.BlackPlayer;
        } else if (win === PlayerType.Black) {
            winner = this.BlackPlayer;
            loser = this.RedPlayer;
        }
        if (!isDraw) {
            winner.AddExp(100);
            winner.AddMoney(this.Bet);
            winner.Save();
            loser.AddExp(10);
            loser.AddMoney(-this.Bet);
            winner.Save();
        } else {
            this.RedPlayer.AddExp(50);
            this.BlackPlayer.AddExp(50);
            this.RedPlayer.Save();
            this.BlackPlayer.Save();
        }
        this.BlackPlayer.Status = ePlayerStatus.InRoom;
        this.RedPlayer.Status = ePlayerStatus.InRoom;
        this.gameOver = true;
    }
    public toRoomModel(): RoomModel {
        const room = this;
        let r = new RoomModel(room.Id, room.Name, this.RoomType);
        r.bet = room.Bet;
        r.redPlayer = this.RedPlayer.ToSharedModel();
        r.redPlayer.isHost = true;
        if (room.BlackPlayer) {
            r.blackPlayer = this.BlackPlayer.ToSharedModel();
        }
        return r;
    }
}