import "reflect-metadata";
import { listen } from "socket.io";
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as path from 'path';
import { UserController } from './controllers/UserController';
import { MysqlConnectionOptions } from "typeorm/driver/mysql/MysqlConnectionOptions";
import { createConnection, AdvancedConsoleLogger } from "typeorm";
import { User } from './entities/User';
import { GameServer } from "./GameServer";
import * as Debug from 'debug';
import { UserDetail } from "./entities/UserDetail";
import * as jwt from 'jsonwebtoken';
import { Config } from './config';
let debug = Debug('app');
let config = Config.DBCONFIG;
debug.enabled = true;

createConnection(config).then(async (conn) => {
  await conn.synchronize(false);
  let app = express();
  app.use(bodyParser.json({ type: 'application/json' }));
  app.use(cookieParser());
  app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'client/index.html'));
  });
  app.use(express.static(path.join(__dirname, 'client')));
  app.use('/user', UserController);
  app.get("*", (req, res) => {
    res.redirect("/");
  });
  let httpServer = app.listen(3000, () => {
    debug("Listening on port 3000");
  });
  let serverIO = listen(httpServer);

  serverIO.use((socket, next) => {
    if (socket.handshake.query && socket.handshake.query.token) {
      let token = socket.handshake.query.token;
      jwt.verify(token, Config.JWT_SECRET, (err, decode) => {
        if (err) {
          socket.disconnect();
        } else {
          next();
        }
      });
    }
  });
  let gameServer = new GameServer(serverIO);
  await gameServer.Init();
  gameServer.Start();
}).catch((err) => {
  debug(err);
});
