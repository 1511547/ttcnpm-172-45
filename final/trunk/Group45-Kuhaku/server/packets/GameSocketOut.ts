import { IGameSocketOut } from "../interfaces/IGameSocketOut";
import { Socket } from "socket.io";
import { GameClient } from "../client/GameClient";
import { Packet } from "../../shared/common/Packet";
import { ePacketCode } from "../../shared/common/enum/ePacketCode";

export class GameSocketOut implements IGameSocketOut {

    private gameClient: GameClient;
    public get GameClient(): GameClient {
        return this.gameClient;
    }

    constructor(client: GameClient) {
        this.gameClient = client;
    }

    public SendUserLogout(msg: string) {
        let packet = new Packet(ePacketCode.Logout);
        packet.content = { message: msg };
        this.SendPacket(packet);
    }

    public SendPacket(packet: Packet) {

        this.gameClient.Socket.emit('packet', packet);
    }
}