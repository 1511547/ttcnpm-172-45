import { IPacketHandler } from "../../interfaces/IPacketHandler";
import { Handler } from "./HandlerDecorator";
import { ePacketCode } from "../../../shared/common/enum/ePacketCode";
import { GameClient } from "../../client/GameClient";
import { Packet } from "../../../shared/common/Packet";
import { ePlayerStatus } from "../../../shared/common/enum/ePlayerStatus";
import { Player } from "../../../shared/gamemodel/board/Player";
@Handler(ePacketCode.GameRequestLose)
export default class GameRequestLoseHandler implements IPacketHandler {
    HandlePacket(client: GameClient, packet: Packet): void {
        try {
            if (client.Player.Status & ePlayerStatus.Playing) {
                let gameOverPkg = new Packet(ePacketCode.GameOver);
                let winner: Player;
                if (client.Player === client.Player.CurrentRoom.BlackPlayer) {
                    winner = Player.Red;
                } else if (client.Player === client.Player.CurrentRoom.RedPlayer) {
                    winner = Player.Black;
                }
                client.Player.CurrentRoom.GameOver(winner, false);
                gameOverPkg.content = { winner: winner };
                client.Player.CurrentRoom.AllPlayers.forEach(p => p.Client.Send(gameOverPkg));
            }
        } catch (e) {
            packet.sussess = false;
            packet.content = e.message;
            client.Send(packet);
        }
    }
}