import { IPacketHandler } from "../../interfaces/IPacketHandler";
import UserLogoutHandler from "./UserLogoutHandler";
import * as fs from 'fs';
import * as path from 'path';
import { ePacketCode } from "../../../shared/common/enum/ePacketCode";
import * as Debug from 'debug';
const debug = Debug("HandlersLoader");
debug.enabled = true;

export class HandlersLoader {
    public static Load(): Promise<Map<ePacketCode, IPacketHandler>> {
        debug('Loading');
        const dir = fs.readdirSync(__dirname);
        const files = dir.filter(x => x.endsWith('Handler.ts') || x.endsWith('Handler.js'));
        let result: Map<ePacketCode, IPacketHandler> = new Map();
        let loaded = 0;
        return new Promise<Map<ePacketCode, IPacketHandler>>(resolve => {
            files.forEach((file, index) => {
                import(path.join(__dirname, file)).then(handler => {
                    loaded++;
                    if (handler && handler.default && handler.default.prototype.HandlePacket !== undefined) {
                        let code = Reflect.getMetadata("HandlerCode", handler.default);
                        if (code) {
                            let obj: IPacketHandler = Object.create(handler.default.prototype);
                            debug(obj);
                            result.set(code, obj);
                        }
                    }
                    if (loaded === files.length) {
                        resolve(result);
                    }
                })
                    .catch(err => {
                        loaded++;
                        debug(file);
                        debug(err);
                        if (loaded === files.length) {
                            resolve(result);
                        }
                    });
            });
        });
    }
}