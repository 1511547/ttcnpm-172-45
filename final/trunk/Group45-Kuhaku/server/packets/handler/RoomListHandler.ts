import { IPacketHandler } from "../../interfaces/IPacketHandler";
import { GameClient } from "../../client/GameClient";
import { Packet } from "../../../shared/common/Packet";
import { ePacketCode } from "../../../shared/common/enum/ePacketCode";
import { Handler } from "./HandlerDecorator";
import { RoomManager } from "../../manager/RoomManager";
import { Room } from '../../../shared/gamemodel/Room';
import { GamePlayer } from "../../../shared/gamemodel/GamePlayer";
@Handler(ePacketCode.GetRoomList)
export default class RoomListHandler implements IPacketHandler {
    HandlePacket(client: GameClient, packet: Packet): void {
        let result = RoomManager.Instance.Rooms.map((room) => {
            return room.toRoomModel();
        });
        let pkg = new Packet(ePacketCode.GetRoomList);
        pkg.content = result;
        client.Out.SendPacket(pkg);
    }
}