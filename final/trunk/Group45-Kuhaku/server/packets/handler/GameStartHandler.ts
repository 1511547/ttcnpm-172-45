import { IPacketHandler } from "../../interfaces/IPacketHandler";
import { Handler } from "./HandlerDecorator";
import { ePacketCode } from "../../../shared/common/enum/ePacketCode";
import { GameClient } from "../../client/GameClient";
import { Packet } from "../../../shared/common/Packet";
import { ePlayerStatus } from "../../../shared/common/enum/ePlayerStatus";
import { eGameStatus } from "../../../shared/common/enum/eGameStatus";
@Handler(ePacketCode.GameStart)
export default class GameStartHandler implements IPacketHandler {
    HandlePacket(client: GameClient, pkg: Packet) {
        try {
            if (client.Player.Status & ePlayerStatus.InRoom) {
                let room = client.Player.CurrentRoom;
                if (!room.BlackPlayer) {
                    throw new Error("Phòng không đủ người.");
                }
                if (room.BlackPlayer.Status !== ePlayerStatus.Ready) {
                    throw new Error("Đối thủ chưa sẵn sàng.");
                }
                if (room.RedPlayer !== client.Player) {
                    throw new Error("Bạn không phải chủ phòng.");
                }
                client.Player.Status = ePlayerStatus.Playing;
                room.BlackPlayer.Status = ePlayerStatus.Playing;
                room.NewGame();
                room.Game.GameStatus = eGameStatus.Playing;
                room.AllPlayers.forEach(p => {
                    p.Client.Send(pkg);
                });
            } else {
                throw new Error("Người chơi không có trong phòng.");
            }
        } catch (e) {
            pkg.sussess = false;
            pkg.content = e.message;
            client.Send(pkg);
        }

    }
}