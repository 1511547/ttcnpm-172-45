import { IPacketHandler } from "../../interfaces/IPacketHandler";
import { Handler } from "./HandlerDecorator";
import { ePacketCode } from "../../../shared/common/enum/ePacketCode";
import { GameClient } from "../../client/GameClient";
import { Packet } from "../../../shared/common/Packet";
import { GameMove } from '../../../shared/gamemodel/board/GameMove';
import { ePlayerStatus } from "../../../shared/common/enum/ePlayerStatus";
import { Player } from "../../../shared/gamemodel/board/Player";
import { GamePlayer } from "../../../shared/gamemodel/GamePlayer";
@Handler(ePacketCode.GameMove)
export default class GameMoveHandler implements IPacketHandler {
    HandlePacket(client: GameClient, packet: Packet) {
        let move: GameMove = packet.content;
        let pkg = new Packet(ePacketCode.GameMove);
        if (client.Player.Status & ePlayerStatus.Playing && !!client.Player.CurrentRoom) {
            if (client.Player.PlayerId === move.userId && client.Player.CurrentRoom.CurrentTurn) {
                if (client.Player.CurrentRoom.Move(move)) {
                    pkg.content = move;
                    client.Player.CurrentRoom.AllPlayers.forEach(p => {
                        if (p !== client.Player) {
                            p.Client.Send(pkg);
                        }
                    });
                    let game = client.Player.CurrentRoom.Game;
                    let room = client.Player.CurrentRoom;
                    if (game.IsGameOver) {
                        let gameOverPkg = new Packet(ePacketCode.GameOver);
                        gameOverPkg.content = { winner: null };
                        if (game.Winner === Player.Red) {
                            gameOverPkg.content.winner = Player.Red;
                        } else {
                            gameOverPkg.content.winner = Player.Black;
                        }
                        room.GameOver(gameOverPkg.content.winner, false);
                        room.AllPlayers.forEach(p => p.Client.Send(gameOverPkg));
                    }
                }
            }

        }
    }
}