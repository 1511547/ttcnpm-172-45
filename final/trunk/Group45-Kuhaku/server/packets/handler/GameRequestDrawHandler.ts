import { IPacketHandler } from "../../interfaces/IPacketHandler";
import { Handler } from "./HandlerDecorator";
import { ePacketCode } from "../../../shared/common/enum/ePacketCode";
import { GameClient } from "../../client/GameClient";
import { Packet } from "../../../shared/common/Packet";
import { ePlayerStatus } from "../../../shared/common/enum/ePlayerStatus";
import { Player } from "../../../shared/gamemodel/board/Player";
@Handler(ePacketCode.GameRequestDraw)
export default class GameRequestDrawHandler implements IPacketHandler {
    HandlePacket(client: GameClient, packet: Packet): void {
        try {
            if (client.Player.Status & ePlayerStatus.Playing) {
                let gameOverPkg = new Packet(ePacketCode.GameOver);
                let winner: Player;
                if (client.Player === client.Player.CurrentRoom.BlackPlayer) {
                    winner = Player.Black;
                } else if (client.Player === client.Player.CurrentRoom.RedPlayer) {
                    winner = Player.Red;
                }
                client.Player.CurrentRoom.GameOver(-1, true);
                gameOverPkg.content = { winner: -1 };
                client.Player.CurrentRoom.AllPlayers.forEach(p => p.Client.Send(gameOverPkg));
            }
        } catch (e) {
            packet.sussess = false;
            packet.content = e.message;
            client.Send(packet);
        }
    }
}