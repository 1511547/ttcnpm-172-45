import { IPacketHandler } from "../../interfaces/IPacketHandler";
import { Handler } from "./HandlerDecorator";
import { ePacketCode } from "../../../shared/common/enum/ePacketCode";
import { GameClient } from "../../client/GameClient";
import { Packet } from "../../../shared/common/Packet";
import { LoginManager } from "../../manager/LoginManager";

@Handler(ePacketCode.GetProfileData)
export default class GetProfileDataHandler implements IPacketHandler {
    HandlePacket(client: GameClient, packet: Packet): void {
        let pkg = new Packet(ePacketCode.GetProfileData);
        pkg.content = client.Player.UserDetail;
        client.Send(pkg);
    }
}