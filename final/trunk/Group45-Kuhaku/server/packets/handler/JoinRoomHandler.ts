import { Handler } from './HandlerDecorator';
import { IPacketHandler } from '../../interfaces/IPacketHandler';
import { GameClient } from '../../client/GameClient';
import { Packet } from '../../../shared/common/Packet';
import { ePacketCode } from '../../../shared/common/enum/ePacketCode';
import { RoomManager } from '../../manager/RoomManager';

@Handler(ePacketCode.JoinRoom)
export default class JoinRoomHandler implements IPacketHandler {
    HandlePacket(client: GameClient, packet: Packet): void {
        let { roomId } = packet.content;
        let room = RoomManager.Instance.getRoomById(roomId);
        if (!!room) {
            room.AddPlayer(client.Player);
            let refreshPkg = new Packet(ePacketCode.GetRoomInfo);
            refreshPkg.content = room.toRoomModel();
            room.AllPlayers.filter(x => x !== client.Player).forEach(x => x.Client.Send(refreshPkg));
            client.Send(packet);
        }
    }
}