import { IPacketHandler } from "../../interfaces/IPacketHandler";
import { GameClient } from "../../client/GameClient";
import { Packet } from "../../../shared/common/Packet";
import { ePacketCode } from "../../../shared/common/enum/ePacketCode";
import { LoginManager } from "../../manager/LoginManager";
import { Handler } from "./HandlerDecorator";

@Handler(ePacketCode.Logout)
export default class UserLogoutHandler implements IPacketHandler {
    public HandlePacket(client: GameClient, packet: Packet) {
        // tslint:disable-next-line:no-console
        console.log("Logout Handler");
        LoginManager.Instance.RemovePlayer(client.Player.PlayerId);
    }
}