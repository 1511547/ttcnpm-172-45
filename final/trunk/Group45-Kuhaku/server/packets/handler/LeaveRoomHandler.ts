import { Handler } from './HandlerDecorator';
import { IPacketHandler } from '../../interfaces/IPacketHandler';
import { GameClient } from '../../client/GameClient';
import { Packet } from '../../../shared/common/Packet';
import { ePacketCode } from '../../../shared/common/enum/ePacketCode';
import { RoomManager } from '../../manager/RoomManager';
import { Player } from '../../../shared/gamemodel/board/Player';
import { eGameStatus } from '../../../shared/common/enum/eGameStatus';

@Handler(ePacketCode.LeaveRoom)
export default class LeaveRoomHandler implements IPacketHandler {
    HandlePacket(client: GameClient, packet: Packet): void {
        let room = client.Player.CurrentRoom;
        if (!!room) {
            if (room.AllPlayers.length >= 2 && !!room.Game && room.Game.GameStatus === eGameStatus.Playing) {
                let gameOverPkg = new Packet(ePacketCode.GameOver);
                if (client.Player === room.RedPlayer) {
                    room.GameOver(Player.Black, false);
                    gameOverPkg.content = { winner: Player.Black, isLeaveRoom: true };
                } else if (client.Player === room.BlackPlayer) {
                    room.GameOver(Player.Red, false);
                    gameOverPkg.content = { winner: Player.Red, isLeaveRoom: true };
                }
                room.AllPlayers.forEach(p => {
                    if (p !== client.Player) {
                        p.Client.Send(gameOverPkg);
                    }
                });
            }
            room.RemovePlayer(client.Player);
            let refreshPkg = new Packet(ePacketCode.GetRoomInfo);
            refreshPkg.content = room.toRoomModel();
            room.AllPlayers.forEach(x => x.Client.Send(refreshPkg));
            client.Send(packet);
        }
    }
}