import { ePacketCode } from "../../../shared/common/enum/ePacketCode";
import 'reflect-metadata';
export function Handler(code: ePacketCode) {
    return function (target: Function) {
        Reflect.defineMetadata("HandlerCode", code, target);
    };
}
