import { IPacketHandler } from "../../interfaces/IPacketHandler";
import { Handler } from "./HandlerDecorator";
import { ePacketCode } from "../../../shared/common/enum/ePacketCode";
import { GameClient } from "../../client/GameClient";
import { Packet } from "../../../shared/common/Packet";
import { ePlayerStatus } from "../../../shared/common/enum/ePlayerStatus";
@Handler(ePacketCode.GameReady)
export default class GameReadyHandler implements IPacketHandler {
    HandlePacket(client: GameClient, pkg: Packet) {
        try {
            if (client.Player.Status === ePlayerStatus.InRoom) {
                let room = client.Player.CurrentRoom;
                if (room.BlackPlayer !== client.Player) {
                    throw new Error("Bạn không phải người chơi hoặc là chủ phòng.");
                }
                client.Player.Status = ePlayerStatus.Ready;
                room.AllPlayers.forEach(p => {
                    p.Client.Send(pkg);
                });

            } else {
                throw new Error("Người chơi không ở trong phòng");
            }
        } catch (e) {
            pkg.sussess = false;
            pkg.content = e.message;
            client.Send(pkg);
        }
    }
}