import 'jest';
import { HandlersLoader } from './HandlersLoader';
describe('Test HandlersLoader', () => {
    it('Loader all handler in handler folder', async () => {
        const handlers = await HandlersLoader.Load();
        expect(handlers.size).toBeGreaterThan(0);
        // tslint:disable-next-line:no-console
        console.log(handlers);
    });
});
