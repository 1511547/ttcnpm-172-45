import { IPacketHandler } from "../../interfaces/IPacketHandler";
import { Handler } from "./HandlerDecorator";
import { ePacketCode } from "../../../shared/common/enum/ePacketCode";
import { GameClient } from "../../client/GameClient";
import { Packet } from "../../../shared/common/Packet";
import { RoomManager } from "../../manager/RoomManager";
import { eRoomType } from "../../../shared/common/enum/eRoomType";
import { Room } from "../../../shared/gamemodel/Room";
import { GamePlayer } from "../../../shared/gamemodel/GamePlayer";
import { ePlayerStatus } from "../../../shared/common/enum/ePlayerStatus";
@Handler(ePacketCode.GetRoomInfo)
export default class GetRoomInfoHandler implements IPacketHandler {
    HandlePacket(client: GameClient, packet: Packet): void {
        let roomId: number = packet.content.roomId;
        let pkg = new Packet(packet.code, true);
        if (!roomId && roomId === 0) {
            if (client.Player.Status !== ePlayerStatus.Free) {
                pkg.content = client.Player.CurrentRoom.toRoomModel();
            } else {
                pkg.sussess = false;
                pkg.content = "Player is not in room";
            }
        } else {
            const room = RoomManager.Instance.getRoomById(roomId);
            if (!!room) {
                pkg.content = room.toRoomModel();
            } else {
                pkg.sussess = false;
                pkg.content = "Room not found";
            }
        }
        client.Send(pkg);
    }
}