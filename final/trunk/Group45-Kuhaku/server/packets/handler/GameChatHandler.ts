import { IPacketHandler } from "../../interfaces/IPacketHandler";
import { Handler } from "./HandlerDecorator";
import { ePacketCode } from "../../../shared/common/enum/ePacketCode";
import { GameClient } from "../../client/GameClient";
import { Packet } from "../../../shared/common/Packet";
import { GameMove } from '../../../shared/gamemodel/board/GameMove';
import { ePlayerStatus } from "../../../shared/common/enum/ePlayerStatus";
import { Player } from "../../../shared/gamemodel/board/Player";
@Handler(ePacketCode.GameChat)
export default class GameChatHandler implements IPacketHandler {
    HandlePacket(client: GameClient, packet: Packet) {
        if (client.Player.Status & ePlayerStatus.InRoom && client.Player.CurrentRoom) {
            client.Player.CurrentRoom.AllPlayers.forEach(p => p.Client.Send(packet));
        } else {
            packet.sussess = false;
            packet.content = "Chỉ chát được khi ở trong phòng.";
            client.Send(packet);
        }

    }
}