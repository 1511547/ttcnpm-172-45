import { IPacketHandler } from "../../interfaces/IPacketHandler";
import { Handler } from "./HandlerDecorator";
import { ePacketCode } from "../../../shared/common/enum/ePacketCode";
import { GameClient } from "../../client/GameClient";
import { Packet } from "../../../shared/common/Packet";
import { RoomManager } from "../../manager/RoomManager";
import { eRoomType } from "../../../shared/common/enum/eRoomType";
@Handler(ePacketCode.CreateRoom)
export default class CreateRoomhandler implements IPacketHandler {
    HandlePacket(client: GameClient, packet: Packet): void {
        let typ: eRoomType = packet.content.type;
        let name: string = packet.content.name;
        let bet: number = packet.content.bet;
        let room = RoomManager.Instance.CreateRoom(typ, name, bet, client.Player);
        let r = room.toRoomModel();
        r.redPlayer.isHost = true;
        let pkg = new Packet(ePacketCode.CreateRoom);
        pkg.content = r;
        client.Out.SendPacket(pkg);
    }
}