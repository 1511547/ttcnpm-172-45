import { Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn, OneToOne } from 'typeorm';
import { IUserDetail } from '../../shared/model/IUserDetail';
import { User } from './User';

@Entity("UserDetail")
export class UserDetail implements IUserDetail {
    @PrimaryGeneratedColumn()
    Id: number;

    @Column({
        type: 'varchar',
        length: 60,
        default: ""
    })
    FullName: string;

    @Column({
        type: 'varchar',
        length: 255,
        default: "/assets/avatar/unknown.png"
    })
    Avatar: string;

    @Column({
        type: 'varchar',
        length: 20,
        default: "USA"
    })
    Country: string;

    @Column({
        type: 'tinyint',
        length: 1,
        default: 0// 0==male,1==female
    })
    Gender: boolean;

    @Column({
        type: 'int',
        length: 6,
        default: 0
    })
    Rank: number;

    @Column({
        type: 'float',
        default: 60
    })
    WinRate: number;
    @Column({
        type: 'int',
        length: 8,
        default: 1000
    })
    Money: number;

    @Column({
        type: 'int',
        length: 4,
        default: 1
    })
    Level: number;

    @Column({
        type: 'int',
        default: 1
    })
    ExpCurrent: number;

    @Column({
        type: 'int',
        default: 100
    })
    ExpNextLevel: number;

    @Column({
        type: 'datetime',
        default: null
    })
    JoinDate: Date;

    @Column({
        type: 'datetime',
        default: null
    })
    LastLogin: Date;

    @Column({
        type: 'int',
        default: 0
    })
    Seen: number;

    @Column({
        type: 'varchar',
        length: 1000,
        default: ""
    })
    Introduce: string;

    // History

    @OneToOne(type => User)
    @JoinColumn()
    User: User;

}
