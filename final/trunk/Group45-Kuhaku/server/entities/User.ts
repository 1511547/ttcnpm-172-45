import { IUser } from '../../shared/model/IUser';
import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';
import { UserDetail } from './UserDetail';
@Entity("User")
export class User implements IUser {
    @PrimaryGeneratedColumn()
    Id: number;

    @Column({
        type: "varchar",
        length: 50,
        unique: true
    })
    Username: string;

    @Column({
        type: "varchar",
        length: 32
    })
    Password: string;

    @Column({
        type: "varchar",
        length: 255,
        unique: true
    })
    Email: string;

    @Column({
        type: 'tinyint',
        length: 1,
        default: 0,
    })
    IsAdmin: boolean;

    @Column({
        type: 'tinyint',
        length: 1,
        default: 1,
    })
    Status: boolean;

    @Column({
        type: "varchar",
        length: 255,
        default: ""
    })
    StatusMessage: string;

}
