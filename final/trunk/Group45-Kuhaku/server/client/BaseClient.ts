import { Socket } from "socket.io";
import * as Debug from 'debug';
import { Packet } from "../../shared/common/Packet";
let debug = Debug('GameClient');

export class BaseClient {

    protected socket: Socket;
    public get Socket(): Socket {
        return this.socket;
    }

    protected onDisconnect(): void {
        throw Error("Not implemented error.");
    }

    protected handlePacket(packet: Packet) {
        throw Error("Not implemented error.");
    }

    constructor(socket: Socket) {
        this.socket = socket;
        this.socket.on('disconnect', this.onDisconnect.bind(this));
        this.socket.on('packet', this.handlePacket.bind(this));
    }

}