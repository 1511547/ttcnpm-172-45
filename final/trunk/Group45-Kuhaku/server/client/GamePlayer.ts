import { GameClient } from "./GameClient";
import { Socket } from "socket.io";
import { UserDetail } from "../entities/UserDetail";
import { Room } from "../room/Room";
import { ePlayerStatus } from '../../shared/common/enum/ePlayerStatus';
import { Coordinate } from "../../shared/gamemodel/board/Coordinate";
import { GameMove } from "../../shared/gamemodel/board/GameMove";
import { UserBusiness } from "../business/UserBusiness";
import { GamePlayer as Player } from '../../shared/gamemodel/GamePlayer';
export class GamePlayer {

    public CurrentRoom: Room;

    private playerId: number;
    public get PlayerId() {
        return this.playerId;
    }

    private username: string;
    public get Username(): string {
        return this.username;
    }

    private client: GameClient;
    public get Client(): GameClient {
        return this.client;
    }

    private userDetail: UserDetail;
    public get UserDetail(): UserDetail {
        return this.userDetail;
    }
    private status: ePlayerStatus;
    public get Status(): ePlayerStatus {
        return this.status;
    }
    public set Status(status: ePlayerStatus) {
        this.status = status;
    }
    public AddExp(exp: number) {
        this.UserDetail.ExpCurrent += exp;
        let level = Math.round(Math.log2(this.UserDetail.ExpCurrent / 100)) + 1;
        this.UserDetail.Level = level;
        this.UserDetail.ExpNextLevel = 100 * Math.pow(2, level);
    }
    public AddMoney(money: number) {
        this.UserDetail.Money += money;
    }
    public Save() {
        let userBussiness = new UserBusiness();
        userBussiness.updateUsetDetail(this.username, this.userDetail);
    }

    constructor(playerId: number, username: string, socket: Socket, userDetail: UserDetail) {
        this.playerId = playerId;
        this.username = username;
        this.userDetail = userDetail;
        this.client = new GameClient(socket, this);
    }

    public ToSharedModel(): Player {
        let player = new Player(this.playerId, this.Username);
        player.setData(this.UserDetail);
        this.status = this.status;
        return player;
    }

}