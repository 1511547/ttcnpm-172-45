import { BaseClient } from "./BaseClient";
import { GamePlayer } from "./GamePlayer";
import { Socket } from "socket.io";
import { Packet } from "../../shared/common/Packet";
import { PacketManager } from "../manager/PacketManager";
import { IGameSocketOut } from "../interfaces/IGameSocketOut";
import { GameSocketOut } from "../packets/GameSocketOut";
import { LoginManager } from "../manager/LoginManager";
import * as Debug from 'debug';
const debug = Debug('GameClient');
debug.enabled = true;
export class GameClient extends BaseClient {

    private player: GamePlayer;
    private id: number;
    public get Id(): number {
        return this.id;
    }
    public get Player(): GamePlayer {
        return this.player;
    }

    private out: IGameSocketOut;
    public get Out(): IGameSocketOut {
        return this.out;
    }

    constructor(socket: Socket, player: GamePlayer) {
        super(socket);
        this.player = player;
        this.out = new GameSocketOut(this);
        this.socket = socket;
        this.id = new Date().getTime();
        const self = this;
    }
    public Send(pkg: Packet): void {
        this.socket.emit('packet', pkg);
    }

    public Disconnect() {
        this.socket.disconnect();
    }

    protected onDisconnect(): void {
        debug(`Disconnected ${this.player.PlayerId}`);
        if (this.player.CurrentRoom && this.player.CurrentRoom !== null) {
            this.player.CurrentRoom.RemovePlayer(this.player);
        }
        LoginManager.Instance.RemovePlayer(this.player.PlayerId);
    }

    protected handlePacket(packet: Packet) {
        PacketManager.Instance.HandlePacket(this, packet);
    }

}