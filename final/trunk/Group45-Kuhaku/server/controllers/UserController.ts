import { Router } from 'express';
import { UserBusiness } from '../business/UserBusiness';
import { Result } from '../../shared/common/Result';
import { Config } from '../config';
import { User } from '../entities/User';
import * as jwt from 'jsonwebtoken';
import { IUser } from '../../shared/model/IUser';
import { JsonResponse } from '../../shared/common/JsonResponse';
export let UserController = Router();

UserController.post('/login', async (req, res) => {
    let business = new UserBusiness();
    let status = await business.login(req.body.username, req.body.password);
    let result: JsonResponse<string | {}>;
    if (status === Result.Success) {
        let user = await business.getUserByUsername(req.body.username);
        if (user) {
            let payload: IUser = {
                Id: user.Id,
                Username: user.Username
            };
            let token = jwt.sign(payload, Config.JWT_SECRET, { expiresIn: "1d" });
            result = new JsonResponse(true, { token: token });
            res.cookie('token', token);
        }
    } else {
        result = new JsonResponse(false, status.message);
    }
    res.json(result);
    res.end();
});

UserController.post('/auth', async (req, res) => {
    if (req.body && req.body.token) {
        jwt.verify(req.body.token, Config.JWT_SECRET, (err: Error, decode) => {
            if (err) {
                res.json(new JsonResponse(false, err.message));
            } else {
                res.json(new JsonResponse(true, 'Token is valid.'));
            }
        });
    } else {
        res.json(new JsonResponse(false, "Post data is invalid."));
    }

});
UserController.post('/register', async (req, res) => {
    let business = new UserBusiness();
    let result = await business.addNewUser({
        Username: req.body.username,
        Email: req.body.email,
        Password: req.body.password
    });
    if (result === Result.Success) {
        result = await business.updateUsetDetail(req.body.username, {
            Gender: req.body.gender,
            FullName: req.body.fullname
        });
    }
    res.json(new JsonResponse(result === Result.Success, result.message));
    res.end();
});

UserController.post('/username', async (req, res) => {
    let business = new UserBusiness();

    let user = await business.getUserByUsername(req.body.username);
    if (user) {
        res.json(new JsonResponse(false, "Ten dang nhap da ton tai"));
        res.end();
    } else {
        res.json(new JsonResponse(true, "Ten dang nhap co the su dung"));
        res.end();
    }
});
UserController.get(':id', async (req, res) => {
    let userId = req.params.id;
    let business = new UserBusiness();
    let result = await business.getUserDetailById(userId);
    return res.end(JSON.stringify(result));
});
