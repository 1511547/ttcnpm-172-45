import { MysqlConnectionOptions } from "typeorm/driver/mysql/MysqlConnectionOptions";
import * as path from 'path';
export namespace Config {
    export const JWT_SECRET = process.env.JWT_SECRET || "kuhaku";
    export const MAX_ROOM = 2000;
    export const DBCONFIG: MysqlConnectionOptions = {
        "name": "kuhaku-connection",
        "type": "mysql",
        "host": process.env.DB_HOST || "localhost",
        "port": 3306,
        "username": "kuhaku",
        "password": "123456",
        "database": "kuhaku",
        "entities": [
            path.join(__dirname, "entities/*.js")
        ]
    };
}