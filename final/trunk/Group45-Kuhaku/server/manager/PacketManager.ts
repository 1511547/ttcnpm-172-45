import { Packet } from "../../shared/common/Packet";
import { IPacketHandler } from "../interfaces/IPacketHandler";
import { ePacketCode } from "../../shared/common/enum/ePacketCode";
import { GameClient } from "../client/GameClient";
import { HandlersLoader } from "../packets/handler/HandlersLoader";
import * as Debug from 'debug';
const debug = Debug('PacketManager');
debug.enabled = true;
export class PacketManager {

    private static instance: PacketManager;

    public static get Instance(): PacketManager {
        if (!PacketManager.instance) {
            PacketManager.instance = new PacketManager();
        }
        return PacketManager.instance;
    }

    private packetHandlers: Map<ePacketCode, IPacketHandler>;

    public async Init() {
        this.packetHandlers = await HandlersLoader.Load();
        debug(`Loaded ${this.packetHandlers.size} handlers`);
    }

    public HandlePacket(client: GameClient, packet: Packet) {
        debug(`Receive packet ${ePacketCode[packet.code]}`);
        let handler = this.packetHandlers.get(packet.code);
        debug(handler);
        if (handler) {
            handler.HandlePacket(client, packet);
        }
    }

    private registerHandler(code: ePacketCode, handler: IPacketHandler) {
        this.packetHandlers.set(code, handler);
    }

}