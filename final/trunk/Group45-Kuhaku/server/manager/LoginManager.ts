import { GamePlayer } from "../client/GamePlayer";
import { GameClient } from "../client/GameClient";
import { Packet } from "../../shared/common/Packet";
import { ePacketCode } from "../../shared/common/enum/ePacketCode";
import * as Debug from 'debug';
const debug = Debug('LoginManager');
debug.enabled = true;
export class LoginManager {

    private static instance: LoginManager;
    public static get Instance(): LoginManager {
        if (!LoginManager.instance) {
            LoginManager.instance = new LoginManager();
        }
        return LoginManager.instance;
    }

    private players: Map<number, GamePlayer>;

    constructor() {
        this.players = new Map<number, GamePlayer>();
    }

    public AddPlayer(id: number, player: GamePlayer) {
        let tempPlayer = this.GetPlayerById(id);
        if (tempPlayer) {
            tempPlayer.Client.Out.SendUserLogout("Tài khoản đăng nhập ở nơi khác");
            tempPlayer.Client.Disconnect();
        }
        debug(`Added player: ${player.PlayerId} - ${player.UserDetail.FullName}`);
        this.players.set(id, player);
    }

    public RemovePlayer(id: number) {
        if (this.players.has(id)) {
            this.players.delete(id);
            debug(`Remove player: ${id}`);
        }
    }

    public GetPlayerById(id: number): GamePlayer | null {
        if (this.players.has(id)) {
            return this.players.get(id);
        }
        return null;
    }
}