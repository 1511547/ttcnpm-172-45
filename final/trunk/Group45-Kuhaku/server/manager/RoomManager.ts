import { eRoomType } from "../../shared/common/enum/eRoomType";
import { TabPane } from "reactstrap";
import { Room } from "../room/Room";
import { GamePlayer } from "../client/GamePlayer";
import { Packet } from "../../shared/common/Packet";
import { ePacketCode } from "../../shared/common/enum/ePacketCode";
import { ePlayerStatus } from "../../shared/common/enum/ePlayerStatus";
import * as Debug from 'debug';
const debug = Debug('RoomManager');
debug.enabled = true;
export class RoomManager {

    private static instance: RoomManager;
    private nextId: number = 0;
    public static get Instance() {
        if (!RoomManager.instance) {
            RoomManager.instance = new RoomManager;
        }
        return RoomManager.instance;
    }

    private rooms: Map<number, Room>;

    constructor() {
        this.rooms = new Map();
    }

    public get Rooms(): Room[] {
        return Array.from(this.rooms.values());
    }
    public getRoomById(roomId: number) {
        if (this.rooms.has(roomId)) {
            return this.rooms.get(roomId);
        } else {
            return null;
        }
    }
    public CreateRoom(type: eRoomType, name: string, bet: number, host: GamePlayer): Room {
        let room: Room = null;
        if (type === eRoomType.PVP || type === eRoomType.PVE) {
            let roomId = ++this.nextId;
            room = new Room(roomId, name, type);
            room.Bet = bet;
            room.AddPlayer(host);
            this.rooms.set(roomId, room);
        }
        return room;
    }

    public RemoveRoom(roomId: number) {
        debug(`Remove room ${roomId}`);
        if (this.rooms.has(roomId)) {
            let room = this.rooms.get(roomId);
            let pkg = new Packet(ePacketCode.RemoveRoom);
            room.AllPlayers.forEach(player => {
                if (player && player !== room.Host) {
                    player.Status = ePlayerStatus.Free;
                    player.Client.Send(pkg);
                }
            });
            this.rooms.delete(roomId);
        }
    }
}