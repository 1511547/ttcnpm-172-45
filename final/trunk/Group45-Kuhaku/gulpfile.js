process.env.NODE_ENV = 'development';
const path = require('path');
const gulpWebpack = require('webpack-stream');
const webpack = require('webpack');
const ts = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const chalk = require('chalk');
const tsServerProject = ts.createProject('server/tsconfig.json');
const tsClientProject = ts.createProject('webclient/tsconfig.json');
const tsSharedProject = ts.createProject('shared/tsconfig.json');
const tslint = require('gulp-tslint');
const formatWebpackMessages = require('react-dev-utils/formatWebpackMessages');
const runSequence = require('run-sequence');
let spawn = require('child_process').spawn, node;
const jest = require('gulp-jest').default;
const gulp = require('gulp');
gulp.task('assets', () => {
  gulp.src(['./webclient/public/**/*.!(html)'])
    .pipe(gulp.dest('build/server/client'))
})
gulp.task('build-client-dev', ['assets'], (done) => {
  const configClientDev = require('./config/webpack.config.dev');
  configClientDev.output.path = path.join(__dirname, "build/server/client");

  webpack(configClientDev).run((err, stats) => {
    if (err) {
      done(err);
    }
    else {
      console.log("Compile client complete");
      done();
    }
  });
});

gulp.task('build-client', ['assets'], (done) => {
  process.env.NODE_ENV = "production";
  const configClient = require('./config/webpack.config.prod');
  configClient.output.path = path.join(__dirname, "build/server/client");
  webpack(configClient).run((err, stats) => {
    if (err) {
      done(err);
    }
    else {
      done();
    }
  });
});
gulp.task('build-shared', () => {
  return tsSharedProject.src()
    .pipe(sourcemaps.init())
    .pipe(tsSharedProject())
    .pipe(sourcemaps.write('.', {
      sourceRoot: '../shared',
      includeContent: false
    }))
    .pipe(gulp.dest('build'));
});

gulp.task('build-server', ['build-shared'], () => {
  return tsServerProject.src()
    .pipe(sourcemaps.init())
    .pipe(tsServerProject())
    .pipe(sourcemaps.write('.', {
      sourceRoot: '../server',
      includeContent: false
    }))
    .pipe(gulp.dest('build'));
});

gulp.task("watch-client", () => {
  gulp.run('build-client-dev');
  return gulp.watch(['webclient/**/*'], () => {
    runSequence('build-client-dev');
  });
})

gulp.task('server', ['build-server'], () => {
  if (node)
    node.kill();
  node = spawn('node', ["--inspect=0.0.0.0:9229", 'build/server/app.js'], { stdio: 'inherit' });
  node.on('close', (code) => {
    if (code == 8) {
      gulp.log('Error while running, waiting for changes');
    }
  });
});

gulp.task('watch-server', () => {
  gulp.run('server');
  gulp.watch(['server/**/*.ts'], () => {
    runSequence('build-server', 'server');
  }
  );
});

gulp.task("watch", ['watch-client', 'watch-server'], () => { });
gulp.task('default', ['build-client', 'build-server']);
gulp.task('tslint-client', () => {
  return tsClientProject.src()
    .pipe(tslint({
      formatter: "verbose"
    }))
    .pipe(tslint.report({
      emitError: true,
      summarizeFailureOutput: true
    }));
});
gulp.task('tslint-server', () => {
  return tsServerProject.src()
    .pipe(tslint({
      formatter: "verbose"
    }))
    .pipe(tslint.report({
      emitError: true,
      summarizeFailureOutput: true
    }));
});

gulp.task('tslint', () => {
  return gulp.src(['webclient/**/*.ts', 'webclient/**/*.tsx', 'server/**/*.ts', 'shared/**/*.ts'])
    .pipe(tslint({
      formatter: "verbose"
    }))
    .pipe(tslint.report({
      emitError: true,
      summarizeFailureOutput: true
    }));
});


gulp.task('test', () => {
  return gulp.src(['.'])
    .pipe(jest({ verbose: true }));
});

gulp.task('docker:build', (done) => {
  let docker = new spawn("docker", [
    "build",
    "-t", "registry.gitlab.com/thuanle/ttcnpm-172-45/node:kuhaku",
    "-f", "./docker/node.Dockerfile",
    "."],
    { stdio: 'inherit' })
  docker.on('close', (code) => {
    if (code == 8) {
      gulp.log('Error while build');
    }
    done();
  });
});

gulp.task('docker:push', (done) => {
  let docker = new spawn("docker", [
    "push",
    "registry.gitlab.com/thuanle/ttcnpm-172-45/node:kuhaku"
  ],
    { stdio: 'inherit' })
  docker.on('close', (code) => {
    if (code == 8) {
      gulp.log('Error while push');
    }
    done();
  });
});
