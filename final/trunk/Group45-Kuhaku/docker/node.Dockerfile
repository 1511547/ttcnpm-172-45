FROM node:8-alpine
LABEL maintainer="duckhan"
ENV DB_HOST mysql
COPY package.json /kuhaku/package.json
WORKDIR /kuhaku
RUN apk update &&\
    apk add git &&\
    npm install -g gulp-cli &&\
    cd /kuhaku &&\
    npm install
EXPOSE 3000
EXPOSE 9229
CMD ["ash"]