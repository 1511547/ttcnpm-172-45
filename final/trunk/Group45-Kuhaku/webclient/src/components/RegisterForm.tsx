import * as React from 'react';
// import './Register.css';
// import ReactDOM from 'react-dom';
import { MD5 } from "crypto-js";
import { JsonResponse } from '../../../shared/common/JsonResponse';
import { isEmail, isEmpty } from 'validator';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import * as fa from '@fortawesome/fontawesome-free-solid';
import { Alert, Modal, ModalBody, Button, Tooltip, Input, FormGroup } from 'reactstrap';

class RegisterFormProps {
    // onLoginSuccess?: (token: string) => void;
    // onClosed?: () => void;
}
class RegisterFormState {
    isTooltipOpen: boolean;
    registerStatus: string;
    passwordStrength: string;
    isRedAlertOpen: boolean;
    isGreenAlertOpen: boolean;
    isFullNameInvalid?: boolean;
    isEmailInvalid?: boolean;
    isUsernameInvalid?: boolean;
    isPasswordInvalid?: boolean;
    isConfirmPasswordInvalid?: boolean;
    isGenderInvalid?: boolean;
    uploadAvatar: string;
}

export class RegisterForm extends React.Component<RegisterFormProps, RegisterFormState> {
    fullname: string = "";
    email: string = "";
    username: string = "";
    password: string = "";
    confirmPassword: string = "";
    gender: boolean = false;
    estimatePassword = ""; // rat yeu, yeu, trung binh, manh
    emailValue: string = "";

    constructor(props: RegisterFormProps) {
        super(props);
        this.state = {
            registerStatus: "",
            isRedAlertOpen: false,
            isGreenAlertOpen: false,
            passwordStrength: "",
            isTooltipOpen: false,
            uploadAvatar: "/assets/avatar/unknown.png"
        };
    }

    onGenderChanged = async (e: React.ChangeEvent<HTMLSelectElement>) => {
        if (e.target && e.target.value) {
            if (e.target.value === "male" || e.target.value === "female") {
                this.gender = e.target.value === "male";
            }
        }
    }

    registerButtonClick = (e: React.MouseEvent<HTMLInputElement>) => {
        let validate: boolean = this.validate("fullname", this.fullname)
            && this.validate("email", this.email)
            && this.validate("username", this.username)
            && this.validate("password", this.password)
            && this.validate("confirmPassword", this.confirmPassword);
        if (validate) {
            this.reqRegister(this.fullname, this.username, this.password, this.gender, this.email);
        }
    }

    reqRegister(
        _fullname: string,
        _username: string,
        _password: string,
        _gender: boolean,
        _emailValue: string) {
        let md5Pass = MD5(_password).toString();
        let body = {
            fullname: _fullname,
            gender: _gender,
            username: _username,
            password: md5Pass,
            email: _emailValue
        };
        fetch("/user/register", {
            method: 'post',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(body)
        })
            .then((req) => {
                return req.json();
            })
            .then((data: JsonResponse<string>) => {
                if (data.success) {
                    this.setState({
                        isGreenAlertOpen: true,
                        registerStatus: "Đăng kí thành công"
                    });
                } else {
                    this.setState({
                        isRedAlertOpen: true,
                        registerStatus: "Đăng kí thất bại: " + data.data
                    });
                }
            }).catch((err: Error) => {
                this.setState({
                    registerStatus: err.message
                });
            });
    }

    isContainUsername(pass: string, use: string) {
        var index = pass.includes(use);
        if (index) {
            this.setState({
                registerStatus: "Mật khẩu chứa tên đăng nhập",
                isRedAlertOpen: true
            });

        } else {
            this.setState({
                registerStatus: ""
            });
        }
    }

    checkComplexPassword(pass: string) {
        let passStrength = "";
        let isCapital = 0;
        let isNumber = 0;
        let isSpecialCharacter = 0;
        let isLowerCase = 0;
        let sum = 0;
        let i = 0;
        while (i < pass.length) {
            var ch = pass.charAt(i);
            if (ch >= '0' && ch <= '9') {
                isNumber = 1;
            } else if (ch >= 'a' && ch <= 'z') {
                isLowerCase = 1;
            } else if (ch >= 'A' && ch <= 'Z') {
                isCapital = 1;
            } else {
                isSpecialCharacter = 1;
            }
            i++;
        }
        sum = isCapital + isLowerCase + isNumber + isSpecialCharacter;
        switch (sum) {
            case 1:
                passStrength = "Rất yếu";
                break;
            case 2:
                passStrength = "Yếu";
                break;
            case 3:
                passStrength = "Trung Bình";
                break;
            case 4:
                passStrength = "Mạnh";
                break;
            default: {
                passStrength = "Mật khẩu không hợp lệ";
                break;
            }
        }
        this.setState({
            passwordStrength: passStrength,
            isTooltipOpen: true,
            isPasswordInvalid: false
        });
    }
    validate = (field: string, val: string) => {
        this.setState({
            isTooltipOpen: false,
            isGreenAlertOpen: false
        });
        switch (field) {
            case "username":
                if (val.length >= 3) {
                    this.username = val;
                    this.setState({
                        isUsernameInvalid: false
                    });
                    this.validate("fullname", this.fullname);
                    return true;
                } else {
                    this.username = "";
                    this.setState({
                        registerStatus: "Tên đăng nhập phải có ít nhất 3 kí tự",
                        isRedAlertOpen: true,
                        isUsernameInvalid: true
                    });
                }
                break;
            case "fullname":
                if (!isEmpty(val)) {
                    this.fullname = val;
                    this.setState({
                        isFullNameInvalid: false
                    });
                    this.validate("email", this.email);
                    return true;
                } else {
                    this.fullname = "";
                    this.setState({
                        registerStatus: "Tên không được trống",
                        isRedAlertOpen: true,
                        isFullNameInvalid: true
                    });
                }
                break;
            case "gender":
                this.gender = val === "male";
                this.setState({
                });
                return true;
            case "email":
                if (isEmail(val)) {
                    this.email = val;
                    this.setState({ isEmailInvalid: false });
                    this.validate("password", this.password);
                    return true;
                } else {
                    this.setState({
                        registerStatus: "Email không hợp lệ",
                        isRedAlertOpen: true,
                        isEmailInvalid: true
                    });
                    this.email = "";
                }
                break;
            case "password":
                if (val.length > 0 && val.length < 6) {
                    this.setState({
                        registerStatus: "Mật khẩu phải có ít nhất 6 kí tự.",
                        isRedAlertOpen: true,
                        isPasswordInvalid: true
                    });
                    this.password = "";
                } else {
                    if (this.username.length > 0 && val.includes(this.username)) {
                        this.setState({
                            registerStatus: "Mật khẩu không được chứa tên đăng nhập",
                            isRedAlertOpen: true,
                            isPasswordInvalid: true
                        });
                    } else {
                        this.password = val;
                        this.checkComplexPassword(this.password);
                        // this.validate("confirmPassword", this.confirmPassword);
                        return true;
                    }
                }
                break;
            case "confirmPassword":
                this.confirmPassword = val;
                if (val.length > 0 && this.confirmPassword !== this.password) {
                    this.setState({
                        registerStatus: "Không khớp với mật khẩu",
                        isRedAlertOpen: true,
                        isConfirmPasswordInvalid: true
                    });
                } else {
                    this.setState({
                        isRedAlertOpen: false,
                        isConfirmPasswordInvalid: false
                    });
                    return true;
                }
                break;
            default:
                break;
        }
        return false;
    }

    onloadImage = (event) => {
        this.setState({ uploadAvatar: event.target.result});
    }

    onChangeAvatar = (e: React.ChangeEvent<HTMLInputElement>) => {
        var fReader = new FileReader();
        fReader.addEventListener('load', this.onloadImage);
        fReader.readAsDataURL(e.currentTarget.files[0]);
    }

    render() {
        let redAlert = this.state.isRedAlertOpen && (
            <div className="input-group p-1">
                <Alert color="danger" className="w-100">
                    {this.state.registerStatus}
                </Alert>
            </div>
        );
        let greenAlert = this.state.isGreenAlertOpen && (
            <div className="input-group p-1">
                <Alert color="success" className="w-100">
                    {this.state.registerStatus}
                </Alert>
            </div>
        );
        return (
            <FormGroup>
                {redAlert}
                {greenAlert}
                <div className="input-group p-2">
                    <div style={{ width: "100%" }}>
                        <div
                            className="avatar-upload text-center centered"
                            style={{ backgroundImage: `url("${this.state.uploadAvatar}")` }}
                        >
                            <a
                                className="btn-upload-avatar"
                                onClick={() => { document.getElementById('avatarUpload').click(); }}
                                href="#"
                            >
                                <FontAwesomeIcon icon="camera" size="2x" />
                            </a>
                            <Input
                                onChange={this.onChangeAvatar}
                                type="file"
                                style={{ display: "none" }}
                                id="avatarUpload"
                            />
                        </div>
                        <div className="text-stroke text-center text-white">{this.username}</div>
                    </div>
                </div>
                <div className="input-group p-2">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon={fa.faUser} />
                        </div>
                    </div>
                    <Input
                        type="text"
                        invalid={this.state.isUsernameInvalid}
                        className="form-control"
                        placeholder="Tên đăng nhập"
                        id="reg_username"
                        onChange={(e) => this.validate("username", e.target.value)}
                    />
                </div>
                <div className="input-group p-2">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon={fa.faUser} />
                        </div>
                    </div>
                    <Input
                        type="text"
                        invalid={this.state.isFullNameInvalid}
                        className="form-control"
                        placeholder="Họ tên"
                        id="reg_fullname"
                        onChange={(e) => this.validate("fullname", e.target.value)}
                    />
                </div>
                <div className="input-group p-2">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon={fa.faTransgender} />
                        </div>
                    </div>
                    <select defaultValue="male" className="custom-select" onChange={this.onGenderChanged}>
                        <option value="male">Nam</option>
                        <option value="female">Nữ</option>
                    </select>
                </div>
                <div className="input-group p-2">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon={fa.faEnvelope} />
                        </div>
                    </div>
                    <Input
                        type="text"
                        invalid={this.state.isEmailInvalid}
                        className="form-control"
                        placeholder="Email"
                        id="reg_email"
                        onChange={(e) => this.validate("email", e.target.value)}
                    />
                </div>
                <div className="input-group p-2">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon={fa.faLock} />
                        </div>
                    </div>
                    <Input
                        type="password"
                        invalid={this.state.isPasswordInvalid}
                        className="form-control"
                        placeholder="Mật khẩu"
                        id="reg_password"
                        onChange={(e) => this.validate("password", e.target.value)}
                    />
                    <Tooltip
                        placement="top"
                        target="reg_password"
                        isOpen={this.state.isTooltipOpen}
                    >
                        {this.state.passwordStrength}
                    </Tooltip>
                </div>
                <div className="input-group p-2">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon={fa.faLock} />
                        </div>
                    </div>
                    <Input
                        type="password"
                        invalid={this.state.isConfirmPasswordInvalid}
                        className="form-control"
                        placeholder="Nhập lại mật khẩu"
                        id="reg_confirmPassword"
                        onChange={(e) => this.validate("confirmPassword", e.target.value)}
                    />
                </div>
                <div className="input-group p-2">
                    <Button
                        color="primary"
                        block={true}
                        onClick={this.registerButtonClick}
                    >
                        Đăng ký
                    </Button>
                </div>
            </FormGroup>
        );
    }
}

export default RegisterForm;