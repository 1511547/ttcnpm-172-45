import * as React from "react";
import * as FontAwesome from 'react-fontawesome';
import { Authentication } from "../../common/Authentication";
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import * as fa from '@fortawesome/fontawesome-free-solid';
import { GamePlayer } from '../../../../shared/gamemodel/GamePlayer' ;
import { PlayerInfoDialog } from "./PlayerInfoDialog";
import { ePlayerStatus } from "../../../../shared/common/enum/ePlayerStatus";

class PlayerCardState {
}

class PlayerCardProps {
    player?: GamePlayer;
    color?: string;
    onClick?: (player: GamePlayer) => void;
}

export class PlayerCard extends React.Component<PlayerCardProps, PlayerCardState> {
    constructor(props: PlayerCardProps) {
        super(props);
    }

    onLogout = (e: React.MouseEvent<HTMLAnchorElement>) => {
        new Authentication().logout(() => {
            window.location.href = '/';
        });
    }

    onPlayerCardClick = () => {
        if (!!this.props.onClick) {
            if (!!this.props.player) {
                this.props.onClick(this.props.player);
            }
        }

    }

    render() {

        const player: GamePlayer = this.props.player;
        let avatar = {
            backgroundImage: `url('${!!player ? player.avatar : '/assets/avatar/unknown.png'}')`
        };
        let player1piece = `${this.props.color}-player-name centerd text-stroke`;
        let coin = !!player && player.coin.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        let title = !!player ? (player.isHost ? "Chủ phòng" : "") : "Đang chờ";
        let username = !!player ? player.username : "";
        let level = !!player ? player.level : "";
        let className = "player-card ";
        if (!player) {
            className += "player-wating";
        } else if (!player.isHost && (player.status & ePlayerStatus.Ready) === 0) {
            className += "player-not-ready";
        }
        return (
            <div className={className} onClick={this.onPlayerCardClick}>
                <div className="player-card-menu text-stroke">
                    <span className="player-level">{level}</span>
                    <span>{title}</span>
                    {!!player && !player.isHost &&
                        <a href="#" className="kick-btn"><FontAwesomeIcon icon="times-circle" size="1x" /></a>
                    }
                </div>
                <div className="avatar x90 centered" style={avatar} />
                <div className={player1piece}>{username}</div>
                <div className="player-coin">{coin}</div>
            </div>
        );
    }
}