import * as React from 'react';
import { Row, Col, Progress } from "reactstrap";
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import * as fa from '@fortawesome/fontawesome-free-solid';
import { Alert, Modal, ModalBody, ModalFooter, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import { GamePlayer } from '../../../../shared/gamemodel/GamePlayer';

class ProfileDialogProps {
    player: GamePlayer;
    onClosed?: () => void;
}

class ProfileDialogState {
}

export class ProfileDialog extends React.Component<ProfileDialogProps, ProfileDialogState> {
    constructor(props: ProfileDialogProps) {
        super(props);
    }

    closeClick = () => {
        if (this.props.onClosed) {
            this.props.onClosed();
        }
    }

    render() {
        let player = this.props.player;
        let username = player.username;
        let avatar = `url("${player.avatar}")`;
        return (
            <Modal isOpen={true}>
                <div className="modal-header">
                    <h5 className="modal-title">Thông tin người chơi</h5>
                    <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        aria-label="Close"
                        onClick={this.closeClick}
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <ModalBody>
                    <div className="profile-info">
                        <div className="profile-left-info">
                            <div className="avatar x90 centered" style={{ backgroundImage: avatar }} />
                            <div className="text-center text-white text-stroke">{username}</div>
                        </div>
                        <div className="profile-right-info">
                            <div className="centered">
                                <span>{player.fullname}</span>
                            </div>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button onClick={this.closeClick} color="danger">OK</Button>
                </ModalFooter>
            </Modal >
        );
    }
}

export default ProfileDialog;
