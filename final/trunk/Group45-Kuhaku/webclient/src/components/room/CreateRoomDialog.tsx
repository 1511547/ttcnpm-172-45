import * as React from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import * as fa from '@fortawesome/fontawesome-free-solid';
import { Alert, Modal, ModalBody } from 'reactstrap';
class CreateRoomDialogProps {
  onSubmited?: (roomName: string, roomType: string, bet: number) => void;
  onClosed?: () => void;
}
class CreateRoomDialogState {
  hasError: boolean;
  errorMessage: string;
  isOpen: boolean;
}
export class CreateRoomDialog extends React.Component<CreateRoomDialogProps, CreateRoomDialogState> {
  roomName: HTMLInputElement | null;
  roomType: string | null;
  bet: HTMLInputElement | null;
  constructor(props: CreateRoomDialogProps) {
    super(props);
    this.state = {
      hasError: false,
      errorMessage: "",
      isOpen: true
    };
    this.roomType = "PVP";
  }
  closeClick = () => {
    this.setState({
      isOpen: false
    });
    if (this.props.onClosed) {
      this.props.onClosed();
    }
  }
  onSubmited = (roomName: string, roomType: string, bet: number): void => {
    if (this.props.onSubmited) {
      this.props.onSubmited(roomName, roomType, bet);
    }
  }
  submitBtnClick = async (e: React.MouseEvent<HTMLButtonElement>) => {
    // tslint:disable-next-line:no-console
    console.log(this.roomType);
    if (!this.bet || !this.bet.value || isNaN(Number(this.bet.value))) {
      this.setState({
        hasError: true,
        errorMessage: "Vui lòng nhập số tiền cược"
      });
    } else if (!this.roomType) {
      this.setState({
        hasError: true,
        errorMessage: "Vui lòng chọn loại phòng"
      });
    } else {
      if (this.roomName && this.roomName.value) {
        this.onSubmited(this.roomName.value, this.roomType, Number(this.bet.value));
      }
      this.closeClick();
    }
  }

  onRoomTypeChanged = async (e: React.ChangeEvent<HTMLSelectElement>) => {
    if (e.target && e.target.value) {
      if (e.target.value === "PVP" || e.target.value === "PVE") {
        this.roomType = e.target.value;
      }
    }
  }

  render() {
    let err = this.state.hasError && (
      <div className="input-group p-1">
        <Alert color="danger w-100">
          {this.state.errorMessage}
        </Alert>
      </div>
    );
    return (
      <Modal isOpen={this.state.isOpen}>
        <div className="modal-header">
          <h5 className="modal-title">Tạo phòng</h5>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.closeClick}>
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <ModalBody>
          <form>
            {err}
            <div className="input-group p-1">
              <div className="input-group-prepend">
                <div className="input-group-text">
                  Tên phòng
                </div>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder="nhập tên phòng"
                defaultValue="phòng game"
                id="roomName"
                ref={(input) => this.roomName = input}
              />
            </div>
            <div className="input-group p-1">
              <div className="input-group-prepend">
                <div className="input-group-text">
                  Loại phòng
                </div>
              </div>
              <select onChange={this.onRoomTypeChanged} defaultValue="PVP" className="custom-select" id="roomType">
                <option value="PVP">PVP</option>
                <option value="PVE">PVE</option>
              </select>
            </div>
            <div className="input-group p-1">
              <div className="input-group-prepend">
                <div className="input-group-text">
                  Tiền cược
                </div>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder="đặt tiền cược"
                defaultValue="1000"
                id="bet"
                ref={(input) => this.bet = input}
              />
            </div>
            <div className="text-center">
              <button className="btn btn-primary" type="button" onClick={this.submitBtnClick}>OK</button>
            </div>
          </form>
        </ModalBody>
      </Modal >
    );
  }
}

export default CreateRoomDialog;
