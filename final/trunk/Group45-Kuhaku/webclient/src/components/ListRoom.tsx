import * as React from 'react';
import { Room } from '../../../shared/gamemodel/Room';
import { Scrollbars } from 'react-custom-scrollbars';
import { RoomItem } from './RoomItem';
import { Button } from 'reactstrap';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import * as FontAwesome from 'react-fontawesome';
import { ProfileCard } from './ProfileCard';
import { GameClient } from '../client/GameClient';
import { GamePlayer as Player } from '../../../shared/gamemodel/GamePlayer';

class ListRoomProps {
    onCreateRoom?: () => void;
    onJoinRoom?: (room: Room) => void;
    onRefreshList?: () => void;
    client: GameClient;
    rooms?: Room[];
}
class ListRoomState {
    rooms: Room[];
    isShowCreateRoom: boolean;
}

export class ListRoom extends React.Component<ListRoomProps, ListRoomState> {
    constructor(props: ListRoomProps) {
        super(props);
        this.state = {
            rooms: [],
            isShowCreateRoom: false
        };
    }
    onRefreshButtonClick = () => {
        if (!!this.props.onRefreshList) {
            this.props.onRefreshList();
        }
    }
    onCreateClick = () => {
        if (!!this.props.onCreateRoom) {
            this.props.onCreateRoom();
        }
    }
    onRoomClick = (room: Room) => {
        // tslint:disable-next-line:no-console
        console.log(room);
        if (!!this.props.onJoinRoom) {
            this.props.onJoinRoom(room);
        }
    }
    render() {
        const rooms = this.props.rooms.map((room, index) => {
            return <RoomItem room={room} key={index} onClick={this.onRoomClick} />;
        });

        let player = this.props.client.ProfileData.GamePlayer;
        return (
            <div className="container-fluid">
                <div className="row head-row">
                    <ProfileCard player={player} />
                </div>
                <div className="row body-row">
                    <div className="col-lg-10 col-centered">
                        <div className="room-list">
                            <div className="room-list-header" />
                            <div className="room-list-container">
                                <Scrollbars
                                    autoHide={true}
                                    autoHideTimeout={1000}
                                    autoHideDuration={200}
                                >
                                    <div className="row">
                                        {rooms}
                                    </div>
                                </Scrollbars>
                            </div>
                        </div>
                        <div className="room-list-menu">
                            <Button
                                color="danger"
                                className="room-list-menu-item"
                                onClick={this.onRefreshButtonClick}
                            >
                                <FontAwesomeIcon icon="sync" size="1x" />
                                &nbsp;Cập nhật
                            </Button>
                            <Button
                                color="danger"
                                className="room-list-menu-item"
                                onClick={this.onCreateClick}
                            >
                                <FontAwesomeIcon icon="plus-circle" size="1x" />
                                &nbsp;Tạo phòng
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
