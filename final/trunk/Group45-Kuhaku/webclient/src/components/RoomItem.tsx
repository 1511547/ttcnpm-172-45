import * as React from "react";
import './RoomItem.css';
import { Room } from '../../../shared/gamemodel/Room';

class RoomItemProps {
    room: Room;
    onClick?: (room: Room) => void;
}
export class RoomItem extends React.Component<RoomItemProps, {}> {
    onRoomClick = () => {
        if (!!this.props.onClick) {
            this.props.onClick(this.props.room);
        }
    }
    render() {
        const room = this.props.room;
        let player1piece = { backgroundImage: "url('/assets/black.png')" };
        let player2piece = { backgroundImage: "url('/assets/black.png')" };
        let player1avatar = { backgroundImage: `url('${room.redPlayer.avatar}')` };
        let player2avatar = room.blackPlayer ?
            { backgroundImage: `url('${room.blackPlayer.avatar}')` } :
            { backgroundImage: "url('/assets/unknown.png')" };
        return (
            <div className="col-sm-12 col-md-12 col-lg-6" onClick={this.onRoomClick}>
                <div className="room-item">
                    <div className="room-id text-stroke">
                        {room.id}
                    </div>
                    <div className="room-basic-info">
                        <div className="room-name text-stroke">
                            {room.name}
                        </div>
                        <div className="room-bet text-stroke">{room.bet}</div>
                    </div>
                    <div className="room-match-info">
                        <div className="player1">
                            <div className="avatar centered" style={player1avatar} />
                            <div className="red-player-name">{room.redPlayer.username}</div>
                        </div>
                        <div className="versus-img" />
                        <div className="player2">
                            <div className="avatar centered" style={player2avatar} />
                            <div className="black-player-name">
                                {room.blackPlayer ? room.blackPlayer.username : "Đang chờ"}
                            </div>
                        </div>
                    </div>
                </div>
            </div>);

    }
}