import * as React from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import * as fa from '@fortawesome/fontawesome-free-solid';
import { Modal, ModalBody, ModalFooter, Button } from 'reactstrap';
class ConfirmDialogProps {
  confirmHeader: string;
  confirmText: string;
  onClosed?: (answer: boolean) => void;
}

class ConfirmDialogState {
  isOpen: boolean;
}
export class ConfirmDialog extends React.Component<ConfirmDialogProps, ConfirmDialogState> {
  constructor(props: ConfirmDialogProps) {
    super(props);
  }

  componentDidMount() {
    this.setState({isOpen: true});
  }

  btnOKClick = () => {
    this.setState({
      isOpen: false
    });
    if (this.props.onClosed) {
      this.props.onClosed(true);
    }
  }

  btnCancelClick = () => {
    this.setState({
      isOpen: false
    });
    if (this.props.onClosed) {
      this.props.onClosed(false);
    }
  }

  render() {
    return (
      <Modal isOpen={true}>
        <div className="modal-header">
          <h5 className="modal-title">{this.props.confirmHeader}</h5>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.btnCancelClick}>
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <ModalBody>
          {this.props.confirmText}
        </ModalBody>
        <ModalFooter>
          <Button onClick={this.btnCancelClick}>Hủy</Button>
          <Button onClick={this.btnOKClick} color="danger">OK</Button>
        </ModalFooter>
      </Modal >
    );
  }
}

export default ConfirmDialog;
