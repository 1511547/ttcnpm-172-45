import * as React from 'react';
class DialogProps {
    content: React.Component;
}
export class Dialog extends React.Component<DialogProps, {}> {
    render() {
        return this.props.content;
    }

}