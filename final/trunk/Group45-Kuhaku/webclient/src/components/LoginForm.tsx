import * as React from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import * as fa from '@fortawesome/fontawesome-free-solid';
import { Alert, Modal, ModalBody, Button } from 'reactstrap';
import { MD5 } from "crypto-js";
import { Authentication } from '../common/Authentication';
import { Link } from 'react-router-dom';

class LoginFormProps {
    onLoginSuccess?: (token: string) => void;
    onClosed?: () => void;
}
class LoginFormState {
    hasError: boolean;
    errorMessage: string;
    isOpen: boolean;
    activeTab: string;
}
export class LoginForm extends React.Component<LoginFormProps, LoginFormState> {
    username: HTMLInputElement | null;
    password: HTMLInputElement | null;
    constructor(props: LoginFormProps) {
        super(props);
        this.state = {
            hasError: false,
            errorMessage: "",
            isOpen: true,
            activeTab: '1'
        };
        this.toggle = this.toggle.bind(this);
    }

    toggle(tab: string) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    closeClick = () => {
        this.setState({
            isOpen: false
        });
        if (this.props.onClosed) {
            this.props.onClosed();
        }
    }
    onLoginSuccess = (token: string): void => {
        if (this.props.onLoginSuccess) {
            this.props.onLoginSuccess(token);
        }
    }

    loginButtonClick = async (e: React.MouseEvent<HTMLButtonElement>) => {
        let self = this;
        if ((!this.username || !this.username.value) || (!this.password || !this.password.value)) {
            this.setState({
                hasError: true,
                errorMessage: "Tên và mật khẩu không được trống."
            });
        } else {
            let md5Pass = MD5(this.password.value).toString();
            let token = await new Authentication().authenicate(this.username.value, md5Pass);
            if (token !== null) {
                this.onLoginSuccess(token);
            }
        }
    }
    render() {
        let err = this.state.hasError && (
            <div className="input-group p-1">
                <Alert color="danger w-100">
                    {this.state.errorMessage}
                </Alert>
            </div>
        );
        return (
            <form>
                {err}
                <div className="input-group p-2">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon={fa.faUser} />
                        </div>
                    </div>
                    <input
                        type="text"
                        className="form-control"
                        placeholder="Tên đăng nhập"
                        id="username"
                        ref={(input) => this.username = input}
                    />
                </div>
                <div className="input-group p-2">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon={fa.faLock} />
                        </div>
                    </div>
                    <input
                        type="password"
                        className="form-control"
                        placeholder="Mật khẩu"
                        id="password"
                        ref={(input) => this.password = input}
                    />
                </div>
                <div className="input-group p-2">
                    <Button
                        color="primary"
                        onClick={this.loginButtonClick}
                        block={true}
                    >
                        Đăng nhập
                    </Button>
                </div>
                <div className="input-group p-2">
                    <Link className="text-secondary" to="/register">Quên mật khẩu?</Link>
                </div>
            </form>
        );
    }
}

export default LoginForm;
