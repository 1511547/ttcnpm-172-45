import { Row, Col, Progress } from "reactstrap";
import * as React from "react";
import * as FontAwesome from 'react-fontawesome';
import { Authentication } from "../common/Authentication";
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import * as fa from '@fortawesome/fontawesome-free-solid';
import { GamePlayer as Player } from "../../../shared/gamemodel/GamePlayer";

class ProfileCardState {
}

class ProfileCardProps {
    player: Player;
}

export class ProfileCard extends React.Component<ProfileCardProps, ProfileCardState> {
    constructor(props: ProfileCardProps) {
        super(props);
    }

    numberWithDots(num: number) {
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    onLogout = (e: React.MouseEvent<HTMLAnchorElement>) => {
        new Authentication().logout(() => {
            window.location.href = '/';
        });
    }

    render() {
        let avatar = { backgroundImage: `url("${this.props.player.avatar}")` };
        return (
            <div className="profile-card">
                <div className="avatar profile-card-img" style={avatar} />
                <div className="profile-card-info">
                    <div className="centered">
                        <span>{this.props.player.username}</span>
                        <a className="btn" style={{ padding: "0" }} onClick={this.onLogout} href="#">
                            <span><FontAwesomeIcon icon="sign-out-alt" /></span>
                        </a>
                    </div>
                    <div className="centered">
                        <img src="/assets/iconGold.png" style={{ maxHeight: "20px", maxWidth: "20px" }} />
                        <span style={{ marginLeft: "10px" }}>{this.numberWithDots(this.props.player.coin)}</span>
                    </div>
                    <div className="centered">
                        <div style={{ float: "left" }}>lv.{this.props.player.level}</div>
                        <Progress
                            color="success"
                            value={this.props.player.expPercent}
                            max="100"
                            style={{ marginTop: "3px" }}
                        >
                            {this.props.player.expPercent}%
                        </Progress>
                    </div>
                </div>
            </div>
        );
    }
}