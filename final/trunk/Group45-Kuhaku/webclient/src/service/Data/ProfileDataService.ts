import { BaseService } from "../BaseService";
import { PkgEvent } from "../../event/PkgEvent";
import { ePacketCode } from "../../../../shared/common/enum/ePacketCode";
import { IUserDetail } from '../../../../shared/model/IUserDetail';
import { Packet } from "../../../../shared/common/Packet";
import { GameClient } from "../../client/GameClient";
import { GamePlayer } from "../../../../shared/gamemodel/GamePlayer";
import { Player } from "../../../../shared/gamemodel/board/Player";
export class ProfileDataService extends BaseService {
    private info: IUserDetail;
    public get Info() {
        return this.info;
    }
    public get GamePlayer(): GamePlayer {
        let player = new GamePlayer(0, "Unknown");
        if (!!this.info) {
            player = new GamePlayer(this.info.User.Id, this.info.User.Username);
            player.setData(this.info);
        }
        return player;
    }
    constructor(client: GameClient) {
        super(client);
        this.init();
        this.info = null;
    }
    public init(): void {
        this.client.PacketListener.on(ePacketCode.GetProfileData, this.onReceiveProfileData, this);
    }

    public getProfileData() {
        let pkg = new Packet(ePacketCode.GetProfileData);
        this.client.Send(pkg);
    }
    private onReceiveProfileData(e: PkgEvent) {
        if (e.Packet.sussess) {
            this.info = e.Packet.content;
            // tslint:disable-next-line:no-console
            console.log(this.info);
        }
    }

    public dispose(): void {
        this.client.PacketListener.remove(ePacketCode.GetProfileData, this);
    }
}