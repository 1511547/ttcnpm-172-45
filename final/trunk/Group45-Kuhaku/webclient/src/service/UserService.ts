import { BaseService } from "./BaseService";
import { GameClient } from "../client/GameClient";
import { ePacketCode } from "../../../shared/common/enum/ePacketCode";
import { PkgEvent } from "../event/PkgEvent";
import { Packet } from "../../../shared/common/Packet";

export class UserService extends BaseService {

    public constructor(client: GameClient) {
        super(client);
    }

    public logout() {
        let pkg = new Packet(ePacketCode.Logout);
        this.client.Send(pkg);
    }
    public init(): void {
        this.client.PacketListener.on(ePacketCode.Logout, this.onLogout, this);
        // this.client.PacketListener.on(ePacketCode.GetProfileData, this.onReceiveProfileData, this);
    }
    private onLogout(e: PkgEvent): void {
        alert("logout success");
    }
    public dispose(): void {
        this.client.PacketListener.remove(ePacketCode.Logout, this.onLogout);
    }
}
