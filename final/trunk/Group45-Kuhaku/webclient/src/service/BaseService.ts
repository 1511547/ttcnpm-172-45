import { EventDispatcher } from "../event/EventDispatcher";
import { GameClient } from "../client/GameClient";
import { EventType } from "../event/EventType";
import { EventObject } from "../event/EventObject";

export abstract class BaseService {
    protected client: GameClient;
    constructor(client: GameClient) {
        this.client = client;
    }
    public abstract init(): void;
    public abstract dispose(): void;
    protected error(message: String) {
        this.client.Event.emit(EventType.Error, new EventObject(message));
    }
}