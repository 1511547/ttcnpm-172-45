import { Room } from "../../../shared/gamemodel/Room";
import { PkgEvent } from "../event/PkgEvent";
import { EventDispatcher } from "../event/EventDispatcher";
import { GameClient } from "../client/GameClient";
import { ePacketCode } from "../../../shared/common/enum/ePacketCode";
import { BaseService } from "./BaseService";
import { Packet } from "../../../shared/common/Packet";
import { EventType } from "../event/EventType";
import { EventObject } from "../event/EventObject";
import { ChessGame } from "../../../shared/gamemodel/board/ChessGame";
import { Player } from "../../../shared/gamemodel/board/Player";
import { eRoomType } from "../../../shared/common/enum/eRoomType";
import { GamePlayer } from "../../../shared/gamemodel/GamePlayer";
import { Coordinate } from "../../../shared/gamemodel/board/Coordinate";
import { AlphaBetaMiniMax } from '../../../shared/gamemodel/AI/SearchEngines/AlphaBetaMiniMax';
import { SearchEngine } from '../../../shared/gamemodel/AI/SearchEngine';
import { Piece } from "../../../shared/gamemodel/board/pieces";
import { GameMove } from '../../../shared/gamemodel/board/GameMove';
import { ePlayerStatus } from "../../../shared/common/enum/ePlayerStatus";
import { setTimeout } from "timers";
export class RoomService extends BaseService {

    public CurrentRoom: Room;
    public CurrentGame: ChessGame;
    private searchEngine: SearchEngine;
    public get IsHost(): boolean {
        if (this.CurrentRoom.redPlayer.userId === this.client.ProfileData.GamePlayer.userId) {
            return true;
        } else {
            return false;
        }
    }
    constructor(gameClient: GameClient) {
        super(gameClient);
        this.CurrentGame = new ChessGame();
    }

    private SetupGameInfo() {
        // tslint:disable-next-line:no-console
        console.log(this.CurrentRoom);
        if (this.CurrentRoom.redPlayer.userId === this.client.ProfileData.Info.User.Id) {
            this.CurrentGame.player = Player.Red;
        } else if (!!this.CurrentRoom.blackPlayer
            && this.CurrentRoom.blackPlayer.userId === this.client.ProfileData.Info.User.Id) {
            this.CurrentGame.player = Player.Black;
        }
        if (this.CurrentRoom.type === eRoomType.PVE) {
            let ai = new GamePlayer(0, "Kuhaku");
            ai.avatar = "/assets/avatar/Shiro.jpg";
            this.CurrentRoom.blackPlayer = ai;
            this.searchEngine = new AlphaBetaMiniMax();
        }
        this.CurrentGame.onMoveCallback = this.onMove.bind(this);
    }
    private onMove(from: Coordinate, to: Coordinate, isMove: boolean) {
        // tslint:disable-next-line:no-console
        console.log("move");
        if (this.CurrentRoom.type === eRoomType.PVE && this.CurrentGame.CurrentTurn === Player.Black) {
            if (!!this.searchEngine) {
                setTimeout(this.ai_move.bind(this), 1000);
            }
        } else {
            let pkg = new Packet(ePacketCode.GameMove);
            let move = new GameMove(from, to, isMove);
            move.userId = this.client.ProfileData.GamePlayer.userId;
            pkg.content = move;
            this.client.Send(pkg);
            this.client.Event.emit(EventType.StopDownDount, null);
        }

    }
    private ai_move() {
        // tslint:disable-next-line:no-console
        console.log("start ai search");
        let move = this.searchEngine.searchAGoodMove(Player.Black, this.CurrentGame, 1000);
        // tslint:disable-next-line:no-console
        console.log(move);
        if (move != null) {
            // tslint:disable-next-line:no-console
            console.log(move);
            let from: Coordinate = this.CurrentGame.getCoordAt(move.fromX, move.fromY);
            let to: Coordinate = this.CurrentGame.getCoordAt(move.toX, move.toY);
            if (!move.isMove) {
                this.CurrentGame.kill(from.piece, to.piece);
            } else {
                this.CurrentGame.move(from.piece, to);
            }
        }

        this.client.Event.emit(EventType.RefreshBoard, null);
    }
    private onReceiveGameMove(e: PkgEvent) {
        let pkg = e.Packet;
        // tslint:disable-next-line:no-console
        console.log(e);
        if (pkg.sussess) {
            let move: GameMove = pkg.content;
            let from = this.CurrentGame.getCoordAt(move.fromX, move.fromY);
            let to = this.CurrentGame.getCoordAt(move.toX, move.toY);
            let flag: boolean = false;
            if (move.isMove) {
                flag = this.CurrentGame.move(from.piece, to);
            } else {
                flag = this.CurrentGame.kill(from.piece, to.piece);
            }
            if (flag) {
                this.client.Event.emit(EventType.RefreshBoard, null);
                this.client.Event.emit(EventType.StartCountDown, null);
            }
            if (this.CurrentGame.IsGameOver) {
                this.gameOver();
            }
        }
    }

    public sendLeaveRoom() {
        let pkg = new Packet(ePacketCode.LeaveRoom);
        this.client.Send(pkg);
    }
    private onReceiveLeaveRoom(e: PkgEvent) {
        let pkg = e.Packet;
        if (pkg.sussess) {
            this.client.Event.emit(EventType.ReceiveLeaveRoom, new EventObject(null));
        } else {
            this.error(pkg.content);
        }
    }
    private gameOver() {
        this.client.Event.emit(EventType.GameOver, new EventObject({ winner: this.CurrentGame.Winner }));
    }
    public init() {
        this.client.PacketListener.on(ePacketCode.EnterRoom, this.onUserEnter, this);
        this.client.PacketListener.on(ePacketCode.LeaveRoom, this.onReceiveLeaveRoom, this);
        this.client.PacketListener.on(ePacketCode.GetRoomInfo, this.onReceiveGetRoomInfo, this);
        this.client.PacketListener.on(ePacketCode.JoinRoom, this.onReceiveJoinRoom, this);
        this.client.PacketListener.on(ePacketCode.GameMove, this.onReceiveGameMove, this);
        this.client.PacketListener.on(ePacketCode.GameReady, this.receiveGameReady, this);
        this.client.PacketListener.on(ePacketCode.GameStart, this.receiveGameStart, this);
        this.client.PacketListener.on(ePacketCode.GameOver, this.receiveGameOver, this);
        this.client.PacketListener.on(ePacketCode.GameChat, this.receiveChatMessage, this);
        this.getRoomInfo();
    }
    public dispose(): void {
        this.client.PacketListener.remove(ePacketCode.EnterRoom, this);
        this.client.PacketListener.remove(ePacketCode.LeaveRoom, this);
        this.client.PacketListener.remove(ePacketCode.GetRoomInfo, this);
        this.client.PacketListener.remove(ePacketCode.JoinRoom, this);
        this.client.PacketListener.remove(ePacketCode.GameMove, this);
        this.client.PacketListener.remove(ePacketCode.GameReady, this);
        this.client.PacketListener.remove(ePacketCode.GameStart, this);
        this.client.PacketListener.remove(ePacketCode.GameOver, this);
        this.client.PacketListener.remove(ePacketCode.GameChat, this);
    }

    public sendRequestLose() {
        let pkg = new Packet(ePacketCode.GameRequestLose);
        this.client.Send(pkg);
    }
    private onReceiveRequestLose(e: PkgEvent) {
        let pkg = e.Packet;
        if (!pkg.sussess) {
            this.error(pkg.content);
        }
    }

    public getRoomInfo(roomId: number = 0) {
        let pkg = new Packet(ePacketCode.GetRoomInfo);
        pkg.content = {
            roomId: 0
        };
        this.client.Send(pkg);
    }

    private onReceiveJoinRoom(e: PkgEvent) {
        if (e.Packet.sussess) {
            this.getRoomInfo();
        } else {
            this.error(e.Packet.content);
        }
    }
    private onUserEnter(event: PkgEvent) {
        let packet = event.Packet;
        if (packet.sussess) {
            this.client.Event.emit(EventType.ReceiveEnterRoom, new EventObject(packet.content));
        } else {
            this.error(packet.content);
        }
    }

    private onUserLeave(event: PkgEvent) {
        let packet = event.Packet;
    }
    private onReceiveGetRoomInfo(e: PkgEvent) {
        let pkg = e.Packet;
        if (pkg.sussess) {
            let room: Room = pkg.content;
            this.CurrentRoom = room;
            this.SetupGameInfo();
            this.client.Event.emit(EventType.ReceiveRoomInfo, new EventObject(room));
        } else {
            this.error(pkg.content);
        }
    }

    public sendGameReady() {
        if (this.CurrentRoom.type === eRoomType.PVE) {
            this.client.Event.emit(EventType.GameStart, null);
            return;
        }
        let pkg: Packet;
        if (this.IsHost) {
            pkg = new Packet(ePacketCode.GameStart);
        } else {
            pkg = new Packet(ePacketCode.GameReady);
        }
        pkg.content = this.client.ProfileData.GamePlayer.userId;
        this.client.Send(pkg);
    }

    private receiveGameReady(e: PkgEvent) {
        let pkg = e.Packet;
        if (pkg.sussess) {
            this.CurrentRoom.blackPlayer.status = ePlayerStatus.Ready;
            this.client.Event.emit(EventType.GameReady, new EventObject(pkg.content)); // Id of user ready
        } else {
            this.error(pkg.content);
        }
    }

    private receiveGameOver(e: PkgEvent) {
        let pkg = e.Packet;
        if (pkg.sussess) {
            this.client.Event.emit(EventType.GameOver, new EventObject(pkg.content));
            this.getRoomInfo();
        }
    }
    public sendChatMessage(messsage: string) {
        let pkg = new Packet(ePacketCode.GameChat);
        pkg.content = {
            username: this.client.ProfileData.GamePlayer.username,
            message: messsage,
            time: new Date().getTime()
        };
        this.client.Send(pkg);
    }
    private receiveChatMessage(e: PkgEvent) {
        if (e.Packet.sussess) {
            // tslint:disable-next-line:no-console
            console.log(e.Packet.content);
            this.client.Event.emit(EventType.ReceiveGameChat, new EventObject(e.Packet.content));
        } else {
            this.error(e.Packet.content);
        }
    }
    private receiveGameStart(e: PkgEvent) {
        let pkg = e.Packet;
        if (pkg.sussess) {
            this.CurrentRoom.redPlayer.status = ePlayerStatus.Playing;
            this.CurrentRoom.blackPlayer.status = ePlayerStatus.Playing;
            this.client.Event.emit(EventType.GameStart, new EventObject(pkg.content)); // Id of user ready

        } else {
            this.error(pkg.content);
        }
    }

}
