import { BaseService } from "./BaseService";
import { GameClient } from "../client/GameClient";
import { PkgEvent } from "../event/PkgEvent";
import { ePacketCode } from "../../../shared/common/enum/ePacketCode";
import { Packet } from "../../../shared/common/Packet";
import { EventType } from "../event/EventType";
import { eRoomType } from '../../../shared/common/enum/eRoomType';
import { EventObject } from "../event/EventObject";
import { Room } from "../../../shared/gamemodel/Room";
export class MainService extends BaseService {

    constructor(client: GameClient) {
        super(client);
    }
    public init(): void {
        // tslint:disable-next-line:no-console
        console.log(Packet);
        this.client.PacketListener.on(ePacketCode.GetRoomList, this.onReceiveGetRoomList, this);
        this.client.PacketListener.on(ePacketCode.CreateRoom, this.onReceiveCreateRoom, this);
        this.client.PacketListener.on(ePacketCode.JoinRoom, this.onReceiveJoinRoom, this);
        // this.client.PacketListener.on(ePacketCode.GetProfileData, this.onReceiveJoinRoom, this);
    }
    public getRoomList() {
        const pkg = new Packet(ePacketCode.GetRoomList);
        this.client.Send(pkg);
    }
    public createRoom(name: string, roomType: string, bet: number) {
        const pkg = new Packet(ePacketCode.CreateRoom);
        let typ: eRoomType = eRoomType[roomType];
        pkg.content = {
            name: name,
            type: typ,
            bet: bet
        };
        this.client.Send(pkg);
    }

    public joinRoom(room: Room) {
        let pkg = new Packet(ePacketCode.JoinRoom);
        pkg.content = { roomId: room.id };
        this.client.Send(pkg);
    }

    private onReceiveJoinRoom(e: PkgEvent) {
        // tslint:disable-next-line:no-console
        console.log("Receive join");
        this.client.Event.emit(EventType.ReceiveCreateRoom, new EventObject(e.Packet.content));
    }
    public onReceiveCreateRoom(e: PkgEvent) {
        this.client.Event.emit(EventType.ReceiveCreateRoom, new EventObject(e.Packet.content));
    }
    private onReceiveGetRoomList(pkg: PkgEvent) {
        this.client.Event.emit(EventType.ReceiveRoomList, new EventObject(pkg.Packet.content));
    }

    public dispose(): void {
        throw new Error("Method not implemented.");
    }
}