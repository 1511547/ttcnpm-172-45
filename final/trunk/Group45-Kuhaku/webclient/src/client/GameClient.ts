import { EventDispatcher } from "../event/EventDispatcher";
// import { GamePlayer } from "../model/GamePlayer";
import { Packet } from "../../../shared/common/Packet";
import { PkgEvent } from "../event/PkgEvent";
import * as io from 'socket.io-client';
import { PacketListener } from "../event/PacketListener";
import { EventManager } from "../event/EventManager";
import { MainService } from '../service/MainService';
import { RoomService } from '../service/RoomService';
import { ProfileDataService } from "../service/Data/ProfileDataService";
export class GameClient {

    private packetListener: PacketListener;
    public get PacketListener(): PacketListener {
        return this.packetListener;
    }

    private token: string;
    private socket: SocketIOClient.Socket;
    private profileData: ProfileDataService;
    public get ProfileData(): ProfileDataService {
        return this.profileData;
    }
    public get Socket() {
        return this.socket;
    }

    private event: EventManager;
    public get Event(): EventManager {
        return this.event;
    }

    constructor(token: string) {
        this.token = token;
        this.socket = io.connect({
            'query': 'token=' + token
        });
        this.socket.on('connect', this.onConnected.bind(this));
        this.packetListener = new PacketListener(this);
        this.event = new EventManager();
        this.profileData = new ProfileDataService(this);
        // this.mainService = new MainService(this);
        // this.roomService = new RoomService(this);
    }
    protected onConnected() {
        // tslint:disable-next-line:no-console
        console.log('connected');
        this.profileData.getProfileData();
    }
    public Send(pkg: Packet) {
        this.socket.emit('packet', pkg);
    }
}