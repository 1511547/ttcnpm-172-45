import * as React from 'react';
import './App.css'; 
import { Authentication } from './common/Authentication';
import { BrowserRouter as Router, Switch, Route, withRouter, Redirect, RouteComponentProps } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
class App extends React.Component<RouteComponentProps<any>, { authenticate: boolean }> {
  auth: Authentication;
  router: Router | null;
  constructor(props: RouteComponentProps<any>) {
    super(props);
    this.auth = new Authentication();
  }
  componentDidMount() {

    this.auth.isAuthenticated().then(val => {
      // tslint:disable-next-line:no-console
      console.log(val);
      if (val) {
        this.props.history.push("/play");
      } else {
        this.props.history.push("/landing");
      }
    }).catch(err => {
      // tslint:disable-next-line:no-console
      console.log("err");
    });
  }
  render() {
    return <div>Loading</div>;
  }
}
// tslint:disable-next-line:no-console
console.log('APP');
export default withRouter(App);
