import * as React from 'react';
import { BoardConfig } from './BoardConfig';
import "./css/Board.css.scss";
class RiverProps {
    config: BoardConfig;
}
export class River extends React.Component<RiverProps, {}> {
    height: number;
    width: number;
    constructor(props: RiverProps) {
        super(props);
        this.width = props.config.spacing * 8;
        this.height = props.config.spacing;
    }
    render() {
        let config = this.props.config;
        let boardTranslation = `translate(${config.boardPadding}, ${config.boardPadding})`;
        let riverTranslation = `translate(0, ${config.spacing * 4})`;
        let textTranslation = `translate(${config.spacing * 4},${config.spacing / 2 + 5})`;
        return (
            <g
                className="river"
                key="river"
                stroke="#dd4444"
                fill={config.fill}
                strokeWidth="1"
                transform={boardTranslation}
            >
                <g className="riverContent" transform={riverTranslation}>
                    <rect width={this.width} height={this.height} />
                    <g className="kaiti" transform={textTranslation}>
                        <text textAnchor="middle">Kuhaku - Chinese Chess Online</text>
                    </g>
                </g>
            </g>
        );
    }
}