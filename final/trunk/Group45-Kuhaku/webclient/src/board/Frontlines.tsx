import * as React from 'react';
import { BoardConfig } from './BoardConfig';

let barracadeLength = 6;
let paths = [
    `M -${3 * barracadeLength} -${barracadeLength} H -${barracadeLength} V -${3 * barracadeLength}`,
    `M ${3 * barracadeLength} ${barracadeLength} H ${barracadeLength} V ${3 * barracadeLength}`,
    `M -${3 * barracadeLength} ${barracadeLength} H -${barracadeLength} V ${3 * barracadeLength}`,
    `M ${3 * barracadeLength} -${barracadeLength} H ${barracadeLength} V -${3 * barracadeLength}`
].join(' ');
class CrossProps {
    transform: string;
}
class Cross extends React.Component<CrossProps, {}> {
    render() {
        return (
            <g transform={this.props.transform}>
                <path d={paths} />
            </g>
        );
    }
}

class FrontlinesProps {
    config: BoardConfig;
}
export class Frontlines extends React.Component<FrontlinesProps, {}> {
    boardPadding: number;
    width: number;
    height: number;
    constructor(props: FrontlinesProps) {
        super(props);
        this.boardPadding = props.config.boardPadding;
        this.width = props.config.spacing * 8;
        this.height = props.config.spacing;
    }

    render() {
        let boardTranslation = `translate(${this.boardPadding}, ${this.boardPadding})`;
        let crosses = [
            { x: 1, y: 2 },
            { x: 7, y: 2 },

            { x: 0, y: 3 },
            { x: 2, y: 3 },
            { x: 4, y: 3 },
            { x: 6, y: 3 },
            { x: 8, y: 3 },

            { x: 1, y: 7 },
            { x: 7, y: 7 },

            { x: 0, y: 6 },
            { x: 2, y: 6 },
            { x: 4, y: 6 },
            { x: 6, y: 6 },
            { x: 8, y: 6 },
        ];
        let config = this.props.config;
        return (
            <g
                className="frontlines"
                key="frontlines"
                stroke="#dd4444"
                fill="#fff"
                strokeWidth="1"
                transform={boardTranslation}
            >
                {crosses.map((cross, index) => {
                    return (
                        <Cross
                            key={`cross-${index}`}
                            transform={`translate(${cross.x * config.spacing}, ${cross.y * config.spacing})`}
                        />
                    );
                })}
            </g>
        );
    }
}