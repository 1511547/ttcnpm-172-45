import * as React from 'react';
import { BoardConfig } from './BoardConfig';
import { ArrayHelper } from '../../../shared/common/ArrayHelper';
class GridProps {
    config: BoardConfig;
}
export class Grid extends React.Component<GridProps, {}> {
    horizontalLines: Array<JSX.Element>;
    verticalLines: Array<JSX.Element>;
    campLines: Array<JSX.Element>;
    translatedBoardPadding: string;

    constructor(props: GridProps) {
        super(props);
        this.translatedBoardPadding = `translate(${props.config.boardPadding}, ${props.config.boardPadding})`;
        this.paintLines();
    }
    paintLines() {
        let config = this.props.config;
        this.verticalLines = ArrayHelper.expand(this.props.config.horizontalCount).map((x) => {
            return (
                <line
                    key={`x${x}`}
                    x1={x * config.spacing}
                    y1={0}
                    x2={x * config.spacing}
                    y2={config.spacing * (config.verticalCount - 1)}
                />);
        });

        this.horizontalLines = ArrayHelper.expand(config.verticalCount).map((y) => {
            return (
                <line
                    key={`y${y}`}
                    x1={0}
                    y1={y * config.spacing}
                    x2={config.spacing * (config.horizontalCount - 1)}
                    y2={y * config.spacing}
                />
            );
        });

        let camp = [
            { x1: 3 * config.spacing, y1: 0, x2: 4 * config.spacing, y2: config.spacing },
            { x1: 5 * config.spacing, y1: 0, x2: 4 * config.spacing, y2: config.spacing },
            { x1: 3 * config.spacing, y1: 2 * config.spacing, x2: 4 * config.spacing, y2: config.spacing },
            { x1: 5 * config.spacing, y1: 2 * config.spacing, x2: 4 * config.spacing, y2: config.spacing },

            { x1: 3 * config.spacing, y1: 9 * config.spacing, x2: 4 * config.spacing, y2: 8 * config.spacing },
            { x1: 5 * config.spacing, y1: 9 * config.spacing, x2: 4 * config.spacing, y2: 8 * config.spacing },
            { x1: 3 * config.spacing, y1: 7 * config.spacing, x2: 4 * config.spacing, y2: 8 * config.spacing },
            { x1: 5 * config.spacing, y1: 7 * config.spacing, x2: 4 * config.spacing, y2: 8 * config.spacing }
        ];
        this.campLines = camp.map((lineAttr, index) => {
            return <line {...lineAttr} key={`campLine${index}`} strokeDasharray="2, 4" />;
        });
    }

    render() {
        return (
            <g key="Grid">
                <g
                    className="xGrid-group"
                    key="xGrid"
                    stroke="#dd4444"
                    fill="white"
                    strokeWidth="1"
                    transform={this.translatedBoardPadding}
                >
                    {
                        this.verticalLines
                    }
                </g>
                <g
                    className="yGrid-group"
                    key="yGrid"
                    stroke="#dd4444"
                    fill="white"
                    strokeWidth="1"
                    transform={this.translatedBoardPadding}
                >
                    {
                        this.horizontalLines
                    }
                </g>
                <g
                    className="camp"
                    key="camp"
                    stroke="#dd4444"
                    fill="#eee"
                    strokeWidth="1"
                    transform={this.translatedBoardPadding} 
                >
                {
                    this.campLines
                }
                </g>
            </g>
        );
    }

}