export class BoardConfig {
    width: number;
    height: number;
    boardPadding: number;
    spacing: number;
    horizontalCount = 9;
    verticalCount = 10;
    fill: string = "#fff";
    get translatedBoardPadding() {
        return `translate(${this.boardPadding}, ${this.boardPadding})`;
    }

}