import * as React from 'react';
import { BoardConfig } from './BoardConfig';
import { Grid } from './Grid';
import { River } from './River';
import { Frontlines } from './Frontlines';
import { ChessGame } from '../../../shared/gamemodel/board/ChessGame';
import { ChessPiece } from './ChessPiece';
import { Piece } from '../../../shared/gamemodel/board/pieces/Piece';
import { Intersection } from './Intersection';
import { Coordinate } from '../../../shared/gamemodel/board/Coordinate';
import { Room } from '../../../shared/gamemodel/Room';
import { GameClient } from '../client/GameClient';
import { EventType } from '../event/EventType';
import { EventObject } from '../event/EventObject';
import { PieceState } from '../../../shared/gamemodel/board/pieces';
class ChessBoardProps {
    config: BoardConfig;
    client: GameClient;
    game: ChessGame;
}
class ChessBoardState {

}
export class ChessBoard extends React.Component<ChessBoardProps, ChessBoardState> {
    game: ChessGame;
    config: BoardConfig;
    constructor(props: ChessBoardProps) {
        super(props);
        this.config = props.config;
        this.game = this.props.game;
        this.props.client.Event.on(EventType.RefreshBoard, this.refreshBoard, this);
    }
    refreshBoard = (e: EventObject) => {
        this.forceUpdate();
    }
    render() {
        return (
            <svg
                width={this.props.config.width}
                height={this.props.config.height}
            >
                <rect
                    width={this.props.config.width}
                    height={this.props.config.height}
                    fill={this.props.config.fill}
                />
                <Grid config={this.props.config} />
                <River config={this.props.config} />
                <Frontlines config={this.props.config} />
                {this.renderChessPieces()}

                {this.renderIntersection()}
            </svg>
        );
    }
    renderChessPieces() {
        return (
            <g
                className="chesspieces"
                key="chesspieces"
                transform={this.config.translatedBoardPadding}
            >
                {this.game.getPieces.filter(x => x.state !== PieceState.Dead).map((avatar, index) => {
                    return (
                        <ChessPiece
                            key={`avatar-${index}-${avatar.name}-${avatar.coordinate.x}-${avatar.coordinate.y}`}
                            piece={avatar}
                            onSelectedPiece={this.onSelectedPiece}
                            spacing={this.config.spacing}
                        />);
                })}
            </g>
        );
    }

    renderIntersection() {
        return (

            this.game.getCoordinates.map((xCol) => {
                return (
                    xCol.map((coord, index) => {
                        // console.log(coord.x + "," + coord.y);
                        return (
                            <Intersection
                                key={`Intersection ${index}`}
                                spacing={this.config.spacing}
                                coord={coord}
                                selecIntersection={this.onSelectedIntersection.bind(this, coord)}
                            />
                        );
                    })
                );
            }
            )
        );
    }

    onSelectedIntersection = (coord: Coordinate) => {
        this.game.selectIntersection(coord);
        this.forceUpdate();
    }

    onSelectedPiece = (piece: Piece) => {

        // console.log(piece.getMoveOptions());
        this.game.selectPiece(piece);
        this.forceUpdate();
        // tslint:disable-next-line:no-console
        // console.log(piece);
    }
}