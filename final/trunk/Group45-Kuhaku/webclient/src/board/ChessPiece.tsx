import * as React from 'react';
import { PieceState } from '../../../shared/gamemodel/board/pieces/PieceState';
import { Piece } from '../../../shared/gamemodel/board/pieces/Piece';
import { Player } from '../../../shared/gamemodel/board/Player';
class ChessPieceProps {
    piece: Piece;
    spacing: number;
    onSelectedPiece: (piece: Piece) => void;
}

export class ChessPiece extends React.Component<ChessPieceProps, {}> {
    constructor(props: ChessPieceProps) {
        super(props);
    }
    onClick = (e: React.MouseEvent<SVGElement>) => {
        if (this.props.onSelectedPiece) {
            this.props.onSelectedPiece(this.props.piece);
        }
    }
    render() {
        var classNames = [
            'chess-piece',
            this.props.piece.isHaloed() === true ? "haloed" : "",
            this.props.piece.player === Player.Black ? "black" : "red",
            this.props.piece.state === PieceState.Selected ? "selected" : "",
            this.props.piece.state === PieceState.Haloed ? "haloed" : "",
        ];
        // console.log(this.props.piece.isHaloed) 

        return !this.props.piece.isDead() && (
            <g
                className={classNames.join(" ")}
                onClick={this.onClick}
            >

                <circle
                    className="piece-outline"
                    cy={this.props.spacing * this.props.piece.coordinate.y}
                    cx={this.props.spacing * this.props.piece.coordinate.x}
                    r="20"
                />

                <circle
                    className="piece-inline"
                    cy={this.props.spacing * this.props.piece.coordinate.y}
                    cx={this.props.spacing * this.props.piece.coordinate.x}
                    r="15"
                />

                <circle
                    className="piece-block"
                    cy={this.props.spacing * this.props.piece.coordinate.y}
                    cx={this.props.spacing * this.props.piece.coordinate.x}
                    r="15"
                />

                <text
                    x={this.props.spacing * this.props.piece.coordinate.x - 10}
                    y={this.props.spacing * this.props.piece.coordinate.y + 7}
                >
                    {this.props.piece.name}
                </text>
                <circle
                    className="piece-halo"
                    r='22'
                    cy={this.props.spacing * this.props.piece.coordinate.y}
                    cx={this.props.spacing * this.props.piece.coordinate.x}
                    fill={this.props.piece.player === Player.Black ? "black" : "red"}
                />
            </g>
        );
    }

}