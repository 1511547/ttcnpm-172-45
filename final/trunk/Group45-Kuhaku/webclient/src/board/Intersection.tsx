import * as React from 'react';
import { Coordinate, CoordinateState } from '../../../shared/gamemodel/board/Coordinate';
import "./css/Intersection.css.scss";
class IntersectionProps {
    coord: Coordinate;
    selecIntersection: (coord: Coordinate) => void;
    spacing: number;
}

export class Intersection extends React.Component<IntersectionProps, {}> {
    constructor(props: IntersectionProps) {
        super(props);
    }
    onClick = (e: React.MouseEvent<SVGElement>) => {
        if (this.props.selecIntersection) {
            this.props.selecIntersection(this.props.coord);
        }
    }
    render() {
        let classNameParts = [
            `intersection intersection-${this.props.coord.x}-${this.props.coord.y}`,
            !!this.props.coord.piece ? "occupied" : ""];
        switch (this.props.coord.state) {
            case CoordinateState.HighlightBlack:
                classNameParts.push("highlighted Coordinate_STATE__blackHighlighted");
                break;
            case CoordinateState.HighlightRed:
                classNameParts.push("highlighted Coordinate_STATE__redHighlighted");
                break;
            default:
                break;
        }
        let className = classNameParts.join(" ");
        return (
            <g key={className} className={className} onClick={this.onClick}  >
                <circle
                    r="14"
                    cy={(this.props.coord.y + 1) * this.props.spacing}
                    cx={(this.props.coord.x + 1) * this.props.spacing}
                    stroke={'1'}
                />
            </g>
        );
    }

}