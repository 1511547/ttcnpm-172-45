export enum EventType {
    ReceiveRoomList,
    ReceiveCreateRoom,
    ReceiveEnterRoom,
    ReceiveRoomInfo,
    ReceiveLeaveRoom,
    RefreshBoard,
    OpenDialog,
    CloseDialog,
    GameReady,
    GameStart,
    GameOver,
    ReceiveGameChat,
    StartCountDown,
    StopDownDount,
    Error
}