import { EventDispatcher } from "./EventDispatcher";
import { ePacketCode } from "../../../shared/common/enum/ePacketCode";
import { PkgEvent } from "./PkgEvent";
import { GameClient } from '../client/GameClient';
import { Packet } from "../../../shared/common/Packet";
/**
 * Dùng để handle event của socket
 */
export class PacketListener extends EventDispatcher<ePacketCode, PkgEvent> {
    private client: GameClient;
    constructor(client: GameClient) {
        super();
        this.client = client;
        this.client.Socket.on('packet', this.receivePacket.bind(this));
    }
    private receivePacket(packet: Packet) {
        this.emit(packet.code, new PkgEvent(packet));
    }
}