import { EventDispatcher } from "./EventDispatcher";
import { EventObject } from "./EventObject";
import { EventType } from "./EventType";

/**
 * Dùng để send event từ service to các service khác hoặc UI
 */
export class EventManager extends EventDispatcher<EventType, EventObject> {

}