import { EventType } from "./EventType";

export class EventObject {
    private obj: any;
    public get Object(): any {
        return this.obj;
    }
    constructor(obj: any) {
        this.obj = obj;
    }
}