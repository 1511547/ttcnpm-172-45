export type EventHandler<TEventArgs> = (e: TEventArgs) => void;
export class EventDispatcher<TKey, TEventArgs> {
    protected listeners: Map<TKey, Map<object, EventHandler<TEventArgs>>>;

    constructor() {
        this.listeners = new Map();
    }
    public on(eventType: TKey, handle: EventHandler<TEventArgs>, receiver: object) {

        let listener: Map<object, EventHandler<TEventArgs>>;
        if (this.listeners.has(eventType)) {
            listener = this.listeners.get(eventType);
        } else {
            listener = new Map<object, EventHandler<TEventArgs>>();
            this.listeners.set(eventType, listener);
        }
        listener.set(receiver, handle.bind(receiver));
    }

    public remove(eventType: TKey, receiver: object) {
        if (this.listeners.has(eventType)) {
            const listener = this.listeners.get(eventType);
            if (listener.has(receiver)) {
                listener.delete(receiver);
            }
        }
    }

    public emit(eventType: TKey, e: TEventArgs) {
        if (this.listeners.has(eventType)) {
            const listener = this.listeners.get(eventType);
            listener.forEach((handle, receiver) => {
                handle(e);
            });
        }
    }
}