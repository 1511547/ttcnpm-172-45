import { Packet } from "../../../shared/common/Packet";
import { ePacketCode } from "../../../shared/common/enum/ePacketCode";

export class PkgEvent  {

    private packet: Packet;
    public get Code(): ePacketCode {
        return this.packet.code;
    }
    public get Packet(): Packet {
        return this.packet;
    }

    constructor(packet: Packet) {
        this.packet = packet;
    }
}