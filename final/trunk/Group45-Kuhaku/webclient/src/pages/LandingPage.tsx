import * as React from 'react';
import { Button } from 'reactstrap';
import './LandingPage.css';
import { Authentication } from '../common/Authentication';
import { RouteComponentProps, Link } from 'react-router-dom';
import { LoginRegisterDialog } from './LoginRegisterDialog';
export interface LandingPageProps extends RouteComponentProps<any> {
}

export class LandingPageState {
    IsLoginOpen: boolean;
    IsLoggedIn: boolean;
    WindowWidth: number;
    WindowHeight: number;
}
export class LandingPage extends React.Component<LandingPageProps, LandingPageState> {
    auth: Authentication;
    constructor(props: LandingPageProps) {
        super(props);
        this.state = {
            IsLoginOpen: false,
            IsLoggedIn: false,
            WindowWidth: 0,
            WindowHeight: 0
        };
        this.auth = new Authentication();
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ WindowWidth: window.innerWidth, WindowHeight: window.innerHeight });
    }

    openLoginDialog = async () => {
        this.setState({
            IsLoginOpen: true
        });
    }
    loginDialogClosed = () => {
        this.setState({ IsLoginOpen: false });
    }
    loginSuccess = (token: string) => {
        alert("Login success.");
        this.props.history.push("play");
    }
    render() {
        let ww = this.state.WindowWidth;
        let wh = this.state.WindowHeight;
        let ratio = wh / 1000;
        if (ww / wh > 2000 / 1000) {
            ratio = ww / 2000;
        }
        return (
            <div className="outerWrapper">
                { 
                    this.state.IsLoginOpen && 
                    <LoginRegisterDialog onLoginSuccess={this.loginSuccess} onClosed={this.loginDialogClosed}/>
                }
                <div className="wrapper" style={{ transform: `scale(${ratio}, ${ratio})` }}>
                    <div className="container h-100">
                        <div className="row h-100 justify-content-center">
                            <div className="col">
                                <div className="intro">
                                    <div className="logo centered"/> 
                                    <h1>Thách đấu các kỳ thủ hàng đầu</h1>
                                    <h3 className="text-center">Cơ hội toả sáng trong một cộng đồng cờ tướng mới mẻ</h3>
                                </div>
                                <div className="text-center">
                                    <Button
                                        color="danger"
                                        className="text-center btn-exlg"
                                        onClick={this.openLoginDialog}
                                    >
                                        Chơi ngay
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default LandingPage;
