import * as React from 'react';
import './Register.css';
// import ReactDOM from 'react-dom';
import { MD5 } from "crypto-js";
import { JsonResponse } from '../../../shared/common/JsonResponse';
import { isEmail, isEmpty } from 'validator';
class RegisterProps {
    // onLoginSuccess?: (token: string) => void;
    // onClosed?: () => void;
}
class RegisterState {
    nameStatus: string; // name
    emailStatus: string; // email
    usernameStatus: string; // username
    passwordStatus: string; // password
    confirmPasswordStatus: string; // confirm password
    genderStatus: string; // gender 
    registerStatus: string; // is register success or not
    gender: boolean;
}

export class Register extends React.Component<RegisterProps, RegisterState> {
    name: string = "";
    email: string = "";
    username: string = "";
    password: string = "";
    confirmPassword: string = "";
    gender: boolean = false;
    estimatePassword = ""; // rat yeu, yeu, trung binh, manh
    emailValue: string = "";

    constructor(props: RegisterProps) {
        super(props);
        this.state = {
            nameStatus: "",
            emailStatus: "",
            usernameStatus: "",
            passwordStatus: "",
            confirmPasswordStatus: "",
            genderStatus: "",
            registerStatus: "",
            gender: false
        };
    }

    registerButtonClick = (e: React.MouseEvent<HTMLInputElement>) => {
        let validate: boolean = this.validate("name", this.name)
            && this.validate("email", this.email)
            && this.validate("username", this.username)
            && this.validate("password", this.password)
            && this.validate("confirmPassword", this.confirmPassword);
        if (validate) {
            this.reqRegister(this.name, this.username, this.password, this.gender, this.email);
        }

    }
    reqRegister(
        _name: string,
        _username: string,
        _password: string,
        _gender: boolean,
        _emailValue: string) {
        let md5Pass = MD5(_password).toString();
        let body = {
            name: _name,
            gender: _gender,
            username: _username,
            password: md5Pass,
            email: _emailValue
        };
        fetch("/user/register", {
            method: 'post',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(body)
        })
            .then((req) => {
                return req.json();
            })
            .then((data: JsonResponse<string>) => {
                if (data.success) {
                    this.setState({
                        registerStatus: "Đăng kí thành công"
                    });
                } else {
                    this.setState({
                        registerStatus: "Đăng kí thất bại: " + data.data
                    });
                }
            }).catch((err: Error) => {
                this.setState({
                    registerStatus: err.message
                });
            });

    }

    isContainUsername(pass: string, use: string) {
        var index = pass.includes(use);
        if (index) {
            this.setState({
                passwordStatus: "Mật khẩu chứa tên đăng nhập"
            });

        } else {
            this.setState({
                passwordStatus: ""
            });
        }
    }

    checkStrongPassword(password: string): void {
        // alert("OK men");
        if (password.length < 6) {
            this.setState({
                passwordStatus: "Mật khẩu phải có độ dài 6 kí tự trở lên"
            });
        } else {
            this.checkComplexPassword(password);
        }
    }

    checkComplexPassword(pass: string) {
        var isCapital = 0;
        var isNumber = 0;
        var isSpecialCharacter = 0;
        var isLowerCase = 0;
        var sum = 0;
        var i = 0;
        while (i < pass.length) {
            var ch = pass.charAt(i);
            if (ch >= '0' && ch <= '9') {
                isNumber = 1;
            } else if (ch >= 'a' && ch <= 'z') {
                isLowerCase = 1;
            } else if (ch >= 'A' && ch <= 'Z') {
                isCapital = 1;
            } else {
                isSpecialCharacter = 1;
            }
            i++;
        }
        sum = isCapital + isLowerCase + isNumber + isSpecialCharacter;
        switch (sum) {
            case 1: {
                this.estimatePassword = "Rất yếu";
                break;
            }
            case 2:
                this.estimatePassword = "Yếu";
                break;
            case 3:
                this.estimatePassword = "Trung Bình";
                break;
            case 4:
                this.estimatePassword = "Mạnh";
                break;
            default: {
                this.estimatePassword = "Mật khẩu không hợp lệ";
                break;
            }
        }
        this.setState({
            passwordStatus: this.estimatePassword
        });
    }
    validate = (field: string, val: string) => {
        switch (field) {
            case "name":
                if (!isEmpty(val)) {
                    this.name = val;
                    this.setState({
                        nameStatus: ""
                    });
                    return true;
                } else {
                    this.name = "";
                    this.setState({
                        nameStatus: "Tên không được trống"
                    });
                }
                break;
            case "username":
                if (val.length >= 3) {
                    this.username = val;
                    this.setState(
                        { usernameStatus: "" }
                    );
                    this.validate("password", this.password);
                    return true;
                } else {
                    this.username = "";
                    this.setState({
                        usernameStatus: "Tên đăng nhập phải có ít nhất 3 kí tự"
                    });
                }
                break;
            case "password":
                if (val.length > 0 && val.length < 6) {
                    this.setState({
                        passwordStatus: "Mật khẩu phải có ít nhất 6 kí tự."
                    });
                    this.password = "";
                } else {
                    if (this.username.length > 0 && val.includes(this.username)) {
                        this.setState({
                            passwordStatus: "Mật khẩu không được chứa tên đăng nhập"
                        });
                    } else {
                        this.password = val;
                        this.validate("confirmPassword", this.confirmPassword);
                        this.checkStrongPassword(this.password);
                        return true;
                    }
                }
                break;
            case "confirmPassword":
                this.confirmPassword = val;
                if (val.length > 0 && this.confirmPassword !== this.password) {
                    this.setState({
                        confirmPasswordStatus: "Không khớp với mật khẩu"
                    });
                } else {
                    this.setState({
                        confirmPasswordStatus: ""
                    });
                    return true;
                }
                break;
            case "email":
                if (isEmail(val)) {
                    this.email = val;
                    this.setState({ emailStatus: "" });
                    return true;

                } else {
                    this.setState({
                        emailStatus: "Email không hợp lệ"
                    });
                    this.email = "";
                }
                break;
            case "gender":
                this.gender = val === "male";
                this.setState({
                    gender: this.gender
                });
                return true;
            default:
                break;
        }
        return false;
    }
    render() {
        return (
            <div className="w-50 p-3">
                <div className="redText">
                    {this.state.registerStatus}
                </div>
                <div className="text-center col-lg-11">
                    <h3><b>ĐĂNG KÝ</b></h3>
                </div>

                <div className="form-row">
                    <div className="input-group">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Họ tên"
                            id="name"
                            onChange={(e) => this.validate("name", e.target.value)}
                        />
                    </div>
                    <div className="redText">
                        {this.state.nameStatus}
                    </div>
                </div>

                {/* <!-- input email><--> */}
                <div className="form-row">
                    <div className="input-group">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Email"
                            id="email"
                            onChange={(e) => this.validate("email", e.target.value)}
                        />
                    </div>
                    <div className="redText">
                        {this.state.emailStatus}
                    </div>
                </div>
                {/* <!-- input tên đăng nhập><--> */}
                <div className="form-row">
                    <div className="input-group">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Tên đăng nhập"
                            id="username"
                            onChange={(e) => this.validate("username", e.target.value)}
                        />
                    </div>
                    <div className="redText">
                        {this.state.usernameStatus}
                    </div>
                </div>

                {/* <!-- input mật khẩu><--> */}
                <div className="form-row">
                    <div className="input-group">
                        <input
                            type="password"
                            className="form-control"
                            placeholder="Mật khẩu"
                            id="password"
                            onChange={(e) => this.validate("password", e.target.value)}
                        />
                    </div>
                    <div className="redText">
                        {this.state.passwordStatus}
                    </div>
                </div>

                {/* <!-- input xác nhận mật khẩu mật khẩu><--> */}
                <div className="form-row">
                    <div className="input-group">
                        <input
                            type="password"
                            className="form-control"
                            placeholder="Xác nhận mật khẩu"
                            id="confirmPassword"
                            onChange={(e) => this.validate("confirmPassword", e.target.value)}
                        />
                    </div>
                    <div className="redText">
                        {this.state.confirmPasswordStatus}
                    </div>
                </div>
                {/* <!-- Checkbox male or female><--> */}
                <div className="form-row">
                    <div className="form-group w-100">
                        <div className="row">
                            <legend className="col-form-label col-sm-2">Giới tính:</legend>
                            <div className="col-sm-10 p-2">
                                <div
                                    className="form-check form-check-inline"
                                >
                                    <input
                                        className="form-check-input"
                                        type="radio"
                                        value="male"
                                        checked={this.state.gender === true}
                                        id="male"
                                        onChange={(e) => this.validate("gender", e.target.value)}
                                        name="gender"
                                    />
                                    <label className="form-check-label" htmlFor="male">Nam</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input
                                        className="form-check-input"
                                        type="radio"
                                        value="female"
                                        checked={this.state.gender === false}
                                        id="female"
                                        onChange={(e) => this.validate("gender", e.target.value)}
                                        name="gender"
                                    />
                                    <label className="form-check-label" htmlFor="female">Nữ</label>
                                </div>
                                <div className="redText">
                                    {this.state.genderStatus}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="form-row">
                    <input
                        className="btn btn-primary myButton"
                        type="button"
                        onClick={this.registerButtonClick}
                        value="Đằng kí"
                    />
                </div>
            </div>
        );
    }
}