import * as React from 'react';
import './Profile.css';
import { IUserDetail } from '../../../shared/model/IUserDetail';
import { IUser } from '../../../shared/model/IUser';

export interface IProfileProps {
    userDetail: IUserDetail;
    user: IUser;
}

export interface IProfileState {

}

export class Profile extends React.Component<IProfileProps, IProfileState> {
    render() {
        var expPercent =
            Math.round(this.props.userDetail.ExpCurrent / this.props.userDetail.ExpNextLevel * 100) + '%';
        return (
            <div className="container-fluid" id="body">
                <h1>Profile</h1>

                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-2">
                            <img
                                src={this.props.userDetail.Avatar}
                                className="img-thumbnail"
                            />
                        </div>
                        <div className="col-sm-6">
                            <p id="user-name">{this.props.user.Username} </p>
                            <p id="full-name">Tên đầy đủ: {this.props.userDetail.FullName} </p>
                            <p id="country">Quê quán: {this.props.userDetail.Country} </p>
                            <p id="sex">Giới tính: {this.props.userDetail.Sex} </p >
                            <i className="material-icons">border_color</i>
                        </div>
                        <div className="col-sm-4">
                            <div>Xếp hạng: {this.props.userDetail.Rank}</div>
                            <div>Tỷ lệ thắng: {this.props.userDetail.WinRate}%</div>
                            <div>Level: {this.props.userDetail.Level}</div>
                            <div>
                                <div className="float-left">Kinh nghiệm: </div>
                                <div className="float-left">
                                    <div className="progress">
                                        <div
                                            className="progress-bar"
                                            style={{ width: expPercent }}
                                        >{expPercent}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm" />
                        <div className="col-sm" id="brief-info">
                            <i className="material-icons" id="brief-info-icon">account_box</i>
                            <div>
                                <div>Đã tham gia</div>
                                <div>
                                    {this.props.userDetail.JoinDate && this.props.userDetail.JoinDate.toDateString()}
                                </div>
                            </div>
                        </div>
                        <div className="col-sm" id="brief-info">
                            <i className="material-icons" id="brief-info-icon">lock_open</i>
                            <div>
                                <div>Lần đăng nhập cuối</div>
                                <div>
                                    {this.props.userDetail.LastLogin && this.props.userDetail.LastLogin.toDateString()}
                                </div>
                            </div>
                        </div>
                        <div className="col-sm" id="brief-info">
                            <i className="material-icons" id="brief-info-icon">search</i>
                            <div>
                                <div>Lượt xem</div>
                                <div>{this.props.userDetail.Seen}</div>
                            </div>
                        </div>
                        <div className="col-sm" />

                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <ul className="nav nav-tabs" id="myTab" role="tablist">
                            <li className="nav-item">
                                <a
                                    className="nav-link active"
                                    id="introduce-tab"
                                    data-toggle="tab"
                                    href="#introduce"
                                    role="tab"
                                    aria-controls="introduce"
                                    aria-selected="true"
                                >
                                    Giới thiệu bản thân
                                </a>
                            </li>
                            <li className="nav-item">
                                <a
                                    className="nav-link"
                                    id="history-tab"
                                    data-toggle="tab"
                                    href="#history"
                                    role="tab"
                                    aria-controls="history"
                                    aria-selected="false"
                                >
                                    Lịch sử đấu
                                </a>
                            </li>
                        </ul>
                        <div className="tab-content" id="myTabContent">
                            <div
                                className="tab-pane fade show active"
                                id="introduce"
                                role="tabpanel"
                                aria-labelledby="introduce-tab"
                            >
                                {this.props.userDetail.Introduce}
                            </div>
                            <div
                                className="tab-pane fade"
                                id="history"
                                role="tabpanel"
                                aria-labelledby="history-tab"
                            >
                                ...
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Profile;
