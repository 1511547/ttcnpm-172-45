import * as React from 'react';
import './FindAcount.css';

class FindAcount extends React.Component {
  render() {
    return (
      <div className="colorBackground">
        <div className="container">
          <br />
          <br />
          <br />
          <br />
          <div className="row">
            <div className="col-lg-2" />
            <div className="col-lg-8 myBackground">
              <br />
              <br />
              <div className="row">

                {/* <!-- This is left component></--> */}
                <div className="col-lg-8">
                  <div className="row">
                    <div className="text-center col-lg-11">

                      <h3><b>TÌM LẠI TÀI KHOẢN</b></h3>

                    </div>
                    <div className="col-lg-1" />

                    {/* <!-- input tên đăng nhập><--> */}
                    <div className="col-lg-1" />
                    <div className="col-lg-10  text-left">
                      <br />
                      <div className="input-group">
                        <input type="text" className="form-control" placeholder="Tên đăng nhập" id="username" />
                      </div>
                    </div>
                    <div className="col-lg-1" />

                    {/* <!-- input mật khẩu><--> */}
                    <div className="col-lg-1" />
                    <div className="col-lg-10  text-left">
                      <br />
                      <div className="input-group">
                        <input type="text" className="form-control" placeholder="Mật khẩu" id="password" />
                      </div>
                    </div>
                    <div className="col-lg-1" />

                    {/* <!-- input xác nhận mật khẩu mật khẩu><--> */}
                    <div className="col-lg-1" />
                    <div className="col-lg-10  text-left">
                      <br />
                      <div className="input-group">
                        <input type="text" className="form-control" placeholder="Xác nhận mật khẩu" id="password" />
                      </div>
                    </div>
                    <div className="col-lg-1" />

                    <div className="col-lg-12" />
                    <br />

                    {/* <!-- Nhập mã xác nhận><--> */}
                    <div className="col-lg-1" />
                    <div className="col-lg-5 text-center">Nhập mã xác nhận</div>
                    <div className="col-lg-5">
                      <div className="input-group">
                        <input type="text" className="form-control" placeholder="Mã xác nhận" id="password" />
                      </div>
                    </div>
                    <div className="col-lg-1" />

                    {/* <!--Đăng nhập button><-->  */}
                    <div className="col-lg-1" />
                    <div className="col-lg-4">
                      <br />
                      <div >
                        <button className="myButton">Xác nhận</button>
                      </div>
                    </div>
                    <div className="col-lg-7" />
                  </div>
                </div>

                {/* <!-- This is right component></--> */}
                <div className="col-lg-4">
                  <br />
                  <br />
                  <div className="row">

                    {/* <!--app icon><--> */}
                    <div className="col-lg-1" />
                    <div className="col-lg-10 ">
                      <img src={require('../image/AppIcon.png')} className="appIcon" alt="Icon" />
                    </div>
                    <div className="col-lg-1" />

                  </div>

                  <div className="row">
                    <div className="col-lg-2" />
                    <div className="col-lg-8">
                      <br />
                      <div >
                        <button className="myButton">Đăng nhập</button>
                      </div>
                    </div>
                    <div className="col-lg-2" />
                  </div>
                </div>
              </div>

              <div className="col-lg-2" />
              <br />
              <br />

            </div>
          </div>
        </div>
        <br />
        <br />
        <br />
        <br />
        <br />

      </div>
    );
  }
}

export default FindAcount;
