import * as React from 'react';
import { ListRoom } from '../components/ListRoom';
import "./MainPage.css";
import { GameClient } from '../client/GameClient';
import { RouteComponentProps } from 'react-router-dom';
import { Authentication } from '../common/Authentication';
import { ProfileCard } from '../components/ProfileCard';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import * as FontAwesome from 'react-fontawesome';
import { Button } from 'reactstrap';
import { CreateRoomDialog } from '../components/room/CreateRoomDialog';
import { MainService } from '../service/MainService';
import { EventType } from '../event/EventType';
import { Room } from '../../../shared/gamemodel/Room';
import { RoomPage } from './RoomPage';
import { ConfirmDialog } from '../components/ConfirmDialog';
import { EventObject } from '../event/EventObject';
import { Dialog } from '../components/Dialog';
import { ToastContainer, toast } from 'react-toastify';

export interface MainPageProps extends RouteComponentProps<any> {
}

class MainPageState {
    IsCreateRoomOpen: boolean;
    Rooms: Room[];
    CurrentPage: string;
    dialog: React.Component;
}

export class MainPage extends React.Component<MainPageProps, MainPageState> {
    client: GameClient;
    service: MainService;
    constructor(props: MainPageProps) {
        super(props);
        let token = new Authentication().getToken();
        if (token) {
            this.client = new GameClient(token);
            this.service = new MainService(this.client);
            this.service.init();
        }
        this.state = {
            IsCreateRoomOpen: false,
            Rooms: [],
            CurrentPage: "ListRoom",
            dialog: null
        };
    }

    componentDidMount() {
        if (this.service) {
            this.client.Event.on(EventType.ReceiveRoomList, this.onReceiveRoomList, this);
            this.client.Event.on(EventType.ReceiveCreateRoom, this.onReceiveCreateRoom, this);
            this.client.Event.on(EventType.OpenDialog, this.onOpenDialog, this);
            this.client.Event.on(EventType.CloseDialog, this.onCloseDialog, this);
            this.client.Event.on(EventType.Error, this.onError, this);
            this.client.Event.on(EventType.ReceiveLeaveRoom, this.onLeaveRoom, this);
            this.service.getRoomList();
        }
    }
    onLeaveRoom = (e: EventObject) => {
        this.client.ProfileData.getProfileData();
        this.service.getRoomList();
        this.setState({
            CurrentPage: "ListRoom"
        });
    }
    onError(e: EventObject) {
        toast.error(e.Object);
    }
    onReceiveCreateRoom(e: EventObject) {
        this.setState({
            CurrentPage: "Room"
        });
    }

    onReceiveRoomList(e: EventObject) {
        this.setState({
            Rooms: e.Object
        });
    }

    openCreateRoomDialog = async () => {
        let createRoomDialog = (
            <CreateRoomDialog
                onSubmited={this.onCreateRoomSubmited}
                onClosed={this.onCreateRoomClosed}
            />
        );
        this.client.Event.emit(EventType.OpenDialog, new EventObject(createRoomDialog));
    }

    onBtnCreateRoomClick = async () => {
        this.setState({
            IsCreateRoomOpen: true
        });
    }

    onRefreshButtonClick = () => {
        this.service.getRoomList();
    }

    onCreateRoomClosed = () => {
        this.setState({ IsCreateRoomOpen: false });
        this.client.Event.emit(EventType.CloseDialog, null);
    }

    onCreateRoomSubmited = (roomName: string, roomType: string, bet: number): void => {
        this.service.createRoom(roomName, roomType, bet);
        this.client.Event.emit(EventType.CloseDialog, null);
    }

    onJoinRoom = (room: Room) => {
        this.service.joinRoom(room);
    }

    onOpenDialog = (obj: EventObject) => {
        if (this.state.dialog) {
            this.setState({ dialog: null });
        }
        this.setState({ dialog: obj.Object });
    }

    onCloseDialog = (obj: EventObject) => {
        this.setState({ dialog: null });
    }

    render() {
        return (
            <div className="main">
                <ToastContainer />
                <Dialog content={this.state.dialog} />
                {this.state.CurrentPage === "ListRoom" &&
                    <ListRoom
                        client={this.client}
                        rooms={this.state.Rooms}
                        onJoinRoom={this.onJoinRoom}
                        onRefreshList={this.onRefreshButtonClick}
                        onCreateRoom={this.openCreateRoomDialog}
                    />
                }
                {this.state.CurrentPage === "Room" &&
                    <RoomPage client={this.client} />
                }
            </div>
        );
    }
}
