import * as React from 'react';
import { Room } from '../../../shared/gamemodel/Room';
import { BoardConfig } from '../board/BoardConfig';
import { ChessBoard } from '../board/ChessBoard';
import { GameClient } from '../client/GameClient';
import { PlayerCard } from '../components/room/PlayerCard';
import { Button } from 'reactstrap';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import { Scrollbars } from 'react-custom-scrollbars';
import ConfirmDialog from '../components/ConfirmDialog';
import './RoomPage.css';
import { RoomService } from '../service/RoomService';
import { EventType } from '../event/EventType';
import { EventObject } from '../event/EventObject';
import { ChatMessage } from '../../../shared/gamemodel/ChatMessage';
import { GamePlayer } from '../../../shared/gamemodel/GamePlayer';
import { ProfileDialog } from '../components/room/ProfileDialog';
import { Player } from '../../../shared/gamemodel/board/Player';
import { platform } from 'os';

class RoomProps {
    client: GameClient;
}

class RoomState {
    ChatMessages: ChatMessage[];
    loaded: boolean;
    IsShowReady: boolean;
    IsShowEndGame: boolean;
    IsShowWating: boolean;
    gameover: any;
    timeout: number;
}
const boardConfig = new BoardConfig();
boardConfig.width = 450;
boardConfig.height = 495;
boardConfig.boardPadding = 45;
boardConfig.spacing = 45;
export class RoomPage extends React.Component<RoomProps, RoomState> {

    private service: RoomService;
    private client: GameClient;
    private timer: NodeJS.Timer;
    private chatMesssages: ChatMessage[] = [];
    constructor(props: RoomProps) {
        super(props);
        this.state = {
            loaded: false,
            ChatMessages: [],
            IsShowReady: true,
            IsShowEndGame: false,
            IsShowWating: false,
            gameover: {},
            timeout: 600
        };
        this.timer = null;
        this.service = new RoomService(this.props.client);
        this.client = this.props.client;
    }

    numberWithDots(num: number) {
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    componentDidMount() {
        this.client.Event.on(EventType.ReceiveRoomInfo, this.onReceiveRoomInfo, this);
        this.client.Event.on(EventType.GameReady, this.onGameReady, this);
        this.client.Event.on(EventType.GameStart, this.onGameStart, this);
        this.client.Event.on(EventType.GameOver, this.onGameOver, this);
        this.client.Event.on(EventType.ReceiveGameChat, this.onReceiveChat, this);
        this.client.Event.on(EventType.StartCountDown, this.startCountDown, this);
        this.client.Event.on(EventType.StopDownDount, this.stopCountDown, this);
        this.service.init();
    }
    startCountDown = () => {
        if (!!this.timer) {
            clearInterval(this.timer);
        } else {
            this.timer = setInterval(
                () => {
                    const time = this.state.timeout;
                    if (time === 0) {
                        this.timeout();
                        return;
                    }
                    this.setState({
                        timeout: time - 1
                    });
                },
                1000);
        }
    }
    timeout = () => {
        this.service.sendRequestLose();
        // tslint:disable-next-line:no-console
        console.log("timeout");
        this.stopCountDown();
    }
    stopCountDown = () => {
        if (!!this.timer) {
            clearInterval(this.timer);
        }
        this.timer = null;
        this.setState({
            timeout: 600
        });
    }
    componentWillUnmount() {
        this.service.dispose();
    }
    onGameReady = (e: EventObject) => {
        if (!this.service.IsHost) {
            this.setState({ IsShowWating: true, IsShowReady: true });
        } else {
            this.forceUpdate();
        }
    }

    onGameStart = (e: EventObject) => {
        this.setState({
            IsShowWating: false,
            IsShowReady: false,
            IsShowEndGame: false
        });
    }

    onGameOver = (e: EventObject) => {
        let winner = e.Object.winner;
        let thang = 'url("/assets/imgThang.png")';
        let hoa = 'url("/assets/imgHoa.png")';
        let thua = 'url("/assets/imgThua.png")';
        let exp = 0;
        let money = 0;
        let image = '';
        // tslint:disable-next-line:no-console
        console.log(!this.service.CurrentRoom.blackPlayer);
        if (winner !== Player.Black && winner !== Player.Red) {
            image = hoa;
            exp = 50;
            money = 0;
        } else if (winner === this.service.CurrentGame.CurrentPlayer || e.Object.isLeaveRoom) {
            image = thang;
            exp = 100;
            money = this.service.CurrentRoom.bet;
        } else {
            image = thua;
            exp = 10;
            money = -this.service.CurrentRoom.bet;
        }

        this.setState({
            IsShowEndGame: true,
            gameover: {
                image: image,
                exp: exp,
                money: money
            }
        });
    }

    onReceiveRoomInfo = (e: EventObject) => {
        this.setState({
            loaded: true
        });
    }

    onReceiveChat = (e: EventObject) => {
        let username = e.Object.username;
        let message = e.Object.message;
        let time = e.Object.time;
        this.chatMesssages.push(new ChatMessage(username, message));
        this.setState({
            ChatMessages: this.chatMesssages
        });
    }
    onChatInputKeyUp = (event: React.KeyboardEvent<HTMLInputElement>): void => {
        if (event.key === 'Enter') {
            let msg = event.currentTarget.value;
            if (msg) {
                event.currentTarget.value = '';
                this.service.sendChatMessage(msg);
            }
        }
    }

    onBtnExitRoomClick = () => {
        let dialog = (
            <ConfirmDialog
                onClosed={this.onExitRoomClosed}
                confirmHeader="Rời phòng"
                confirmText="Bạn có muốn xin thua và rời khỏi phòng ?"
            />
        );
        this.client.Event.emit(EventType.OpenDialog, new EventObject(dialog));
    }

    onBtnXinDiLaiClick = () => {
        let dialog = (
            <ConfirmDialog
                onClosed={this.onXinDiLaiClosed}
                confirmHeader="Xin đi lại"
                confirmText="Bạn có muốn xin đi lại?"
            />
        );
        this.client.Event.emit(EventType.OpenDialog, new EventObject(dialog));
    }

    onBtnCauHoaClick = () => {
        let dialog = (
            <ConfirmDialog
                onClosed={this.onCauHoaClosed}
                confirmHeader="Cầu hoà"
                confirmText="Bạn có muốn cầu hoà không ?"
            />
        );
        this.client.Event.emit(EventType.OpenDialog, new EventObject(dialog));
    }

    onBtnXinThuaClick = () => {
        let dialog = (
            <ConfirmDialog
                onClosed={this.onXinThuaClosed}
                confirmHeader="Xin thua"
                confirmText="Bạn có muốn xin thua?"
            />);
        this.client.Event.emit(EventType.OpenDialog, new EventObject(dialog));
    }

    onXinDiLaiClosed = (answer: boolean) => {
        this.client.Event.emit(EventType.CloseDialog, new EventObject(null));
        // send xin di lai
    }

    onCauHoaClosed = (answer: boolean) => {
        this.client.Event.emit(EventType.CloseDialog, new EventObject(null));
        // send cau hoa 
    }

    onXinThuaClosed = (answer: boolean) => {
        this.client.Event.emit(EventType.CloseDialog, new EventObject(null));
        // send xin thua
        this.service.sendRequestLose();
    }

    onExitRoomClosed = (answer: boolean) => {
        if (answer) {
            this.service.sendLeaveRoom();
        }
        this.client.Event.emit(EventType.CloseDialog, new EventObject(null));
        // send exit room
    }

    onBtnReadyClick = () => {
        this.service.sendGameReady();
    }

    onBtnGameoverContinueClick = () => {
        this.service.CurrentGame.reset();
        this.client.Event.emit(EventType.RefreshBoard, null);
        this.setState({
            IsShowEndGame: false,
            IsShowReady: true
        });
    }

    onPlayerCardClick = (player: GamePlayer) => {
        let dialog = <ProfileDialog player={player} onClosed={this.onDialogClosed} />;
        this.client.Event.emit(EventType.OpenDialog, new EventObject(dialog));
    }

    onDialogClosed = () => {
        this.client.Event.emit(EventType.CloseDialog, new EventObject(null));
    }

    render() {

        if (!this.state.loaded) {
            return null;
        }
        const chatMessages = this.state.ChatMessages.map((chatMsg, index) => {
            return <p key={index}><span>{chatMsg.username}:</span>&nbsp;{chatMsg.chatMessage}</p>;
        });
        // if player is host
        let readyText = "";
        if (this.service.IsHost) {
            readyText = "Bắt đầu";
        } else {
            readyText = "Sẵn sàng";
        }
        return this.state.loaded ? (
            <div className="game-room" >
                <Button
                    className="btn-exit-room"
                    color="danger"
                    onClick={this.onBtnExitRoomClick}
                >
                    Thoát&nbsp;
                    <FontAwesomeIcon icon="sign-out-alt" size="1x" />
                </Button>
                <div className="game-left-panel">
                    <div className="player-info">
                        <PlayerCard
                            color="red"
                            onClick={this.onPlayerCardClick}
                            player={this.service.CurrentRoom.redPlayer}
                        />
                    </div>
                    <div className="game-info">
                        <div className="game-info-border">
                            <div className="room-bet text-stroke">
                                {this.numberWithDots(this.service.CurrentRoom.bet)}
                            </div>
                            <div className="room-time">
                                <div className="player-time">{this.state.timeout}</div>
                            </div>
                        </div>
                    </div>
                    <div className="game-menu">

                        {/*                         <Button
                            onClick={this.onBtnXinDiLaiClick}
                            className="game-menu-item float-left"
                            color="danger"
                        >
                            Xin đi lại
                        </Button>
                        <Button
                            onClick={this.onBtnCauHoaClick}
                            className="game-menu-item"
                            color="danger"
                        >
                            Cầu hòa
                        </Button> */}
                        <Button
                            onClick={this.onBtnXinThuaClick}
                            className="game-menu-item"
                            color="danger"
                        >
                            Xin thua
                        </Button>
                    </div>
                    <div className="player-info">
                        <PlayerCard
                            color="black"
                            onClick={this.onPlayerCardClick}
                            player={this.service.CurrentRoom.blackPlayer}
                        />
                    </div>
                </div>
                <div className="game-middle-panel">
                    <div className="text-center">
                        <h1>{this.service.CurrentRoom.name}</h1>
                    </div>
                    <div className="text-center">
                        <div className="chess-board">
                            <ChessBoard config={boardConfig} client={this.client} game={this.service.CurrentGame} />
                            <div
                                className="ready-overlay"
                                style={{ display: this.state.IsShowReady ? "block" : "none" }}
                            >
                                {this.state.IsShowWating && <h1 className="text-stroke">Đang chờ...</h1>}
                                {!this.state.IsShowWating &&
                                    <Button
                                        className="btn-lg"
                                        color="danger"
                                        onClick={this.onBtnReadyClick}
                                    >
                                        {readyText}
                                    </Button>
                                }
                            </div>
                            <div
                                className="end-game-overlay"
                                style={{ display: this.state.IsShowEndGame ? "block" : "none" }}
                            >
                                <div className="end-game-info">
                                    <div
                                        className="end-game-img centered"
                                        style={{ backgroundImage: this.state.gameover.image }}
                                    />
                                    <span className="player-coin text-stroke">{this.state.gameover.money}</span>
                                    <span className="player-exp text-stroke">{this.state.gameover.exp}</span>
                                </div>
                                <Button
                                    className="btn-sm"
                                    color="danger"
                                    onClick={this.onBtnGameoverContinueClick}
                                >
                                    Tiếp tục
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="game-right-panel">
                    <div className="game-chat">
                        <div className="chat-scrollbar">
                            <Scrollbars
                                autoHide={true}
                                autoHideTimeout={1000}
                                autoHideDuration={200}
                            >
                                <div className="chat-area">
                                    {chatMessages}
                                </div>
                            </Scrollbars>
                        </div>
                        <input
                            className="chat-input"
                            type="text"
                            placeholder="nhập nội dung chat"
                            onKeyUp={this.onChatInputKeyUp}
                        />
                    </div>
                </div>
            </div>
        ) : (<div>Loading</div>);
    }
}
