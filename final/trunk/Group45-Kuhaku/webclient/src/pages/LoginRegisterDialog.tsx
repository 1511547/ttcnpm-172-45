import * as React from 'react';
import './Login.css';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import * as fa from '@fortawesome/fontawesome-free-solid';
import { Alert, Modal, ModalBody } from 'reactstrap';
import { MD5 } from "crypto-js";
import { Authentication } from '../common/Authentication';
import { Link } from 'react-router-dom';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import { LoginForm } from '../components/LoginForm';
import { RegisterForm } from '../components/RegisterForm';

class LoginRegisterDialogProps {
    onLoginSuccess?: (token: string) => void;
    onClosed?: () => void;
}
class LoginRegisterDialogState {
    hasError: boolean;
    errorMessage: string;
    isOpen: boolean;
    activeTab: string;
}
export class LoginRegisterDialog extends React.Component<LoginRegisterDialogProps, LoginRegisterDialogState> {
    username: HTMLInputElement | null;
    password: HTMLInputElement | null;
    constructor(props: LoginRegisterDialogProps) {
        super(props);
        this.state = {
            hasError: false,
            errorMessage: "",
            isOpen: true,
            activeTab: '1'
        };
        this.toggle = this.toggle.bind(this);
    }

    toggle(tab: string) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    closeClick = () => {
        this.setState({
            isOpen: false
        });
        if (this.props.onClosed) {
            this.props.onClosed();
        }
    }
    onLoginSuccess = (token: string): void => {
        if (this.props.onLoginSuccess) {
            this.props.onLoginSuccess(token);
        }
    }
    loginButtonClick = async (e: React.MouseEvent<HTMLButtonElement>) => {
        let self = this;
        if ((!this.username || !this.username.value) || (!this.password || !this.password.value)) {
            this.setState({
                hasError: true,
                errorMessage: "Tên và mật khẩu không được trống."
            });
        } else {
            let md5Pass = MD5(this.password.value).toString();
            let token = await new Authentication().authenicate(this.username.value, md5Pass);
            if (token !== null) {
                this.onLoginSuccess(token);
            }
        }
    }
    render() {
        let err = this.state.hasError && (
            <div className="input-group p-1">
                <Alert color="danger w-100">
                    {this.state.errorMessage}
                </Alert>
            </div>
        );
        return (
            <Modal isOpen={this.state.isOpen}>
                <div className="modal-header">
                    <h5 className="modal-title">Đăng ký / Đăng nhập - KUHAKU</h5>
                    <button className="close" data-dismiss="modal" aria-label="Close" onClick={this.closeClick}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <ModalBody>
                    <Nav tabs={true}>
                        <NavItem>
                            <NavLink
                                onClick={() => { this.toggle('1'); }}
                                active={this.state.activeTab === '1'}
                            >
                                Đăng nhập
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                onClick={() => { this.toggle('2'); }}
                                active={this.state.activeTab === '2'}
                            >
                                Đăng ký
                            </NavLink>
                        </NavItem>
                    </Nav>
                    <TabContent activeTab={this.state.activeTab}>
                        <TabPane tabId="1">
                            <Row>
                                <Col sm="12">
                                    <LoginForm onLoginSuccess={this.onLoginSuccess}/>
                                </Col>
                            </Row>
                        </TabPane>
                        <TabPane tabId="2">
                            <Row>
                                <Col sm="12">
                                    <RegisterForm />
                                    
                                </Col>
                            </Row>
                        </TabPane>
                    </TabContent>
                </ModalBody>
            </Modal >
        );
    }
}

export default LoginRegisterDialog;
