import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import 'jquery/dist/jquery.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import './pages/MainPage.tsx';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { PrivateRoute } from './common/PrivateRoute';
import LandingPage from './pages/LandingPage';
import { MainPage } from './pages/MainPage';
import { Register } from './pages/Register';
ReactDOM.render(
  (
    <Router>
      <Switch>
        <Route exact={true} path="/" component={App} />
        <Route exact={true} path="/landing" component={LandingPage} />
        <Route exact={true} path="/register" component={Register} />
        <PrivateRoute exact={true} path="/play" component={MainPage} />
      </Switch>
    </Router>
  ),
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
