import { Route, RouteProps, Redirect, Switch } from 'react-router-dom';
import * as React from 'react';
import { Authentication } from './Authentication';
type PrivateRouteProps = RouteProps;

export class PrivateRoute extends React.Component<PrivateRouteProps, any> {
    constructor(props: PrivateRouteProps) {
        super(props);
    }
    render() {
        if (Authentication.authenticated) {
            return <Route {...this.props} />;
        } else {
            return (
                <Redirect to="/" />
            );
        }
    }
}
