import { JsonResponse } from '../../../shared/common/JsonResponse';
import * as Store from 'store';
import * as local from 'store/storages/localStorage';
import * as session from 'store/storages/sessionStorage';
const localStore = Store.createStore(local);
const sessionStore = Store.createStore(session);
export type Token = string | null;
export class Authentication {
    static authenticated: boolean = false;
    async isAuthenticated(): Promise<boolean> {
        const auth = sessionStore.get('authenticated');
        const token = localStore.get('token');
        if (auth && auth === "true") {
            return true;
        }
        if (token !== undefined) {
            try {
                const result: JsonResponse<string> = await fetch('/user/auth', {
                    method: 'post',
                    headers: {
                        'content-type': 'application/json'
                    },
                    body: JSON.stringify({
                        token: token
                    })
                }).then(async (res) => {
                    return await res.json();
                });
                if (result.success) {
                    sessionStore.set('authenticate', "true");
                    Authentication.authenticated = true;
                    return true;
                } else {
                    return false;
                }
            } catch (err) {
                return false;
            }
        }
        return false;
    }
    async authenicate(username: string, password: string): Promise<string | null> {
        try {
            let result: JsonResponse<any> = await fetch('/user/login', {
                method: 'post',
                headers: {
                    'content-type': 'application/json'
                },
                body: JSON.stringify({
                    username: username,
                    password: password
                })
            }).then(async (res) => {
                return await res.json();
            });
            if (result.success) {
                Authentication.authenticated = true;
                localStore.set('token', result.data.token);
                return result.data.token;
            } else {
                return null;
            }
        } catch (err) {
            return null;
        }
    }
    getToken(): Token {
        if (Authentication.authenticated) {
            return localStore.get('token');
        } else {
            return null;
        }

    }
    logout(callback: () => void) {
        localStore.remove('token');
        Authentication.authenticated = false;
        if (callback) {
            callback();
        }
    }
}