# Kuhaku - Chinese Chess Online

## Tính năng chính:

1. PVP

    Người chơi có thể tạo phòng, và chơi với nhau, có hệ thống  tiền thưởng, cấp độ
    
2. PVE

    Người chơi có thể tạo phòng PVE để luyện tập với AI . AI được viết bằng thuật toán Minimax  Aplha-Beta 

## Environment

* NodeJs 8
* Typescript
* ReactJs
* Docker
* Database: Mysql 5.7 (docker image mysql:5.7)
* Gulp
* Jest test
* Tslint 

## Develop

## CI Pipeline

Được setup 2 stage đê kiểm tra style coding và chạy unit test mỗi khi push commit lên. Và  branch master được protected chỉ được phép merge request khi đã pass tất cả các stage trong pipeline.

## Develop

Cài các phần mềm kể trên, cd vào thư mục code. Chạy  `npm install` để cài đặt các package của nodejs

Chạy `npm run dev` để bắt đầu dev .  Có sẵn chế độ auto-rebuild  khi save code.

## Style coding 
    
Bắt buộc phải chạy `gulp tslint ` để kiểm style coding trước khi commit 
    
## Unit test

Chạy `npx jest <test_name>` để chạy từng test một. Hoặc chạy  `gulp test` đẻ chạy toàn bộ test 
